'''
Created on Jan 13, 2022

@author: dweissen
'''

import logging as lg
log = lg.getLogger('ActiveLearner')
from typing import List
from pandas.core.frame import DataFrame
import configparser

from Data.Pool import Pool
from ActiveLearners.ActiveLearnerInt import ActiveLearnerInt
from ActiveLearners.Classifiers.ClassifierInt import ClassifierInt
from ActiveLearners.Ensemblers.EnsemblerInt import EnsemblerInt
from ActiveLearners.Oracles.OracleInt import OracleInt
from Evaluation.Evaluater import Scores
from Evaluation.Evaluater import Performance
from Evaluation.EvaluaterInt import EvaluaterInt
from Evaluation.Evaluater import Evaluater


class ActiveLearner(ActiveLearnerInt):
    '''
    The basic learner which runs the active learning session
    This learner implement a default majority voting function to merge 
    the predictions of multiple classifiers
    '''


    def __init__(self, config: configparser, ensembler:EnsemblerInt, classifiers: List[ClassifierInt], evaluater:EvaluaterInt = None, trainingArgs:dict=None):
        super(ActiveLearner, self).__init__(config, ensembler, classifiers, evaluater, trainingArgs)


    def getClassifiers(self) -> List[ClassifierInt]:
        return self.classifiers

    
    def train(self, pool:Pool, oracle:OracleInt) -> list:
        """
        Train the classifier with AL
        :param pool: a valid pool with training set starting with the seed, a validation and test set annotated as well as unlabeled data
        :param oracle: an oracle that can return the labels of the unlabeled data
        :return: 
        """
        iteration = 0
        #at this step it should be only the seeds examples (iteration 0)
        for classifier in self.getClassifiers():
            classifier.train(pool.getMostRecentTrainingExamples(), pool.valEx, classifier.getInitialCheckpointPath())
        self.ensembler.train(pool.getMostRecentTrainingExamples(), pool.valEx)
        
        if self.trainingArgs['evaluateEachALIteration']:
            self.__evaluate(pool, iteration)
            
        self.saveAt(0)

        log.warning("TODO: set the patience correctly.")         
        stillImproving = True
        
        while stillImproving:
            if self.budget<=0:
                log.info("The learner may still improve but it ran out of budget for annotation. Exit...")
                break
            else:
                iteration = iteration + 1
                log.debug(f"At iteration {iteration}, budget={self.budget}.")
                 
                miExamples = self.getMostInformativeExamples(pool, ExQueriedBatchSize=self.ExQueriedBatchSize)
                oracle.getQueryLabels(miExamples, iteration, writeQueriedExamples=True)
                log.info(f"The distribution of positive/negative labels in the most informative examples queried was pos:{len(miExamples[miExamples['label']==1])} - neg:{len(miExamples[miExamples['label']==0])}")
                #if we are working with a pre-annotated unlabeled set, write the remaining number ofn positive examples in the set (for debbugging purpose)
                if self.trainingArgs["unlabledSetAnnotated"]:
                    unlabeledCopied = pool.unlabEx.copy(deep=True)
                    oracle.getQueryLabels(unlabeledCopied, iteration, writeQueriedExamples=False)
                    log.info(f"The distribution of positive/negative labels remaining in the unlabeled set is pos:{len(unlabeledCopied[unlabeledCopied['label']==1])} - neg:{len(unlabeledCopied[unlabeledCopied['label']==0])}")
                pool.addQueryResult(miExamples, iteration)
                self.budget = self.budget - len(miExamples)
                
                if self.trainingArgs["incrementalLearning"]: #training on the batch of examples most recently annotated
                    for classifier in self.getClassifiers():
                        classifier.train(pool.getMostRecentTrainingExamples(), pool.valEx, classifier.getCurrentCheckpointPath())
                else: #retrain on the full set of examples annotated
                    for classifier in self.getClassifiers():
                        classifier.train(pool.trainEx, pool.valEx, classifier.getInitialCheckpointPath())
                self.ensembler.train(pool.trainEx, pool.valEx)
                
                if self.trainingArgs['evaluateEachALIteration']:
                    self.__evaluate(pool, iteration)
                    self.evaluater.valPerf.drawPerformance(self.config["Data"]["tmpFolder"]+self.__class__.__name__+"_valPerf.pdf")
                    self.evaluater.testPerf.drawPerformance(self.config["Data"]["tmpFolder"]+self.__class__.__name__+"_testPerf.pdf")

                self.saveAt(iteration)


    def saveAt(self, iteration: int):
        for classifier in self.getClassifiers():
            classifier.save(iteration)

    def __evaluate(self, pool:Pool, iteration: int):
        """
        Evaluate the performance of the learner on the validation and test sets of the pool for the given iteration of AL
        :param pool: a valid pool with eval and test sets
        :param iteration: the number of iteration in AL, 0 for the iteration on the seed
        :do: add the scores in the evaluater attached to the learner
        """
        #we compute the improvement on eval
        predictionsEval = self.classify(pool.valEx)
        scoresEval = self.evaluater.evaluate(predictionsEval, pool.valEx)
        if iteration==0:
            log.info(f"Performance after training on the seed set on the Evaluation set: {scoresEval}")
        else:
            log.info(f"Performance after training on the training set at iteration n'{iteration} on the Evaluation set: {scoresEval}")
        self.evaluater.valPerf.addPerformance(iteration, scoresEval)
        
        #we compute the improvement of the al on the test set and add to the scores in performance
        predictionsTest = self.classify(pool.testEx)
        if self.trainingArgs['writePredictionsAtEachALIteration']:
            predictionsTest.to_csv(self.config["Data"]["tmpFolder"]+"outActiveLearner_I"+str(iteration)+".tsv", sep="\t")
        scoresTest = self.evaluater.evaluate(predictionsTest, pool.testEx)
        if iteration==0:
            log.info(f"Performance after training on the seed set on the Test set: {scoresTest}")
        else:
            log.info(f"Performance after training on the training set at iteration n'{iteration} on the Test set: {scoresTest}")
        self.evaluater.testPerf.addPerformance(iteration, scoresTest)
        


'''
Created on Oct 8, 2019

@author: davy
'''

import abc
from typing import List
from pandas.core.frame import DataFrame
import configparser

from Data.Pool import Pool
from Evaluation.EvaluaterInt import EvaluaterInt
from ActiveLearners.Classifiers.ClassifierInt import ClassifierInt
from ActiveLearners.Ensemblers.EnsemblerInt import EnsemblerInt
from ActiveLearners.Oracles.OracleInt import OracleInt

class ActiveLearnerInt(object):
    '''
    The interface all active learners should implement
    '''
    def __init__(self, config: configparser, ensembler:EnsemblerInt, classifiers: List[ClassifierInt], evaluater:EvaluaterInt = None, trainingArgs:dict=None):
        """
        :param config: the config file to access resources and paths
        :param ensembler: an ensembler to combine the decision of multiple classifiers, if only one classifier is used nothing is done
        :param classifiers: the list of classifiers to train with AL
        :param evaluater: the evaluater to evaluate the performances of the learner on each iteration, none if not needed
        :param trainingArgs: a dictionary containing options for the active learning algorithm
            - unlabledSetAnnotated: True,False, set to True if all examples of the unlabeled set have been pre-annotated, false otherwise (used for logging purpose)
            - incrementalLearning: True/False, if True, only the new examples annotated will be used for training during the current iteration, otherwise all examples annotated are used (i.e. seed + previous queried examples)
            - evaluateEachALIteration: True/False, if True, the Active learner evaluates its classification at each AL iteration on the validation and test sets
            - writePredictionsAtEachALIteration: True/False, if evaluateEachALIteration is True and the value is True, write the predictions of the learner on the test set at each AL iteration in the tmp folder
            - batchSizeExamplesQueried: int, the size of the batch of examples queried for annotation at each iteration of AL
            - biasTowardMinorityClass: int, if not None, the learner will select first the uncertain examples of the minority class (indicated by the int) when querying the most informative examples, if None
            - budget: int, how many unlabeled examples the oracle agrees to annotate before the end of the training
        """
        self.config = config
        self.ensembler = ensembler
        self.classifiers = classifiers
        self.evaluater = evaluater
        self.trainingArgs = trainingArgs
        assert "unlabledSetAnnotated" in trainingArgs.keys(), "I can't find 'unlabledSetAnnotated' options in the trainingArgs, check the code"
        assert "incrementalLearning" in trainingArgs.keys(), "I can't find 'incrementalLearning' options in the trainingArgs, check the code"
        assert "evaluateEachALIteration" in trainingArgs.keys(), "I can't find 'evaluateEachALIteration' options in the trainingArgs, check the code"
        assert "writePredictionsAtEachALIteration" in trainingArgs.keys(), "I can't find 'writePredictionsAtEachALIteration' options in the trainingArgs, check the code"
        assert "batchSizeExamplesQueried" in trainingArgs.keys(), "I can't find 'batchSizeExamplesQueried' options in the trainingArgs, check the code"
        self.ExQueriedBatchSize = trainingArgs["batchSizeExamplesQueried"]
        assert "biasTowardMinorityClass" in trainingArgs.keys(), "I can't find 'biasTowardMinorityClass' options in the trainingArgs, check the code (put None if not needed" 
        assert "budget" in trainingArgs.keys(), "I can't find 'budget' options in the trainingArgs, check the code"
        self.budget = trainingArgs["budget"]


    @abc.abstractmethod
    def getClassifiers(self) -> List[ClassifierInt]:
        '''
        :return the list of Classifiers used by the learner
        '''
        pass    
    
    @abc.abstractmethod
    def getMostInformativeExamples(self, pool: Pool, ExQueriedBatchSize: int) -> DataFrame:
        '''
        :param: pool with all unlabeled data
        :param: ExQueriedBatchSize the number of most informative examples returned to be annotated
        :return: a set of examples that should be the most informative to annotate
        '''
        pass
    
    @abc.abstractmethod
    def train(self, pool:Pool, oracle:OracleInt):
        '''
        Implement the main loop of active learning
        :param pool: a valid pool with training set starting with the seed, a validation and test set annotated as well as unlabeled data
        :param oracle: an oracle that can return the labels of the unlabeled data
        :return: models updated, trained with Active Learning
        '''
        pass
                
    @abc.abstractmethod
    def classify(self, examples: DataFrame) -> DataFrame:
        '''
        :param: examples to classify
        :return: a copy of the examples updated with n+1 columns: prediction and n columns certainty_label_n:
          - prediction: the best prediction made by the classifier, ie, the label with the highest certainty 
          - certainty_label_n: a column where the classifier assign the probability for the label n for each example
        '''
        pass

    @abc.abstractmethod
    def saveAt(self, iteration:int):
        '''
        Save the model for the iteration given in input.
        The model should be reloadable and ready for classification
        :return: model saved on disk in the tmp file defined in the config.
        '''
        pass

    @abc.abstractmethod    
    def load(self, models:dict):
        '''
        load the models used by the learner
        :param models: a dictionary name model -> path model
        '''
        pass
    
    

    
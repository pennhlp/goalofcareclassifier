'''
Created on Jul 10, 2020

@author: dweissen
'''

import logging as lg
log = lg.getLogger('BertNSPClassifier')

from Data.Pool import Pool
from ActiveLearners.Classifiers.ClassifierInt import ClassifierInt
from Properties import PropertiesInt

import os
import re
from tqdm import tqdm
import pandas as pd
from pandas.core import series
from pandas.core.frame import DataFrame
import numpy as np
import keras
from keras.callbacks import ModelCheckpoint
from keras_bert import load_trained_model_from_checkpoint

from keras_bert import Tokenizer
from keras import layers
from keras_bert import AdamWarmup, calc_train_steps
from keras.models import load_model
from keras_bert import get_custom_objects


class BertNSPClassifier(ClassifierInt):
    '''
    I reimplement Haitao's Bert based on keras-bert which gave good results on unbalanced corpus
    '''

    def __init__(self, name:str, properties:PropertiesInt, InitialModelsPath=None):
        '''
        :param: name, the name of the classifier for further reference
        :param: properties the properties to access BERT embeddings
        :param InitialModelsPath: the path to an initial Model (randomly initialized or pre-trained)
        '''
        super(BertNSPClassifier, self).__init__(name)
        log.info(f"A Bert classifier: {self.getName()} is used")

        self.performance = {}

        #tweets batch:64/len:64; webMD batch:16/len:256; batch:64/len:256 not enough memory...
        #good compromised could be batch:32/len:128
        self.batchSize = 64#16#64
        self.firstIterEpoch=3
        self.maxEpoch=3
        self.MAX_SEQ_LEN=256#64
        self.COLUMN_TEXT = 'text'
        self.COLUMN_LABEL = 'label'
        self.USER_NAME = ' USERNAME '
        self.URL = ' URL '        

        #self.bert_model_name="uncased_L-2_H-128_A-2" #tiny BERT to debug
        self.bert_model_name="uncased_L-12_H-768_A-12" #basic BERT for production
        self.bert_ckpt_dir = os.path.join(properties.BERTEmbedding, self.bert_model_name)
        self.bert_ckpt_file = os.path.join(self.bert_ckpt_dir, "bert_model.ckpt")
        self.bert_config_file = os.path.join(self.bert_ckpt_dir, "bert_config.json")
        self.bert_vocab_file = os.path.join(self.bert_ckpt_dir, "vocab.txt")
        
        token_dict = {}
        with open(self.bert_vocab_file, 'r', encoding='utf8') as reader:
            for line in reader:
                token = line.strip()
                token_dict[token] = len(token_dict)
        self.token_list = list(token_dict.keys())
        self.tokenizer = Tokenizer(token_dict)
        
        if not os.path.exists('/home/gesy17/'):
            self.checkPointPath = f'{properties.tmpPath}{self.getName()}_bestModel'
            self.initialModelPath = f'{properties.tmpPath}Initial_{self.getName()}'
            self.currentModelPath = f'{properties.tmpPath}Current_{self.getName()}'
            if InitialModelsPath is None:
                self.__buildNN()
            else:
                log.info(f"An existing model is given and will be loaded from {InitialModelsPath}.")
                self.initialModelPath = InitialModelsPath
                self.model = load_model(self.initialModelPath, custom_objects=get_custom_objects())
#                 self.model = keras.models.load_model(self.initialModelPath)
        
        #the list of historie return after each training
        self.histories = []
        
        
    def __buildNN(self, trainingSetSize = None):
        
        self.model = load_trained_model_from_checkpoint(
            self.bert_config_file,
            self.bert_ckpt_file,
            training=True,
            trainable=True,
            seq_len=self.MAX_SEQ_LEN,
        )

        inputs = self.model.inputs[:2]
        dense = self.model.get_layer('NSP-Dense').output
        # dense = layers.Lambda(lambda x: keras.backend.expand_dims(x, -1))(dense)
        # dense = layers.Conv1D(16, 5, activation='relu')(dense)
        # dense = layers.MaxPooling1D(pool_size=4)(dense)
        # dense = layers.Flatten()(dense)
        # print(dense.shape)
        # dense = layers.Dense(units=32,
        #                      activation='relu',
        #                      kernel_regularizer=regularizers.l2(0.003),
        #                      activity_regularizer=regularizers.l1(0.005))(dense)
        # dense = layers.BatchNormalization()(dense)
        dense = layers.Dropout(0.1)(dense)
        outputs = layers.Dense(units=2, activation='softmax')(dense)

        log.error("A verifier le nombre d'epoch de l'optimisateur, je met le nombre d'epoques sur le seed")
        
        if trainingSetSize==None:
            trainingSetSize = 9000
        decay_steps, warmup_steps = calc_train_steps(
                trainingSetSize,#trainLabel.shape[0],
                batch_size=self.batchSize,
                epochs=self.firstIterEpoch,
                )
        opt = AdamWarmup(decay_steps=decay_steps, warmup_steps=warmup_steps, lr=1e-4)
        #opt=keras.optimizers.Adam(1e-5)
        
        self.model = keras.models.Model(inputs, outputs)
        self.model.compile(opt, 'categorical_crossentropy', metrics=['accuracy'])
        self.model.summary()
        
        
    def __preprocess_sentences(self, texts):
        """
        Simple preprocessing of the sentences: lowercase and remove twitter users and urls
        """
        def __preprocess_text(text):
            if pd.isna(text):
                log.error("I found a text which was empty and transformed into nan by pandas, check data. I just retransform into an empty string")
                text = ""
            text = text.strip().lower();
            # Mask user names
            text = re.sub(r'(?<!\w)@[a-z0-9_]+', self.USER_NAME, text)
            # URLs
            text = re.sub(r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*(),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', self.URL, text)
            return text
        texts = texts.apply(__preprocess_text)
        return texts


    def __to_one_hot(self, input_labels, range_labels):
        num_rows = len(input_labels)
        ohe = np.zeros((num_rows, range_labels))
        ohe[np.arange(num_rows), input_labels] = 1
        return ohe
    
    
    def __generate_sentence_index(self, texts: series):
        """
        :param texts: the texts to map to the token IDs
        """
        #first rpreprocess
        texts = self.__preprocess_sentences(texts)
        #then tokenize
        input_ids = []
        for txt in tqdm(texts,position=0, leave=True):
            ids, _ = self.tokenizer.encode(txt, max_len=self.MAX_SEQ_LEN)
            input_ids.append(ids)
        input_ids = np.array(input_ids)
 
        return [input_ids, np.zeros_like(input_ids)]
        
#         indices = []
#         for text in texts[self.COLUMN_TEXT]:
#             ids, _ = self.tokenizer.encode(text, max_len=self.MAX_SEQ_LEN)
#             indices.append(ids)
#         indices = np.array(indices)
#         
#         return [indices, np.zeros_like(indices)], labels
    
    def __generate_label_oh(self, labels: series):
        """
        :param labels: the labels to map to a one hot vector
        """
        labels = self.__to_one_hot(labels, 2)
        return labels

    def train(self, pool: Pool):
        '''
        :param: use the most recent examples annotated from the pool to update the classifier model
        :param: evaluate, if True, the classifier will evaluate its score on the evaluation set and record its performance
                          if false, no evaluation is done
        :return: a model updated 
        '''
        trainPool=pool.getMostRecentTrainingExamples()
        log.info(f"I received {len(trainPool)} examples for training CNN. Use __trainCheckPoints")

        trainText=trainPool['text']
        trainLabel=trainPool['label']
#         trainText = trainText[:200]
#         trainLabel = trainLabel[:200]

        trainIndices=self.__generate_sentence_index(trainText)
        trainLabel=self.__generate_label_oh(trainLabel)
        assert len(trainLabel)==len(trainIndices[0]), f"number of training examples {len(trainIndices[0])} and labels {len(trainLabel)} are not equal, check the preprocessing in Bert."
        
        valTexts = pool.valEx['text']
        valLabels = pool.valEx['label']
#         valTexts = valTexts[:200]
#         valLabels = valLabels[:200]        
        valIndices=self.__generate_sentence_index(valTexts) 
        valLabels=self.__generate_label_oh(valLabels)
        assert len(valLabels)==len(valIndices[0]), f"number of validation examples {len(valIndices[0])} and labels {len(valLabels)} are not equal, check the preprocessing in Bert."
        
        if pool.getLastIteration()==0:
            epochNum = self.firstIterEpoch
            decay_steps, warmup_steps = calc_train_steps(
                trainLabel.shape[0],
                batch_size=self.batchSize,
                epochs=epochNum,
                )
            opt = AdamWarmup(decay_steps=decay_steps, warmup_steps=warmup_steps, lr=1e-4)
            self.model.compile(opt, 'categorical_crossentropy', metrics=['accuracy'])

            checkpoint = ModelCheckpoint(self.checkPointPath, monitor = 'val_loss', verbose = 1, save_best_only = True, mode = 'min')
            #history = self.model.fit(trainIndices, trainLabel, validation_data = (valIndices, valLabels), batch_size=self.batchSize, epochs=epochNum, shuffle=True, verbose=1)
            history = self.model.fit(trainIndices, trainLabel, validation_data = (valIndices, valLabels), batch_size=self.batchSize, epochs=epochNum, shuffle=True, verbose=1, callbacks = [checkpoint])
            #history = self.model.fit(trainIndices, trainLabel, validation_data = (valIndices, valLabels), batch_size=self.batchSize, epochs=epochNum, shuffle=True, verbose=1, callbacks = [checkpoint], class_weight=class_weight)
            self.model = load_model(self.checkPointPath, custom_objects=get_custom_objects())
        else:
            epochNum = self.maxEpoch
            #Reset the optimizer with the actual epochs and training size
            decay_steps, warmup_steps = calc_train_steps(
                trainLabel.shape[0],
                batch_size=self.batchSize,
                epochs=epochNum,
                )
            opt = AdamWarmup(decay_steps=decay_steps, warmup_steps=warmup_steps, lr=1e-4)
            self.model.compile(opt, 'categorical_crossentropy', metrics=['accuracy'])
            checkpoint = ModelCheckpoint(self.checkPointPath, monitor = 'val_loss', verbose = 1, save_best_only = True, mode = 'min')
            #checkpoint = ModelCheckpoint(filepath=self.checkPointPath, monitor = 'val_loss', verbose = 1, save_best_only = True, mode = 'min')
            #history = self.model.fit(trainIndices, trainLabel, validation_data = (valIndices, valLabels), batch_size=self.batchSize, epochs=epochNum, shuffle=True, verbose=1, callbacks = [checkpoint], class_weight=class_weight)
            history = self.model.fit(trainIndices, trainLabel, validation_data = (valIndices, valLabels), batch_size=self.batchSize, epochs=epochNum, shuffle=True, verbose=1, callbacks = [checkpoint])
            self.model = load_model(self.checkPointPath, custom_objects=get_custom_objects())
        self.model.save(self.checkPointPath+f'_ALiteration{pool.getLastIteration()}')

        self.histories = [history]
        
        
    def classify(self, examples: DataFrame) -> DataFrame:
        '''
        :param: classify the examples given
        :return: a copy of the examples updated with 2 columns label and certainty:
          - prediction: the best prediction made by the classifier, ie, the label with the highest certainty 
          - certainty: a dictionary where each possible label is assigned a probability
        '''
        log.info(f"BertClassifier assigns labels and certainty to the {len(examples)} examples")
        predExamples = examples.copy()
        predText=predExamples['text']
        predIndices=self.__generate_sentence_index(predText)
        predLabel=self.model.predict(predIndices, batch_size=self.batchSize, verbose=1)
        
        predExamples['certainty_label_0'] = predLabel[:,0]
        predExamples['certainty_label_1'] = predLabel[:,1]        
        predLabel = np.argmax(predLabel, axis=1)
        predExamples['prediction']=predLabel

        return predExamples
        
        
    def evaluate(self, examplesPredicted: DataFrame, examplesTruth: DataFrame) -> dict:
        '''
        evaluate the performance of the classifier on the set of examples given
        :param: examplesPredicted, the predictions made by the classifier, the df should have a column 'label' where the predictions are store
        :param: examplesTruth, the true values of the examples also store in a column label, both set of examples should have the same docIDs
        :return: P,R,F1 score
        '''
        raise Exception("This function has not been implemented.")
        
        
    def __getScores__(self, examplesTruth: DataFrame, examplesPredicted: DataFrame) -> dict:
        '''
        :param: examplesTruth a dataframe with the docIDs and label  
        :param: examplesPredicted a dataframe with the docIDs and prediction
        :return: list with the Precision, Recall, F1 scores computed
        '''
        raise Exception("This function has not been implemented.")
        
        
    def getPerformanceHistory(self) -> dict:
        '''
        :return: a dictionary of all evaluations made on the evaluation set after training for each iteration (empty if no evaluation were ran after training)
        '''
        return self.performance
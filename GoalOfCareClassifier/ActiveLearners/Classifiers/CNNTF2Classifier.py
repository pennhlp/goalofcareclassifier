'''
Created on Dec 12, 2019

@author: dweissen
'''

from __future__ import absolute_import, division, print_function, unicode_literals

import configparser
import logging as lg
log = lg.getLogger('CNNClassifier')
from ActiveLearners.Classifiers.ClassifierInt import ClassifierInt
from Data.Pool import Pool
from TextProcessor.Wor2VecProcessor import Word2VecPreprocessor
from deprecated import deprecated
import os
# os.environ["MKL_THREADING_LAYER"] = "GNU"
# os.environ['KERAS_BACKEND']='theano'
import keras
from keras import backend as K
from keras.layers import *
from keras.callbacks import ModelCheckpoint, History

import tensorflow as tf
# from tensorflow.keras import datasets, layers, models, backend 
from keras.utils.np_utils import to_categorical
import numpy as np
from pandas.core.frame import DataFrame



class CNNTF2Classifier(ClassifierInt):
    '''
    I reimplement a CNN in TF2, this may help with the memory leak problem...
    '''
    def __init__(self,  name:str, preprocessor: Word2VecPreprocessor, config: configparser, InitialModelsPath=None):
        '''
        :param: name, the name of the classifier for further reference
        :param: preprocessor an access to the word2vect embeddings the classifier needs
        :param: initialModel if none, the CNNClassifier will compile a new network, if a path to a h5 describing a valid model is given, the model is loaded instead 
        :return: save the model compiled in /tmp/Initial_self.getName().h5
        '''
        super(CNNTF2Classifier, self).__init__(name)
        log.info(f"A CNN classifier: {self.getName()} is used")
        
        self.batchSize = 64
        self.maxEpoch=2
        self._preprocesser = preprocessor
        self.config = config
        
        self.InitialCheckpointPath = f'{config["Data"]["tmpFolder"]}Initial_{self.getName()}.h5'
        self.CurrentCheckpointPath = f'{config["Data"]["tmpFolder"]}Current_{self.getName()}.h5'
        if InitialModelsPath is None:
            self.__buildCNN()
        else:
            log.info(f"An existing model is given and will be loaded from {InitialModelsPath}.")
            self.initialModelPath = InitialModelsPath
            self.model = keras.models.load_model(self.initialModelPath)
        
        #the list of historie return after each training
        self.histories = []
    
    
    def __getCopyInitialModel(self):
        '''
        :return: a copy of the initial model given or build, just read the model from the disk
        '''
        return keras.models.load_model(self.initialModelPath)
    
    def __buildCNN(self):
        '''
        Build and compile the architecture of the CNN
        '''
        self.myadam = tf.keras.optimizers.Adam(lr=0.001, amsgrad=True)
        self.text_input = keras.layers.Input(shape=(100,), dtype='int32') 
        self.text_embedding_layer = keras.layers.Embedding(input_dim = self._preprocesser.lister.shape[0], output_dim = 400, weights = [self._preprocesser.lister], trainable = True)

        self.embedded_text = self.text_embedding_layer(self.text_input)
        
        self.d_embedded_text = keras.layers.Dropout(0.2)(self.embedded_text)

        self.cnn1 = keras.layers.Conv1D(padding="same", activation="relu", strides=1, filters=400, kernel_size=3)(self.d_embedded_text)
        self.d_cnn1 = keras.layers.Dropout(0.2)(self.cnn1)
        self.p = keras.layers.GlobalMaxPooling1D()(self.d_cnn1)
        # self.cp=concatenate([self.p,Flatten()(self.d_embedded_ease),Flatten()(self.d_embedded_effect),Flatten()(self.d_embedded_satis)],axis=-1)

        self.dense = keras.layers.Dense(200, activation='relu')(self.p)
        self.final = keras.layers.Dense(1, activation='sigmoid')(self.dense)
        self.model = keras.models.Model(self.text_input, self.final)
        # self.model = Model([self.text_input,self.ease_input,self.effect_input,self.satis_input], self.final)
        self.model.compile(loss='BinaryCrossentropy', optimizer=self.myadam, metrics=[tf.keras.metrics.TruePositives(), tf.keras.metrics.TrueNegatives(), tf.keras.metrics.Precision(), tf.keras.metrics.Recall()])
        self.model.save(f'/tmp/Initial_{self.getName()}.h5')


    def train(self, trainExamples: DataFrame, evalExamples: DataFrame, checkPointPath:str):
        '''
        :param trainExamples: train the classifier on the training examples given
        :param evalExamples: the validation set used during the training
        :param checkPointPath: a path to the current checkpoint of the model, None if the classifier does not allowed incremental training
        :return: a model updated
        '''
        history = self.__trainCheckPoints(trainExamples, evalExamples, checkPointPath)
        #take too much memory, since the history keep the annotations
        #self.histories.append(history)
        self.histories = [history]
                
        
    def __trainCheckPoints(self, trainExamples: DataFrame, evalExamples: DataFrame, checkPointPath:str) -> History:
        '''
        Train the model and outputs a check point to restart training the model between each iteration, use only the last examples annotated
        Load the best model found after the iteration 
        '''

        trainIndices=self._preprocesser.generate_word_index(trainExamples['text'])
        trainIndices=np.array(trainIndices)
        trainLabel=np.array(trainExamples['label'])
        
        valIndices=self._preprocesser.generate_word_index(evalExamples['text'])
        valIndices=np.array(valIndices)
        valLabels=np.array(evalExamples['label'])

        log.debug(f"start training classifier {self.getName()}")        
        checkpoint = ModelCheckpoint(checkPointPath, monitor = 'val_loss', verbose = 1, save_best_only = True, mode = 'min')
        history = self.model.fit(trainIndices, trainLabel, validation_data = (valIndices, valLabels), batch_size=self.batchSize, epochs=self.maxEpoch, shuffle=False, verbose=1, callbacks = [checkpoint])
        #history = self.model.fit(trainIndices, trainLabel, validation_data = (valIndices, valLabels), batch_size=self.batchSize, epochs=self.maxEpoch, shuffle=False, verbose=1, callbacks = [checkpoint], class_weight={0:1, 1:1})
        log.debug(f"classifier {self.getName()} is trained.")
        # the model fit and keep the best model (based on its performance on the evaluation set)
        self.model = keras.models.load_model(checkPointPath)

        self.model.save(self.getCurrentCheckpointPath())
        log.debug(f"Model saved @{self.getCurrentCheckpointPath()}.")
        #history = []
        return history
    
    
    def classify(self, examples: DataFrame) -> DataFrame:
        '''
        :param: classify the examples given
        :return: a copy of the examples updated with 2 columns label and certainty:
          - prediction: the best prediction made by the classifier, ie, the label with the highest certainty 
          - certainty_label_n: a column where the classifier assign the probability for the label n for each example
        '''
        log.info(f"CNN assigns labels and certainty to the {len(examples)} examples")
        evalExamples = examples.copy()
        evalText=evalExamples['text']
        evalIndices=self._preprocesser.generate_word_index(evalText)
        evalIndices=np.array(evalIndices)
        evalLabel=self.model.predict(evalIndices,batch_size=self.batchSize,verbose=1)
        evalExamples['certainty_label_0'] = evalLabel[:,0]
        evalExamples['certainty_label_1'] = 1-evalLabel[:,0]        
        evalExamples['prediction']=0
        evalExamples['prediction'].where(cond=evalExamples['certainty_label_0']<0.5, other=1, inplace=True)

        return evalExamples    
    
    
    def load(self, pathModel):
        self.model = keras.models.load_model(pathModel)
        log.debug(f"Model loaded for prediction from @{pathModel}.")
        
        
    def save(self, iteration: int):
        '''
        Save the model at the iteration in the tmp folder
        '''
        log.debug(f"classifier {self.getName()} is trained.")
        pathFile = self.config["Data"]["tmpFolder"]+self.getName()+"_I"+str(iteration)+".pkl"
        self.model.save(pathFile)
        log.debug(f"Model saved @pathFile.")    
    
    
    def getInitialCheckpointPath(self) -> str:
        return self.InitialCheckpointPath
    
    
    def getCurrentCheckpointPath(self) -> str:
        return self.CurrentCheckpointPath

'''
Created on Oct 8, 2019

@author: davy
'''

import abc
from pandas.core.frame import DataFrame
from Data.Pool import Pool

class ClassifierInt(object):
    '''
    Define the basic services for a classifier
    '''
    def __init__(self, name:str):
        """
        :param: name, the name of the classifier for further reference
        """
        self.name = name
        
        
    def getName(self) -> str:
        """
        :return: the name of the classifier
        """
        return self.name

    @abc.abstractmethod
    def train(self, trainExamples: DataFrame, evalExamples: DataFrame, checkPointPath:str):
        '''
        :param trainExamples: train the classifier on the training examples given
        :param evalExamples: the validation set used during the training
        :param checkPointPath: a path to the current checkpoint of the model, None if the classifier does not allowed incremental training
        :return: a model updated
        '''
        pass
    
    @abc.abstractmethod
    def classify(self, examples: DataFrame) -> DataFrame:
        '''
        :param: examples to classify
        :return: a copy of the examples updated with n+1 columns: prediction and n columns certainty_label_n:
          - prediction: the best prediction made by the classifier, ie, the label with the highest certainty 
          - certainty_label_n: a column where the classifier assign the probability for the label n for each example
        '''
        pass

    @abc.abstractmethod
    def save(self, iteration: int):
        '''
        Save the model at the iteration in the tmp folder
        '''
        pass
    
    @abc.abstractmethod
    def load(self, pathModel):
        '''
        Load a valid model
        :param pathModel: a path to the model to load 
        '''
        pass
    
    @abc.abstractmethod
    def getInitialCheckpointPath(self) -> str:
        '''
        :return: the path to the initial model checkpoint (so the model initially instantiated, None if the classifier does not support checkpoints)
        '''    
        pass
    
    @abc.abstractmethod
    def getCurrentCheckpointPath(self) -> str:
        '''
        :return: the path to the current model checkpoint (not necessarily the best model, just the current model from the last AL iteration, None if the classifier does not support checkpoints)
        '''
        pass
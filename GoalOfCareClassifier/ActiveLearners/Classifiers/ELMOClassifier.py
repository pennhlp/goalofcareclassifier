'''
Created on Oct 25, 2019

@author: davy
'''

import logging as lg
log = lg.getLogger('ELMOClassifier')
from typing import List
from pathlib import Path
from pandas.core.frame import DataFrame
import numpy as np
from flair.data_fetcher import NLPTaskDataFetcher
from flair.embeddings import WordEmbeddings, FlairEmbeddings, DocumentLSTMEmbeddings, DocumentPoolEmbeddings
from flair.models import TextClassifier
from flair.trainers import ModelTrainer
from flair.data import Corpus, Sentence
from flair.datasets import ClassificationCorpus
from sklearn.metrics import precision_score, recall_score, f1_score, confusion_matrix

from ActiveLearners.Classifiers.ClassifierInt import ClassifierInt
from Data.Pool import Pool
from Properties import PropertiesInt

class ELMOClassifier(ClassifierInt):
    '''
    A basic ELMO classifier build with FLAIR
    '''

    def __init__(self, name:str, properties: PropertiesInt):
        '''
        :param: name, the name of the classifier for further reference, should be unique (otherwise their respective models will be erased...)
        :param: properties
        '''
        super(ELMOClassifier, self).__init__(name)
        log.info(f"An ELMO classifier: {self.getName()} is used.")
        #A map to record the performance of the classifier after each training iteration
        #iteration -> {precision,recall,f1}
        self.performance = {}
        # a reference to the properties object
        self.properties = properties
        # the name of the model trained
        self.ModelName = f'{self.getName()}.md'
        
        self.MAX_EPOCHS = 2
        
    def train(self, pool: Pool, evaluate: bool):
        '''
        :param: pool, use the most recent examples annotated from the pool to update the classifier model
        :param: evaluate, if True, the classifier will evaluate its score on the evaluation set and record its performance
        :return: a model updated
        '''
        self.__train(pool)
        if evaluate:
            log.info("I am evaluating my performance on the evaluation set after my training.")
            valExPredicted = self.classify(pool.valEx)
            perf = self.evaluate(valExPredicted, pool.valEx)
            self.performance[pool.getLastIteration()] = perf

            
    def __train(self, pool: Pool):
        '''
        '''
        log.error("TODO: implement the training algo for ELMO.")
        
        log.debug("TODO: I'm in rush so I write the file on the disk and reload them, the corpus should be created directly from the dataframe.")
        self.__dfToFlairTSV(pool.getMostRecentTrainingExamples()[['label','text']].copy(), f'/tmp/{self.getName()}_train.csv')
        self.__dfToFlairTSV(pool.valEx[['label','text']].copy(), f'/tmp/{self.getName()}_val.csv')

        # !!! here I just use the validation set for testing...
        log.error("Check here the validation corpus we want to use...")
        corpus = NLPTaskDataFetcher.load_classification_corpus(Path('/tmp/'), test_file=f'{self.getName()}_val.csv', dev_file=f'{self.getName()}_val.csv', train_file=f'{self.getName()}_train.csv')
        log.info(corpus)
         
        if Path(self.properties.classifierModelsPath+self.ModelName).exists():
            #https://github.com/zalandoresearch/flair/blob/master/resources/docs/TUTORIAL_7_TRAINING_A_MODEL.md
            log.info(f"I am continuing training a model from checkpoint @{self.properties.classifierModelsPath}{self.ModelName}")
            log.fatal("TODO: come back from here!")
            #model: TextClassifier = TextClassifier.load(self.properties.classifierModelsPath+self.ModelName+'/best-model.pt')
            trainer = ModelTrainer.load_checkpoint(self.properties.classifierModelsPath+self.ModelName+'/checkpoint.pt', corpus)
            log.info("Start to trained the model on training and validation set...")           
            trainer.train(self.properties.classifierModelsPath+self.ModelName, max_epochs=self.MAX_EPOCHS, checkpoint=True)
            log.info("Model trained and saved @{}".format(f"{self.properties.classifierModelsPath}{self.ModelName}"))
        else:
            log.info(f"No model found @{self.properties.classifierModelsPath}{self.ModelName}, create one.")
            trainer = self.__buildClassifier(corpus)
            
            log.info("Start to trained the model on training and validation set...")
            trainer.train(self.properties.classifierModelsPath+self.ModelName, max_epochs=self.MAX_EPOCHS, checkpoint=True)
            log.info("Model trained and saved @{}".format(f"{self.properties.classifierModelsPath}{self.ModelName}"))

        
    def __buildClassifier(self, corpus: Corpus) -> ModelTrainer:
        '''
        Build the classifier and return it
        :return: trainer
        '''
        #word_embeddings = [WordEmbeddings('glove'), FlairEmbeddings('news-forward-fast'), FlairEmbeddings('news-backward-fast')]            
        #document_embeddings = DocumentLSTMEmbeddings(word_embeddings, hidden_size=512, reproject_words=True, reproject_words_dimension=256)
        embeddings = WordEmbeddings('glove')
        document_embeddings = DocumentPoolEmbeddings([embeddings], fine_tune_mode='nonlinear')
        classifier = TextClassifier(document_embeddings, label_dictionary=corpus.make_label_dictionary(), multi_label=False)
        trainer = ModelTrainer(classifier, corpus)
        
        return trainer

        
    def __dfToFlairTSV(self, examples: DataFrame, tsvPath: str):
        '''
        :param: examples, as a dataframe with the only 2 columns label, text
        :return: write the df as a properly formated tsv for FLAIR
        '''
        examples['label'] = '__label__'+examples['label'].astype(str)
        examples['text']
        examples.to_csv(path_or_buf=tsvPath, sep='\t', index=False, header=False)
        

    def classify(self, examples: DataFrame) -> DataFrame:
        '''
        :param: classify the examples given
        :return: a copy of the examples updated with 1 columns label and n columns certainty_label_n:
          - prediction: the best prediction made by the classifier, ie, the label with the highest certainty 
          - certainty_label_n: a column where the classifier assign the probability for the label n for each example
        '''
        model: TextClassifier = TextClassifier.load(self.properties.classifierModelsPath+self.ModelName+'/best-model.pt')
        sentences = [Sentence(str(sent)) for sent in examples['text'].tolist()]
        predictions = model.predict(sentences)
        certainty_label_0 = []
        labels = []
        for pred in predictions:
            labels.append(pred.labels[0].value)
            if pred.labels[0].value=='0':
                certainty_label_0.append(pred.labels[0].score)
            elif pred.labels[0].value=='1':
                certainty_label_0.append(1-pred.labels[0].score)
            else:
                raise Exception(f'I received the label {pred.labels[0]} from the prediction of Flair when I was expecting only 0 or 1, check the code.')
        examples['prediction'] = labels
        examples['certainty_label_0'] = certainty_label_0
        examples['certainty_label_1'] = 1-examples['certainty_label_0'] 
        
        return examples
    
    
    def evaluate(self, examplesPredicted: DataFrame, examplesTruth: DataFrame) -> List[float]:
        '''
        evaluate the performance of the classifier on the set of examples given
        :param: examplesPredicted, the predictions made by the classifier, the df should have a column 'label' where the predictions are store
        :param: examplesTruth, the true values of the examples also store in a column label, both set of examples should have the same docIDs
        :return: P,R,F1 score
        '''
        assert len(examplesPredicted.index.intersection(examplesTruth.index))==len(examplesPredicted.index), f"There is no index mapping between the predicted and the truth set of examples, check the code."
        assert 'label' in examplesPredicted.columns and 'label' in examplesTruth.columns, "The examples predicted or gold truth do not have the expected label columns, check code."
        
        log.fatal("Triple check the evaluation here.")
        return self.__getScores__(examplesTruth, examplesPredicted)
        
    def __getScores__(self, examplesTruth: DataFrame, examplesPredicted: DataFrame) -> dict:
        '''
        :param: examplesTruth a dataframe with the docIDs and label  
        :param: examplesPredicted a dataframe with the docIDs and label
        :return: list with the Precision, Recall, F1 scores computed
        '''        
        #df = DataFrame({'docIDTruth':examplesTruth['docID'], 'truths':examplesTruth['label'], 'docIDPredicted':examplesPredicted['docID'], 'preds':examplesPredicted['label']})
        df = DataFrame({'docIDTruth':examplesTruth.index, 'truths':examplesTruth['label'], 'docIDPredicted':examplesPredicted.index, 'preds':examplesPredicted['label']})
        assert(np.array_equal(df['docIDTruth'], df['docIDPredicted'])), "I have differences between the lists of docIDs from the truth and the predicted which should be identical lists, check the code..."
        df.to_csv("/tmp/RandomClassifierEvaluationOutput.tsv", sep='\t')
        print(confusion_matrix(list(df['truths']), list(df['preds'])))
        #precision, recall, f1, support
        prec = precision_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
        rec = recall_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
        f1 = f1_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
        
        return {'precision':prec, 'recall':rec, 'f1':f1}

        
    def getPerformanceHistory(self) -> dict:
        '''
        :return: a dictionary of all evaluations made on the evaluation set after training for each iteration (empty if no evaluation were ran after training)
        '''
        return self.performance
    
    
'''
Created on Nov 19, 2021

@author: dweissen
'''
import configparser
import logging as log
from pandas.core.frame import DataFrame

from flair.data import Sentence
from flair.embeddings import TransformerDocumentEmbeddings
from flair.data import Corpus
from flair.datasets import CSVClassificationCorpus
from flair.models import TextClassifier
from flair.trainers import ModelTrainer

from ActiveLearners.Classifiers.ClassifierInt import ClassifierInt
from Data.Pool import Pool


class FlairClassifier(ClassifierInt):
    '''
    classdocs
    '''


    def __init__(self, config: configparser):
        '''
        Constructor
        '''
        self.config = config
        self.embedding = TransformerDocumentEmbeddings('bert-base-uncased', fine_tune=True)
    
    def train(self, train:DataFrame, val:DataFrame, test:DataFrame, column_name_map:dict, label_type="label"):
        """
        Regular Flair training using the 
        """        
        #Flair is expecting the data in a folder
        train.to_csv(self.config['Data']['tmpFolder']+'/flairFolder/train.csv', sep='\t')
        val.to_csv(self.config['Data']['tmpFolder']+'/flairFolder/dev.csv', sep='\t')
        test.to_csv(self.config['Data']['tmpFolder']+'/flairFolder/test.csv', sep='\t')         
        # load corpus containing training, test and dev data and if CSV has a header, you can skip it
        corpus: Corpus = CSVClassificationCorpus(data_folder=self.config['Data']['tmpFolder']+'flairFolder/', train_file="train.csv", test_file="test.csv", dev_file="dev.csv", column_name_map=column_name_map, skip_header=True, delimiter='\t', label_type="label")
        classifier = TextClassifier(self.embedding, label_dictionary=corpus.make_label_dictionary(label_type="label"), label_type="label")
        trainer = ModelTrainer(classifier, corpus)
        trainer.fine_tune(self.config['Data']['tmpFolder']+'/flairFolder/', learning_rate=5.0e-5, mini_batch_size=4, max_epochs=10, checkpoint=True) 
        return
    
    def pretrain(self, pretrain:DataFrame, val:DataFrame):
        print("Hello!")
        #checkpoint=True,
        return
        
    def classify(self, examples: DataFrame) -> DataFrame:
        log.info("Start predicting the labels of unknown dataset...")
        classifier = TextClassifier.load(self.config['Data']['tmpFolder']+'/flairFolder/final-model.pt')
        sentence = Sentence("goals are restorative")
        classifier.predict(sentence)
        print(sentence.labels)
        return
'''
Created on Nov 19, 2021

@author: sidrawal
Logistic regression classifier with TF-IDF.
Used this notebook as a guide:
https://www.kaggle.com/kashnitsky/logistic-regression-tf-idf-baseline/notebook
'''
import configparser
import logging as lg
log = lg.getLogger('GOCLogRegClassifier')
from pandas.core.frame import DataFrame
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_score, StratifiedKFold
from sklearn.feature_extraction.text import TfidfVectorizer
import pickle as pkl

from ActiveLearners.Classifiers.ClassifierInt import ClassifierInt



class GOCLogRegClassifier(ClassifierInt):
    '''
    A logistic Regression with N-gram filtered by tf-idf scores
    '''

    def __init__(self, name: str, config: configparser):
        '''
        Constructor, create sklearn LogisticRegression classifier
        '''
        super(GOCLogRegClassifier, self).__init__(name)
        self.name = name
        self.config = config
        self.text_transformer = TfidfVectorizer(stop_words='english', ngram_range=(1, 2), lowercase=True, max_features=150000)
        self.classifier = LogisticRegression(random_state=0)


    def getName(self):
        """
        :return: the name of the classifier
        """
        return self.name

    def train(self, train: DataFrame, val: DataFrame = None, checkPointPath: str = None):
        """
        Train logistic regression classifier
        """
        log.debug(f"start training the {self.getName()}")
        train_tfidf = self.text_transformer.fit_transform(train['text'].values.astype('str'))
        self.classifier.fit(train_tfidf, train['label'])
        log.debug(f"{self.getName()} is trained.")
        return

    def classify(self, examples: DataFrame) -> DataFrame:
        log.info("Start predicting the labels of unknown dataset...")
        evalExamples = examples.copy()
        examples_TFIDF = self.text_transformer.transform(evalExamples['text'].values.astype('str'))
        preds = self.classifier.predict(examples_TFIDF)
        probs = self.classifier.predict_proba(examples_TFIDF)
        evalExamples['prediction'] = preds
        evalExamples['certainty_label_0'], evalExamples['certainty_label_1'] = probs[:, 0], probs[:, 1]
        return evalExamples


    def save(self, iteration: int):
        pkl.dump(self.classifier, open(self.config["Data"]["tmpFolder"]+self.getName()+"_I"+str(iteration)+".pkl", 'wb'))
    
    def load(self, pathModel:str):
        self.classifier = pkl.load(open(pathModel, 'rb'))
        
    def getInitialCheckpointPath(self) -> str:
        '''
        :return: the path to the initial model checkpoint (so the model initially instantiated, None if the classifier does not support checkpoints)
        '''    
        return None
    
    def getCurrentCheckpointPath(self) -> str:
        '''
        :return: the path to the current model checkpoint (not necessarily the best model, just the current model from the last AL iteration, None if the classifier does not support checkpoints)
        '''
        return None
    

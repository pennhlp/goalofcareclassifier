'''
Created on Jan 30, 2020

@author: Ari Klein/Davy Weissenbacher/Sid


A set of regular expressions to find mention of medication changes to be tested...

Feb 23, 2022:
Update for GOC detection: Added a set of regular expressions to find mentions of goal-of-care discussion in clinical notes
'''

import configparser
import logging as lg

log = lg.getLogger('RegExFinder')
import re
import pandas as pd
from pandas.core import series
from pandas.core.series import Series
from pandas.core.frame import DataFrame

from Data.Pool import Pool
from ActiveLearners.Classifiers.ClassifierInt import ClassifierInt
# from Performance.Performance import Scores
# from keras.callbacks import History
# coding=utf-8
import sys
import csv

# reload(sys)
# sys.setdefaultencoding('utf8')

REGEXES_GOC = [
    # r'\b(stated|said|explained|asked) (\w+\s){,10}(goals?|comfort|care|intubated?|intubation|extubated?|extubation|resuscitated?|resuscitation|therapy|dependent|dependency|home|family)\b',
    r'\b(goals?|comfort|care|intubated?|intubation|extubated?|extubation|resuscitated?|resuscitation|dependent|dependency|home|family) (\w+\s){,10}(responded|mentioned|brought up|suggested|consulted)\b',
    r'\b(?<!need for)(?<!want for)(?<!plan for) (a |an )?(explanation|discussion|meeting)\b',

    r'\b(talked|talking|spoke|speaking) (to|with) (patient(\'s)?|pt|pt.|family|he|she|they|brothers?|sisters?|siblings?|sons?|daughters?|nieces?|nephews?|child|children|mother|father|parents?)\b',
    r'\b(talked|talking|spoke|speaking) (to|with) (child|children|mother|father|parents?|wife|husband)\b',

    r'\b(communication|discussion|meeting) (from|with) (patient(\'s)|pt|pt.|family|him|her|them|brothers?|sisters?|siblings?|sons?|daughters?|nieces?|nephews?)\b',
    r'\b(communication|discussion|meeting) (from|with) (child|children|mother|father|parents?|wife|husband)\b',

    r'\b(plans?|goals?) (\w+\s){,5}(hospice|restorative|comfort|care|measures|palliative|home|family)\b',
    r'\b(plans?|goals?) (\w+\s){,5}(re)?-?(intubation|intubate|resuscitate|resuscitation|CPR)\b',

    # Added from APC train notes
    # Notes: (\w+\s) was not capturing words with non-word characters such as patient's, so I changed it to (\w|\'|\.)
    r'\b(expressed|voiced|vocalized|verbalized|stated|told|said|explained|asked|discussed|reiterated|notified|shared) ((\w|\')+\s){,10}(re)?-?(intubated?|intubation|extubated?|extubation)\b',
    r'\b(expressed|voiced|vocalized|verbalized|stated|told|said|explained|asked|discussed|reiterated|notified|shared) ((\w|\')+\s){,10}(CPR|DNR|DNI|comfort|care|resuscitat(ed|ion)?|dialysis)\b',
    r'\b(expressed|voiced|vocalized|verbalized|stated|told|said|explained|asked|discussed|reiterated|notified|shared) ((\w|\')+\s){,10}(therapy|chemo(therapy)?|interventions?)\b',
    r'\b(expressed|voiced|vocalized|verbalized|stated|told|said|explained|asked|discussed|reiterated|notified|shared) ((\w|\')+\s){,10}(prognosis|trajectory|progression|progressive|severity)\b',
    r'\b(expressed|voiced|vocalized|verbalized|stated|told|said|explained|asked|discussed|reiterated|notified|shared) ((\w|\')+\s){,10}(condition|deteorate|deteorating|point|state)\b',
    r'\b(expressed|voiced|vocalized|verbalized|stated|told|said|explained|asked|discussed|reiterated|notified|shared) ((\w|\')+\s){,10}(wish|wishes|goals?|desires?|hopes?)\b',
    r'\b(expressed|voiced|vocalized|verbalized|stated|told|said|explained|asked|discussed|reiterated|notified|shared) ((\w|\')+\s){,10}(dependent|dependency|home|family|time)\b',
    r'\b(expressed|voiced|vocalized|verbalized|stated|told|said|explained|asked|discussed|reiterated|notified|shared) ((\w|\')+\s){,10}(sustain(ed)?|ventilator|ventilation|hospice|dying|risk)\b',
    r'\b(expressed|voiced|vocalized|verbalized|stated|told|said|explained|asked|discussed|reiterated|notified|shared) ((\w|\')+\s){,10}(concerns?|issues?|worry|worries|problems?|understanding)\b',
    r'\b(expressed|voiced|vocalized|verbalized|stated|told|said|explained|asked|discussed|reiterated|notified|shared) ((\w|\')+\s){,10}(life support|breathing (tube|machine))\b',

    # Present tense of previous block of REs
    r'\b(express(es)?|voices?|vocalizes?|verbalizes?|states?|tells?|says?|explains?|asks?|discuss(es)?|reiterates?) ((\w|\')+\s){,10}(re)?-?(intubated?|intubation|extubated?|extubation)\b',
    r'\b(express(es)?|voices?|vocalizes?|verbalizes?|states?|tells?|says?|explains?|asks?|discuss(es)?|reiterates?) ((\w|\')+\s){,10}(CPR|DNR|DNI|comfort|care|resuscitat(ed|ion)?|dialysis)\b',
    r'\b(express(es)?|voices?|vocalizes?|verbalizes?|states?|tells?|says?|explains?|asks?|discuss(es)?|reiterates?) ((\w|\')+\s){,10}(therapy|chemo(therapy)?|interventions?)\b',
    r'\b(express(es)?|voices?|vocalizes?|verbalizes?|states?|tells?|says?|explains?|asks?|discuss(es)?|reiterates?) ((\w|\')+\s){,10}(prognosis|trajectory|progression|progressive|severity)\b',
    r'\b(express(es)?|voices?|vocalizes?|verbalizes?|states?|tells?|says?|explains?|asks?|discuss(es)?|reiterates?) ((\w|\')+\s){,10}(condition|deteorate|deteorating|decline|declining)\b',
    r'\b(express(es)?|voices?|vocalizes?|verbalizes?|states?|tells?|says?|explains?|asks?|discuss(es)?|reiterates?) ((\w|\')+\s){,10}(wish|wishes|goals?|desires?|hopes?|dependent|dependency)\b',
    r'\b(express(es)?|voices?|vocalizes?|verbalizes?|states?|tells?|says?|explains?|asks?|discuss(es)?|reiterates?) ((\w|\')+\s){,10}(sustain(ed)?|ventilator|ventilation|hospice|dying|risk)\b',
    r'\b(express(es)?|voices?|vocalizes?|verbalizes?|states?|tells?|says?|explains?|asks?|discuss(es)?|reiterates?) ((\w|\')+\s){,10}(concerns?|issues?|worry|worries|problems?)\b',
    r'\b(express(es)?|voices?|vocalizes?|verbalizes?|states?|tells?|says?|explains?|asks?|discuss(es)?|reiterates?) ((\w|\')+\s){,10}(home|family|time)\b',


    # If discussion verb starts with "I", we do not need any other term
    r'\bI (expressed|voiced|vocalized|verbalized|stated|told|said|explained|asked|discussed|reiterated|offered|suggested|encouraged|introduced|confirmed)\b',

    # Discussion may also appear in the form "in discussing..."
    r'\bin (expressing|voicing|vocalizing|verbalizing|stating|telling|saying|explaining|asking|discussing|reiterating)\b',

    r'\b(wants?(ed)?(ing)?|wish(es)?(ed)?|(would|did)( not)? (want|like)|desires?|hopes?|hoping|favors?|undecided|interested) (\w+\s){,7}(re)?-?(intubat(ed|ion)|extubat(ed|ion)|DNR|DNI|CPR|ICD)\b',
    r'\b(wants?(ed)?(ing)?|wish(es)?(ed)?|(would|did)( not)? (want|like)|desires?|hopes?|hoping|favors?|undecided|interested) (\w+\s){,7}(dialysis|resuscitat(ed|ion)|ventilator|trach(eotomy)?)\b',
    r'\b(wants?(ed)?(ing)?|wish(es)?(ed)?|(would|did)( not)? (want|like)|desires?|hopes?|hoping|favors?|undecided|interested) (\w+\s){,7}(code|therapy|therapies|treatments?|chemo(therapy)?|surgery)\b',
    r'\b(wants?(ed)?(ing)?|wish(es)?(ed)?|(would|did)( not)? (want|like)|desires?|hopes?|hoping|favors?|undecided|interested) (\w+\s){,7}(infusions?|interventions?)\b',
    r'\b(wants?(ed)?(ing)?|wish(es)?(ed)?|(would|did)( not)? (want|like)|desires?|hopes?|hoping|favors?|undecided|interested) (\w+\s){,10}(home|family|time|longevity|recovery)\b',
    r'\b(wants?(ed)?(ing)?|wish(es)?(ed)?|(would|did)( not)? (want|like)|desires?|hopes?|hoping|favors?|undecided|interested) (\w+\s){,7}(hospice|comfort(able)?|palliative|pal care|care|icu)\b',
    r'\b(wants?(ed)?(ing)?|wish(es)?(ed)?|(would|did)( not)? (want|like)|desires?|hopes?|hoping|favors?|undecided|interested) (\w+\s){,7}(home|life support|live|alive|die)',
    r'\b(wants?(ed)?(ing)?|wish(es)?(ed)?|(would|did)( not)? (want|like)|desires?|hopes?|hoping|favors?|undecided|interested) (\w+\s){,7}(discomfort|suffer|pain|painful|painless)\b',
    r'\b(wants?(ed)?(ing)?|wish(es)?(ed)?|(would|did)( not)? (want|like)|desires?|hopes?|hoping|favors?|undecided|interested) (\w+\s){,7}(prolong|suffering|extend|sustain|survive|donations?)\b',

    r'\b(patient|pt|pt.|family|he|she|they|we|brothers?|sisters?|sons?|daughters?|children) (\w+\s){,3}(said|says?|states?|stated|expressed|expresses|opt(ed)?(ing)?|requests?(ed)?(ing)?)\b',
    r'\b(patient|pt|pt.|family|he|she|they|we|brothers?|sisters?|sons?|daughters?|children) (\w+\s){,3}(elect(s|ed)?|believes?|indicates?|indicated|decide(s|d)?|reiterated|prefer(s|red)?)\b',
    r'\b(patient|pt|pt.|family|he|she|they|we|brothers?|sisters?|sons?|daughters?|children) (\w+\s){,3}(understand|understood|acknowledges?d?|maintains?)\b',

    r'\b(session) (\w+\s){0,3}(from|with) ((\w|\'|\.)+\s){,3}(patient(\'s)?|pt|pt.|family|him|her|them|brothers?|sisters?|siblings?|sons?|daughters?|children)\b',
    r'\b(counseled|advised) ((\w|\'|\.)+\s){,5}(patient(\'s)?|pt|pt.|family|him|her|them|brothers?|sisters?|siblings?|sons?|daughters?|children)\b',

    r'\bresponse: ((\w|\W)+\s){,5}(time|family|care|goals?|life|live|prolong(ed)?|prolonging|sustain(ed?)|comfort(able)?|dignity|aware|independent(ly)?|independence|burden)\b',
    r'\bresponse: ((\w|\W)+\s){,5}(intubated?|intubation|extubated?|extubation|treatments?|chemo(therapy)?|ventilator|resuscitated?|resuscitation|CPR|DNR|DNI)\b',

    r'\b(palliative care|pal care|hospice) (\w+\s){,5}(will follow|will continue|will assess)\b',

    r'\b(prefers|prioritizes) (\w+\s){,5}(comfort|longevity|survival|pain|family|time|chances|quality of life)\b',
    r'\b(pt\'s|pt\.\'s|patient\'s|his|her|their|family\'s) (priority|priorities|goals?|plans?|wish(es)?)',

    r'\b(agrees?|agreed|agreeable|agreement|confims?|confirmed|confirmation|inclined|endorsed?s?) (\w+\s){,7}(goals?|comfort|care|intubated?|intubation|extubated?|extubation|dialysis)\b',
    r'\b(agrees?|agreed|agreeable|agreement|confims?|confirmed|confirmation|inclined|endorsed?s?) (\w+\s){,7}(resuscitated?|resuscitation|therapy|therapies|treatments?|interventions?)\b',
    r'\b(agrees?|agreed|agreeable|agreement|confims?|confirmed|confirmation|inclined|endorsed?s?) (\w+\s){,7}(dependent|dependency|home|family|sustain(ed)?|ventilator|ventilation)\b',
    r'\b(agrees?|agreed|agreeable|agreement|confims?|confirmed|confirmation|inclined|endorsed?s?) (\w+\s){,7}(CPR|DNR|DNI|hospice|palliative|pal care|code)\b',

    r'\b(patient(\'s)?|pt(\'s)?|family(\'s)?|he|his|she|her|they|their) (goals?|plans?|aims?)\b',
    r'\b(patient(\'s)?|pt(\'s)?|family(\'s)?|he|his|she|her|they|their) (requests|requested)\b',

    r'\b(understands?|understanding|understood|realizes?|knows?|aware) (\w+\s){,7}(risks?|harms?|concerns?|problems?|issues?|conditions?|prognosis|trajectory|progression|progressive|severity)\b',
    r'\b(understands?|understanding|understood|realizes?|knows?|aware) (\w+\s){,7}(better|worse|die|death|alive|live)\b',
    r'\b(understands?|understanding|understood|realizes?|knows?|aware) (\w+\s){,7}(risks?|harms?|problems?|issues?|time|possibility|possibilities)\b',

    r'\b(considers?|considering|thinks?|thinking|plans?|planning) (\w+\s){,7}(hospice|palliative|pal care)\b',

    r'\bwhich (he|she|patient|pt|pt\.|family) (understands?|understood|realizes?|aware|agrees?|agreed|confirms?|confirmed)\b',

    # Quotes from patient or family
    r'\"\s?((\w|\')+\s){,7}(want|will|hope|comfortable|family|home|suffer|die|live|life|interventions?|therapy|therapies|alive|burden|aware)\b',

    r'\b(according to) (\w+\s){,3}(patient(\'s)?|pt|pt.|family|him|her|them|brothers?|sisters?|siblings?|sons?|daughters?|children)',

    # General discussion terms starting with "we" -- no other terms needed
    #r'\bwe (discussed|talked|spoke|reviewed|went over)\b'

]


class GOCRegExFinder(ClassifierInt):
    def __init__(self, name: str, config: configparser, verbose=False):
        """
        :param: name, the name of the classifier for further reference
        :param verbose: if true it will out put the debug comments and generate the analysis files in tmp
        """

        self.CORPUS = "GOC"
        self.REGEXES = REGEXES_GOC

        super(GOCRegExFinder, self).__init__(name)
        # there is no history computed by this classifier
        self.histories = None
        self.verbose = verbose
        self.name = name
        self.config = config

    def getName(self):
        """
        :return: the name of the classifier
        """
        return self.name

    def train(self, trainExamples: DataFrame, evalExamples: DataFrame = None, checkPointPath:str = None):
        '''
        :param: just run the REs on the validation set of the pool
        :return: history a raw instance of the History for compatibility
        '''
        log.debug(
            "=> I am a manually designed REs classifier, nothing to learn. But I can run your REs on the training and output your score on this set.")
        # we may compute the P/R/F on the training to compute the efficiency of each pattern
        self.classify(trainExamples)
        return None

    def preprocess(self, tweet: series):
        """
        :param tweet: just preprocess the string of the tweet before applying the REs, lower the case
        If we process tweets, we add one more step we anonymize the drug names with the joker __drug_name__
        """
        #         if pd.isna(tweet['text']):
        #             return ""
        #print(f"=> {tweet['text']}")
        try:
            twt = re.sub(r'\W(rrb|lrb)\W', " ", tweet['text'])
        except:
            log.error(f"Error with tweet, try with the tweet casted as string: {tweet['text']}")
            twt = re.sub(r'\W(rrb|lrb)\W', " ", str(tweet['text']))
        twt = re.sub(r'\s{2,}', " ", twt)
        twt = twt.lower()
        # If we are processing tweets we continue by replacing the drug names annotated with a joker __drug_name__
        if self.CORPUS == "Twitter":
            assert 'Drug Name Extracted' in tweet.index, "I was expecting the name of drugs extracted manually in Drug Name Extracted, they are not. Implement an alternative with find them from a dictionary..."
            if not pd.isna(tweet['Drug Name Extracted']):
                drugs = re.split(';|,', tweet['Drug Name Extracted'])
                for drug in drugs:
                    drug = drug.strip().lower()
                    if drug not in twt:
                        log.debug(
                            f"=> can't find the drug name [{drug.strip()}] in the current tweet [{twt}] - original tweet: [{tweet['text']}], I'll just ignore it but REs will not match this tweet. Check the data probably not correctly annotated.")
                    else:
                        twt_replaced = twt.replace(drug, "__drug_name__")
                        if len(drug) <= 3:
                            log.debug(f"Before for drug {drug}: {twt}")
                            log.debug(
                                f"Check the replacement of the drug in the tweet because it is a small name [{drug}]: {twt}")
                            log.debug(f"After: {twt_replaced}")
                        twt = twt_replaced
            else:
                log.debug(
                    f"=> can't find the drug name [NaN] in the current tweet [{twt}] - original tweet: [{tweet['text']}], I'll just ignore it but REs will not match this tweet. Check the data probably not correctly annotated.")

        return twt

    def classify(self, examples: DataFrame) -> DataFrame:
        '''
        :param: examples to classify
        :return: a copy of the examples updated with 1 columns prediction and n columns certainty_label_n:
          - prediction: the best prediction made by the classifier, ie, the label with the highest certainty
          - certainty_label_n: a column where the classifier assign the probability for the label n for each example
        '''
        regexes_compiled = [re.compile(regex, re.IGNORECASE) for regex in self.REGEXES]
        FPs = {}
        FPs_post = {}
        FNs = []
        TPs = {}
        TPs_post = {}
        evalExamples = examples.copy()

        def applyREs(ex: Series, regexes, FPs, FNs, TPs):
            # preprocess the tweet before applying the REs
            text = self.preprocess(ex)
            matched = 0
            for regex in regexes:
                if re.search(regex, text):
                    if ex['label'] == 0:
                        if regex not in FPs_post.keys():
                            FPs_post[regex] = []
                        FPs_post[regex].append(text)
                        if regex not in FPs.keys():
                            FPs[regex] = 0
                        FPs[regex] = FPs[regex] + 1
                    #                         log.debug(f"FP => Label: {ex['label']} Example: {text} matched on \n\tPattern: {regex}")
                    if ex['label'] == 1:
                        if regex not in TPs_post.keys():
                            TPs_post[regex] = []
                        TPs_post[regex].append(text)
                        if regex not in TPs.keys():
                            TPs[regex] = 0
                        TPs[regex] = TPs[regex] + 1
                    #                         log.debug(f"TP => Label: {ex['label']} Example: {text} matched on Pattern: {regex}")
                    matched = 1
            if ex['label'] == 1 and matched == 0:
                FNs.append(text)
            #                     log.debug(f"FN => Label: {ex['label']} Example: {text} not matched by any pattern")
            return matched

        evalExamples['prediction'] = evalExamples.apply(lambda post: applyREs(post, regexes_compiled, FPs, FNs, TPs),
                                                        axis=1)
        # when I output 1 I'm always sure
        evalExamples['certainty_label_1'] = evalExamples['prediction']

        # when I output 0 I'm always sure as well
        def inverse(label):
            if label == 1:
                return 0
            elif label == 0:
                return 1
            else:
                raise Exception(f"I got an unexpected label {label} in the RegFinder when I was expecting 0 or 1.")

        evalExamples['certainty_label_0'] = evalExamples['prediction'].apply(lambda label: inverse(label))

        log.error(f"TODO: finish to compute the certainty of the REs on the training set")
        if self.verbose:
            self.displayDetails(FPs, TPs, FNs, FPs_post, TPs_post)

        evalExamples.to_csv(self.config['Data']['tmpFolder']+"RegExOutput.tsv", sep='\t')
        return evalExamples

    def displayDetails(self, FPs: dict, TPs: dict, FNs: list, FPs_post: dict, TPs_post: dict):
        # Everything is computed so we can display the stats...
        # the FNs, not capture by any pattern
        log.debug(f"Display {len(FNs)} FNs:")
        for fn in FNs:
            log.debug(f"FN - Tweet missed: {fn}")
        self.writeFNs(FNs)
        # display the classification performance by pattern
        count = 0
        log.debug("Display FPs/TPs by patterns:")
        sorted_d = sorted(FPs.items(), key=lambda x: x[1], reverse=True)
        for tup in sorted_d:
            count = count + 1
            log.debug(f"Pattern in File n'{count}")
            log.debug(f"FP - Score: {tup[1]} ===> Pattern: {tup[0]}")
            if tup[0] in TPs.keys():
                log.debug(f"TP - Score: {TPs[tup[0]]} ===> Pattern: {tup[0]}")
                # remove the pattern from the list to know which one are in TPs without FPs
                TPs.pop(tup[0])
            else:
                # the pattern was not found in TPs so no actual posts were matched by the pattern, can probably delete it
                log.debug(f"TP - Score: 0 ===> Pattern: {tup[0]}")
            self.writePatternsPosts(count, FPs_post, TPs_post, tup[0])
        sorted_d = sorted(TPs.items(), key=lambda x: x[1], reverse=True)
        log.debug("--- Display TPs without FPs ---")
        for tup in sorted_d:
            count = count + 1
            log.debug(f"TP - Score: {tup[1]} ===> Pattern: {tup[0]}")
            assert tup[
                       0] not in FPs.keys(), f"I was expecting that the pattern {tup[0]} was not in the set of FPs, check the code."
            log.debug(f"FP - Score: 0 ===> Pattern {tup[0]}")
            self.writePatternsPosts(count, FPs_post, TPs_post, tup[0])

    def writeFNs(self, FNs: list):
        # Sid: Changed encoding to utf-8 to avoid error from some special characters
        with open(self.config['Data']['tmpFolder']+"RegExFNs.txt", 'w', encoding='utf-8') as file:
            file.write(f"#FNs: {len(FNs)}\n")
            for fn in FNs:
                # try:
                file.write(f"{fn.lower()}\n\n")
                # except:
                #     print(fn.lower)
                #     file.write("character decode issue\n\n")

    def writePatternsPosts(self, counter: int, FP_post: dict, TP_post: dict, pattern: str):
        """
        Write a file in tmp named by the counter and display first the TPs captured by the REs then the FPs
        :param counter: a simple counter to put a name on the file, not related with the indice of th REs in the list of REs
        :param FP_post: a dict with pattern -> [tweet1, tweet2, ...] where the tweets are matched by the pattern, here False Positive
        :param TP_post: a dict with pattern -> [tweet1, tweet2, ...] where the tweets are matched by the pattern, here True Positive
        :param pattern: the pattern we are looking for in the dictionary
        """
        # Sid: changed encoding to utf-8 again
        with open(self.config['Data']['tmpFolder']+f"pattern_{counter}.txt", 'w', encoding='utf-8') as file:
            file.write(f"Pattern: {pattern}\n")
            if pattern in TP_post and pattern in FP_post:
                file.write(f"#TPs: {len(TP_post[pattern])}; #FPs: {len(FP_post[pattern])}\n")
                file.write(f"------------ TPs ------------\n")
                for twt in TP_post[pattern]:
                    file.write(f"{twt.lower()}\n\n")
                file.write(f"------------ FPs ------------\n")
                for twt in FP_post[pattern]:
                    file.write(f"{twt.lower()}\n\n")
            elif pattern in TP_post and pattern not in FP_post:
                file.write(f"#TPs: {len(TP_post[pattern])}; #FPs: 0\n")
                file.write(f"------------ TPs ------------\n")
                for twt in TP_post[pattern]:
                    file.write(f"{twt.lower()}\n\n")
                file.write(f"------------ No FPs ------------\n")
            elif pattern not in TP_post and pattern in FP_post:
                file.write(f"#TPs: 0; #FPs: {len(FP_post[pattern])}\n")
                file.write(f"------------ No TPs ------------\n")
                file.write(f"------------ FPs ------------\n")
                for twt in FP_post[pattern]:
                    file.write(f"{twt.lower()}\n\n")
            else:
                raise Exception(
                    f"I have a pattern that is neither in the list of FP or FN, should not happen: {pattern}")

    
    def save(self, iteration: int):
        log.info("I am a RegEx classifier, nothing to save.")

    
    def load(self, pathModel: str):
        log.info("I am a RegEx classifier, nothing to load.")
        
    def getInitialCheckpointPath(self) -> str:
        '''
        :return: the path to the initial model checkpoint (so the model initially instantiated, None if the classifier does not support checkpoints)
        '''    
        return None
    
    def getCurrentCheckpointPath(self) -> str:
        '''
        :return: the path to the current model checkpoint (not necessarily the best model, just the current model from the last AL iteration, None if the classifier does not support checkpoints)
        '''
        return None
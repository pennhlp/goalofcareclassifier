'''
Created on Oct 8, 2019

@author: davy
'''

import logging as lg
log = lg.getLogger('RandomClassifier')
from pandas.core.frame import DataFrame
import numpy as np
from sklearn.metrics import precision_score, recall_score, f1_score, confusion_matrix
from deprecated import deprecated

from ActiveLearners.Classifiers.ClassifierInt import ClassifierInt
from Data.Pool import Pool


class RandomClassifier(ClassifierInt):
    '''
    A dummy classifier which returns a random decision
    The classifier is binary: 0 or 1; also indicate a random probability for its decisions
    '''

    def __init__(self, name:str):
        '''
        :param::param: name, the name of the classifier for further reference
        '''
        super(RandomClassifier, self).__init__(name)
        log.info(f"A random classifier: {self.getName()} is used, be sure that this is what you want to use...")
        

    def train(self, trainExamples: DataFrame, evalExamples: DataFrame, pathCheckPoint:str):
        '''
        :param: trainExamples train the classifier on the training examples given
        :param: evalExamples the validation set used during the training
        :param pathCheckPoint: the path to the checkpoint to update, here it will just be ignored...
        :return: a model updated
        '''
        log.info(f"I received {len(trainExamples)} training examples for training, but I am a random classifier, not much to train for, so my training is done ^^")
    
    
    def classify(self, examples: DataFrame) -> DataFrame:
        '''
        :param: examples to classify
        :return: a copy of the examples updated with 3 columns: prediction and 2 columns certainty_label_0/1:
          - prediction: the best prediction made by the classifier, ie, the label with the highest certainty 
          - certainty_label_0/: a column where the classifier assign the probability for the label 0/1 for each example
        '''
        log.info(f"Random binary classifier, randomly assigns labels and certainty to the {len(examples)} examples given")
        examples = examples.copy()
        examples['prediction'] = np.random.randint(2, size=len(examples))
        rand0 = np.random.rand(1,len(examples))[0]
        examples['certainty_label_0'] = rand0
        examples['certainty_label_1'] = 1-rand0
        examples['prediction'] = 0
        examples.loc[examples["certainty_label_1"]>=.5, 'prediction']=1
        return examples
        
    def getInitialCheckpointPath(self) -> str:
        return None
    
    def getCurrentCheckpointPath(self) -> str:
        return None
        
'''
Created on Jan 30, 2020

@author: Ari Klein/Davy Weissenbacher


A set of regular expressions to find mention of medication changes to be tested... 
'''

import logging as lg
log = lg.getLogger('RegExFinder')
import pandas as pd
from pandas.core import series
from pandas.core.series import Series
from pandas.core.frame import DataFrame

from Data.Pool import Pool
from ActiveLearners.Classifiers.ClassifierInt import ClassifierInt
from Performance.Performance import Scores
from keras.callbacks import History
# coding=utf-8
import sys

# reload(sys)
# sys.setdefaultencoding('utf8')

import re
import csv

#A set of REs developed on the WebMD training set and validated on the validation set
REGEXES_WebMD = [
r'\b(not|never|no) ((\w|\W)+\s){,2}(take|taking|took|taken|use|using|used|touch|touching|touched|continue|continuing|continued|recommend|reccomend|reccommend|try|trying)\b',
   
r'\bi (have\s?n\W?t|did\s?n\W?t|do\s?n\W?t|wo\s?n\W?t|ca\s?n\W?t|could\s?n\W?t|would\s?n\W?t) (\w+\s)?(take|taking|taken|use|using|used|touch|touching|touched|continue|continuing|continued|recommend|reccomend|reccommend|try|trying)\b',
   
r'\b(i|i\W{,2}m|i am||i\W{,2}ve|i have|i have been|i\W{,2}ll|i will|i\W{,2}d|i would|i had|i do|i did) (refuse|refusing|refused) to (take|use|touch|continue|keep|try)\b',
   
r'\b(quit|quitting|stop|stopping|stopped|discontinue|discontinuing|discontinued|cut|done with)\b',
   
r'\b(i|i\W{,2}m|i am|i\W{,2}ve|i have|i have been|i\W{,2}ll|i will|i\W{,2}d|i would|i had|i do|i did) (forget|forgot|forgetting|forgotten|forgetting) to (take|use)\b',
   
r'(take|taking|took|taken|wean|weaning|weaned|get|getting|got|gotten|pull|pulling|pulled|come|coming|came|went|been|be|go|going|being|i\W{,2}m|i am|i was|tapered|tapering|backed|turned) ((myself|me|her|him)\s)?off\b',
   
r'\b(i|i\W{,2}m|i am|i\W{,2}ve|i have|i\W{,2}ll|i will|i\W{,2}d|i would|i had|i had to|i did|i was) (\w+\s){,5}(switch|switching|switched|change|changing|changed|substitute|substituting|substituted)\b',
   
r'\b(switch|switching|switched|change|changing|changed|substitute|substituting|substituted) (me|my|medications?|meds?)\b',
   
r'\b(back|return|returned) (to|on)\b',
   
r'\b(made|gave) me\b',
   
r'\b(was|had been|i\W{,2}d been) (\w+\s)?(taking|using|on)\b',
   
r'\b((increase|increased|increasing|up|upped|uped|uping|upping|raise|raised|raising|decrease|decreased|decreasing|lower|lowered|lowering|adjust|adjusted|adjusting|change|changed|changing|cut|cutting|reduce|reduced|reducing) (\w+\s)?(dosage|doseage|dose)|(dosage|doseage|dose) (\w+\s){,2}(increased|upped|uped|raised|decreased|lowered|adjusted|changed|cut|reduced)|(increase|increased|increasing|up|upped|uped|uping|upping|raise|raised|raising|decrease|decreased|decreasing|lower|lowered|lowering|adjust|adjusted|adjusting|change|changed|changing|cut|cutting|reduce|reduced|reducing) ((\w|\d)+\s){,3}to (\d|\w|\W)+\s?(mg|mcg|pills?|tablets?|tabs?))\b',
   
r'\bfor ((\w+\s){,2}|\d+\s)(days?|nights?|weeks?|months?|years?)\b',
   
r'\bside effects\b',
#
r'\b(took|used|was on|finished|prescribed|given) this (medication|medicine|med|product)\b',
#
r'\b(took|taken|used|was on|had) (\w+\s)?(\d|\w)+ (times?|treatments?|pills?|dose)\b',
   
r'\bcolonoscopy\b',
   
r'\bbeen ((\d|\w)+\s)?(days?|weeks?|months?|years?) since (i|i\W{,2}ve|i have) (took|taken|used|was on)\b',
   
r'\b(er|emergency room|hospital|hospitalized)\b',
   
r'\bsamples?\b',
   
r'\b(allergy|allergies|allergic)\b',
   
r'\breactions?\b',
   
r'\b(1\/2|1\/4|3\/4|half|quarter|three quarters) (\w+\s){,2}(pills?|tablets?|tabs?|dosage)\b',
   
r'\b(killed|die|died|death)\b',
   
r'\b(costs?|expensive)',
   
r'\b(took|taken|used|was on|prescribed|put on) (\w+\s){1,2}(for|because|after)\b',
   
r'\btried (\w+\s){,2}(take|taking|use|using)\b',
   
r'\bworst\b',
   
r'\bcaused\b',
   
r'\b(ineffective|had no effect)\b',
   
r'\bnow (\w+\s){,2}(taking|using|on)\b',
   
r'\b(new|newer|old|older)\b',
   
r'\b(not|did\s?n\W?t) help\b',
   
r'\bafter trying\b',
   
r'\bbeware\b',
#
r'\bwithdrawals?\b',
#
r'\bawful\b',
   
r'\bdid nothing\b',
   
r'\b(other ways|another way)\b',
   
r'\bmore than (\w+\s)?prescribed\b'
]

REGEXES_Twitter = [

#we added patterns that covered multiple examples (with no more FP than TP) or patterns that cover 1 but could eventually cover other based on the language used
###NEW patterns

  r'\bhad to give (\w+\s){,5}__drug_name__\b',
  
  r'\b(i|he|she) self-medicates?\b',
  
  r'\b(smaller|larger) (__drug_name__|dose)\b',
  
  r'\b(lowered|increased) (\w+\s){,4}(\d\d?mg)\b',
  
  r'\b(reduced?|decreased?|increased?|increasing|decreasing|upping|doubled?-up) (\w+\s){,3}(dosage|doses?|medications?|__drug_name__)\b',
  
  r"\b(?<!will just\s)(?<!will\s)(?<!time to\s)(?<!gets\s)(?<!get\s)(?<!may have to\s)(?<!gonna make me\s)(?<!gonna make him\s)(?<!gonna make her\s)(?<!'ll\s)(?<!may\s)(?<!better\s)(?<!need to\s)doubled? (\w+\s){,3}(dosage|doses?|medications?|__drug_name__)\b",
  
  r'\b(down|up) to (\w+\s){,3}(doses?|dosage|pills?)\b',
  
  r'\bcut (\w+\s){,3}intake of (\w+\s){,3}__drug_name__\b',
  
  r"\b(been|was|am|'s) od on (\w+\s){,3}__drug_name__\b",
  
  r'\btons? (\w+\s){,3}(__drug_name__|meds?|pills?)\b',
  
  r'\bpopped ((\d){2,}\s)__drug_name__\b',
 
  r"\binjected\b",
  
  r'\btook an extra\s(\w+\s){,2}__drug_name__\b',
  
  r'\btaking so much (\w+\s){,2}__drug_name__\b',
 
  r"\bmaximum dosage (\w+\s){,3}(__drug_name__|meds?|medications?|pills?)\b",
 
  r"\b__drug_name__ (\w+\s){,4}cocktails\b",
  
  r'\bshould have (only )?taken\b',
  
  r'\b(new|newer) (dose|dosage) (\w+\s){,2}__drug_name__\b',
 
  r'\bdosage has changed\b',
 
  r"\btapering off\b",
 
  r'\bput (me\s|her\s|him\s)?on (\w+\s){,3}__drug_name__ (instead|again)\b',
  
  r'\btake a (high|lower) dosage\b',
  
  r'\b__drug_name__ (\W|\w+\s){,3}half(?!\san hour)\b',
 # r'\bhalf (an\s|of a\s)__drug_name__\b',
  
  r'\bdoubled (\w+\s){,3}(__drug_name__|dose)\b',
  
  r'\bchanged (\w+\s){,3}(__drug_name__|meds)\b',
  r'\bchanged (\w+\s){,3}to (\w+\s){,2}(__drug_name__|meds?|pills?)\b',
 
  r'\bprescribed (\w+\s){,3}new (__drug_name__|meds?|pills?|medications?)\b',
  
  r'\bnow on __drug_name__\b',
  
  r'\b(start back|restart) (\w+\s){,3}__drug_name__\b',
 
  r"\bgo back to ((\w+|\W){,4}\s)(__drug_name__|meds?|medicactions?|pills?)\b",
 
 r"\bonly (\w+\s){,3}i could take\b",
  
  r'\b(try|trying)\s(\w+\s){,2}(__drug_name__|meds)\s(\w+\s){,2}(now|again)\b',
  
  r'\b(switch(ing)?|swap(ped)?) (\w+\s){,3}(__drug_name__|meds?|medications?|pills?|dosage|doses?)\b',
  
  r'\btrying (\w+\s){,3}without (\w+\s){,3}(__drug_name__|meds?|pills?)\b',
  
  r'\bbeen through (\w+\s){,3}__drug_name__\b',
  
  r"\bended up (\w+\s){,3}__drug_name__\b",
  
  r'\bnow (i\s|he\s|she\s)(\w+\s){,3}take __drug_name__\b',
  
  r'\bswitched to\b',
 
  r"\bswitched (pills?|meds?|medications?)\b",
  
  r'\bswitched (\w+\s){,3}__drug_name__\b',
  
  r'\b(gp|doctor|obg|chiro) said (stop|go)\b',
  
  r"\b(?<!ever\s)(?<!have you\s)(?<!haven't\s)(?<!has anyone\s)tried (\w+?\s){,3}?__drug_name__\b",
  
  r"\bfrom (\w+\s){,2}__drug_name__ to (\w+\s){,2}__drug_name__\b",
  
  r'\b__drug_name__ (\w+\s){,3}works better than\b',
  
  r'\bback (to|on) (\w+\s){,3}__drug_name__\b',
  
  r'\bmiss (\w+\s){,3}__drug_name__\b',
  
  r"\bmisses ((\w+|#)\s){,3}#?(meds?|medications?|pills?|__drug_name__)\b",
  
  r'was on (\w+\s){,4}(__drug_name__|meds?|pills?|medications?)',
  
  r'\b(?<!you ever\s)(?<!you\s)(?<!never\s)tried #?__drug_name__\b',
  
  r'\b(ran out of|ran outta)\s(\w+\s){,3}?(__drug_name__|meds?)\b',
  
  r'\bdenied (\w+\s){,3}__drug_name__\b',
  
  r'\blast (?!week)(?!night)(?!tip)(?!advice)(?!year)(?!day)(?!month)(\w+\s){,3}__drug_name__\b',
  
  r'\b(consumed|took|drunk) (my|her|his) last __drug_name__\b',
  
  r'\btook (my|his|her) last dose of __drug_name__\b',
  
  r"\bno more (\w+\s){,2}(__drug_name__|meds?|pills?)\b",
  
  r'\b(?<!fuck\s)(?<!worth\s)out of (?<!mind\s)(\w+\s){,3}(__drug_name__|meds?|pills?)\b',
  
  r'\bbreak from (\w+\s){,2}(__drug_name__|meds|pills)\b',
  
  r"(not\s|n't\s)renew(ed)? (\w+\s){,3}refill\b",
  
  r"\b(took\s|popped\s|drunk\s)(\w+\s){,2}__drug_name__ (\w+\s){,2}accident\b",
  
  r"\baccidentally took (\w+\s){,3}(__drug_name__|meds)\b",
  
  r'\b(takes?\s|taking\s|took\s|popped\s|gave\s)(\w+\s){,3}instead of (\w+\s){,2}(__drug_name__|meds|pills)\b',
  
  r"\bforgot to give (\w+\s){,3}__drug_name__\b",
  
  r'\bcouldn’t find (\w+\s){,3}(__drug_name__|meds?|medications?|pills)\b',
  
  r'\b(forgetting|had forgotten|forgot|4got|supposed) (to|2) (take|pack) (\w+\s){,3}(__drug_name__|med?|pills?)\b',
  
  r"\b(?<!$\d\s)(?<!should go\s)(?<!can't wait to be\s)(?<!can't get\s)(?<!need to get\s)(?<!faded\s)off\s(\w+\s){,2}#?(__drug_name__|meds?|pills?|medications?)\b",
  
  r'\b(__drug_name__|meds?|pills?) (\w+\s){,3}no longer taking\b',
  
  r'\bstop(ped)? taking\b',
 
  r"\bnot give (me|his|her) more __drug_name__\b",
  
  r'\bclean from __drug_name__\b',
  
  r'\bsober (\w+\s){,3}__drug_name__\b',
  
  #r'\bget off? (\w+\s){,3}(meds?|medications?|__drug_name__)\b',
  
  r'\bnever again (\w+\s){,3}__drug_name__\b',
 
  r'\bnever take it again\b',
  
  r'\bquit (\w+\s){,2}__drug_name__\b',
  
  r'\bquit taking\b',
 
  r"\bgone (\w+\s){,3}without (__drug_name__|meds?|pills?|medications?)\b",
  
  r"\b(haven't|hasn't|have not|has not) taken (\w+\s){,3}__drug_name__\b",
  
  r'\b__drug_name__ (\w+\s){,3}withdrawal\b',
  
  r'\bwithdrawal (\w+\s){,3}__drug_name__\b',
  
  r'\bwithdrawn (\w+\s){,3}\b',
  
  r'\bwas taking (\w+\s){,2}__drug_name__\b',
  
  #r'\bwas given (\w+\s){,3}__drug_name__\b',
  
  r'\bnot taking\b',
  
  r'\b(threw|tossed) (\w+\s){,3}__drug_name__\b',
  
  r'\b(used to take|going back on|ended up on) (\w+\s){,2}__drug_name__\b',
  
  r'\bused to take (\d+)(mg?|pills)\b',
  
  r'\b(never had it refilled)|(never refilled)|(should have refilled)\b',
  
  r"\b(haven't|have not|hasn't|has not) been taking (\w+\s){,2}(__drug_name__|meds?|pills?)\b",
  
  r'\bstopped (\w+\s){,2}(__drug_name__|meds?|pills?)\b',
  
  r"\bgiving up (\w+\s){,3}(__drug_name__|pills?|meds?|medications?)\b",
 
  r"\bnever take (\w+\s){,3}(__drug_name__|meds?|medications?|pills?) again\b",
 
  r'\bnot getting all of (his|my|her) (meds?|__drug_name__|pills?|medications?)\b',
  
  r'\bto take (\w+\s){,3}(__drug_name__|meds?|pills?) anymore\b',
  
  r'\b(__drug_name__|meds|pills) (\w+\s){,2}made me\b',
  
  r'\boverdosed on (\w+\s){,3}?__drug_name__\b',
 
 ###NEW patterns
     
 #r'\b(not|never|no) ((\w|\W)+\s){,2}(take|taking|took|taken|use|using|used|touch|touching|touched|continue|continuing|continued|recommend|reccomend|reccommend|try|trying)\b',
 #r'\b(not|never|no) ((\w|\W)\s){,3}?(take|taking|took|taken|use|using|used|touch|touching|touched|continue|continuing|continued)\b',
 # noisy but can be accepted
   
 #r'\bi (have\s?n\W?t|did\s?n\W?t|do\s?n\W?t|wo\s?n\W?t|ca\s?n\W?t|could\s?n\W?t|would\s?n\W?t) (\w+\s)?(take|taking|taken|use|using|used|touch|touching|touched|continue|continuing|continued|recommend|reccomend|reccommend|try|trying)\b',
 '\bi (have\s?n\W?t|did\s?n\W?t|do\s?n\W?t|wo\s?n\W?t|ca\s?n\W?t|could\s?n\W?t|would\s?n\W?t) (\w+\s)?(take|taking|taken|use|using|used|touch|touching|touched|continue|continuing|continued)\b',
   
 r'\b(i|i\W{,2}m|i am||i\W{,2}ve|i have|i have been|i\W{,2}ll|i will|i\W{,2}d|i would|i had|i do|i did) (refuse|refusing|refused) to (take|use|touch|continue|keep|try)\b',
   
 #r'\b(quit|quitting|stop|stopping|stopped|discontinue|discontinuing|discontinued|cut|done with)\b',
 r'\b(quit taking|quit drinking|quitting|had to stop|stop taking|stopping|stopped taking|no longer taking|not taking|discontinue|discontinuing|discontinued|cut back|cut (\w+\s){,3}half|cut (\w+\s){,3}intake|done with)\b',
   
 r'\b(i|i\W{,2}m|i am|i\W{,2}ve|i have|i have been|i\W{,2}ll|i will|i\W{,2}d|i would|i had|i do|i did) (forget|forgot|forgetting|forgotten|forgetting) to (take|use)\b',
   
 r'(take|taking|took|taken|wean|weaning|weaned|get|getting|got|gotten|pull|pulling|pulled|come|coming|came|went|been|be|go|going|being|i\W{,2}m|i am|i was|tapered|tapering|backed|turned) ((myself|me|her|him)\s)?off\b',
   
 r'\b(i|i\W{,2}m|i am|i\W{,2}ve|i have|i\W{,2}ll|i will|i\W{,2}d|i would|i had|i had to|i did|i was) (\w+\s){,5}(switch|switching|switched|change|changing|changed|substitute|substituting|substituted)\b',
   
 r'\b(switch|switching|switched|change|changing|changed|substitute|substituting|substituted) (me|my|medications?|meds?)\b',
   
 #r'\b(back|return|returned) (to|on)\b', DROP
 r'\b(back|return|returned) (to|on)(\s\w+?){,3}(\s__drug_name__|\sdose|\smg)\b',
   
 #r'\b(made|gave) me\b', DROP
 r'\b(__drug_name__\s(\w+?\s){,3}?made\sme)|(gave\sme\s(\w+?\s){,3}?__drug_name__)\b',
   
 #r'\b(was|had been|i\W{,2}d been) (\w+\s)?(taking|using|on)\b', DROP
 r'\b(was|had been|i\W{,2}d been) (\w+\s)?(taking|using|on)\s(\w+?\s){,4}?__drug_name__\b',
   
 #r'\b((increase|increased|increasing|up|upped|uped|uping|upping|raise|raised|raising|decrease|decreased|decreasing|lower|lowered|lowering|adjust|adjusted|adjusting|change|changed|changing|cut|cutting|reduce|reduced|reducing) (\w+\s)?(dosage|doseage|dose)|(dosage|doseage|dose) (\w+\s){,2}(increased|upped|uped|raised|decreased|lowered|adjusted|changed|cut|reduced)|(increase|increased|increasing|up|upped|uped|uping|upping|raise|raised|raising|decrease|decreased|decreasing|lower|lowered|lowering|adjust|adjusted|adjusting|change|changed|changing|cut|cutting|reduce|reduced|reducing) ((\w|\d)+\s){,3}to (\d|\w|\W)+\s?(mg|mcg|pills?|tablets?|tabs?))\b',
 # noisy but can be accepted
   
 #r'\bfor ((\w+\s){,2}|\d+\s)(days?|nights?|weeks?|months?|years?)\b',
 #r'\b(meds?|pills|__drug_name__|mg)\s(\w+?\s){,4}?for ((\w+\s){,2}|\d+\s)(days?|nights?|weeks?|months?|years?)\b', DROP
 #no replacement too noisy in tweets
   
 #r'\bside effects\b',
 #no replacement too noisy in tweets
   
 r'\b(took|used|was on|finished|prescribed|given) this (medication|medicine|med|product)\b',
   
 #r'\b(took|taken|used|was on|had) (\w+\s)?(\d|\w)+ (times?|treatments?|pills?|dose)\b',
 # noisy but can be accepted
   
 r'\bcolonoscopy\b',
   
 r'\bbeen ((\d|\w)+\s)?(days?|weeks?|months?|years?) since (i|i\W{,2}ve|i have) (took|taken|used|was on)\b',
   
 #r'\b(er|emergency room|hospital|hospitalized)\b',
 #no replacement too noisy in tweets
   
 #r'\bsamples?\b',
 #no replacement too noisy in tweets
   
 #r'\b(allergy|allergies|allergic)\b',
 #no replacement too noisy in tweets
   
 #r'\breactions?\b',
 #no replacement too noisy in tweets
   
 r'\b(1\/2|1\/4|3\/4|half|quarter|three quarters) (\w+\s){,2}(pills?|tablets?|tabs?|dosage)\b',
   
 #r'\b(killed|die|died|death)\b',
 #no replacement too noisy in tweets
   
 #r'\b(costs?|expensive)',
 #no replacement too noisy in tweets
   
 #r'\b(took|taken|used|was on|prescribed|put on) (\w+\s){1,2}(for|because|after)\b',
 r'\b(took|taken|was on|put on) (\w+\s){1,2}(for|because|after)\b',
   
 #r'\btried (\w+\s){,2}(take|taking|use|using)\b',
 #no replacement too noisy in tweets
   
 #r'\bworst\b',
 #no replacement too noisy in tweets
   
 #r'\bcaused\b',
 # noisy but can be accepted
   
 #r'\b(ineffective|had no effect)\b',
 #no replacement too noisy in tweets
   
 r'\bnow (\w+\s){,2}(taking|using|on)\b',
   
 #r'\b(new|newer|old|older)\b',
 '\b(newer|older) +(dose|med|meds|__drug_name__)+\b',
   
 #r'\b(not|did\s?n\W?t) help\b',
 #no replacement too noisy in tweets
   
 r'\bafter trying\b',
   
 r'\bbeware\b',
   
 r'\bwithdrawals?\b',
 # noisy but can be accepted
   
 #r'\bawful\b',
 #no replacement too noisy in tweets
   
 r'\bdid nothing\b',
   
 r'\b(other ways|another way)\b',
   
 r'\bmore than (\w+\s)?prescribed\b'
]

# def preprocess(post):
#     post = re.sub(r'\W(rrb|lrb)\W', " ", post)
#     post = re.sub(r'\s{2,}', " ", post)
#     return post
# 
# def write_nonadherence_posts():
#     regexes_compiled = [re.compile(regex, flags=re.IGNORECASE) for regex in REGEXES]
#     #with open("MSCClassification_12972_1211019_temp_VAL_pos.tsv") as filename_input:
#     #with open("MSCClassification_12972_1211019_temp_VAL_neg.tsv") as filename_input:
#     #with open("MSCClassification_12972_1211019_temp_TRAIN_pos.tsv") as filename_input:
#     #with open("MSCClassification_12972_1211019_temp_TEST_pos.tsv") as filename_input:
#     with open("MSCClassification_12972_1211019_temp_TEST_neg.tsv") as filename_input:
#         with open("non-adherence_matches.tsv", 'wb') as filename_output:
#             filereader = csv.reader(filename_input, delimiter='\t')
#             filewriter = csv.writer(filename_output, delimiter='\t')
#             for row in filereader:
#                 text = unicode(preprocess(row[5]))
#                 if any(re.search(re_comp, text) for re_comp in regexes_compiled):
#                 #if not any(re.search(re_comp, text) for re_comp in regexes_compiled):
#                     filewriter.writerow([row[5]])
#                     print row[5]
# 
# 
# if __name__ == '__main__':
#     write_nonadherence_posts()

class RegExFinder(ClassifierInt):
    def __init__(self, name:str, RegExCorpus, verbose=False):
        """
        :param: name, the name of the classifier for further reference
        :param RegExCorpus: [WebMD | Twitter], indicate which set of REs will be applied, the REs made for WebMD or made for Twitter
        :param verbose: if true it will out put the debug comments and generate the analysis files in tmp
        """
        assert RegExCorpus=='WebMD' or RegExCorpus=='Twitter', f"I got a corpus name {RegExCorpus} for which I have not REs developed for, check the code."
        if RegExCorpus=='WebMD':
            self.CORPUS = "WebMD"
            self.REGEXES = REGEXES_WebMD
        else:
            self.CORPUS = "Twitter"
            self.REGEXES = REGEXES_Twitter
            
        super(RegExFinder, self).__init__(name)
        # there is no history computed by this classifier
        self.histories = None
        self.verbose = verbose
        

    def train(self, pool: Pool):
        '''
        :param: just run the REs on the validation set of the pool
        :return: history a raw instance of the History for compatibility
        '''
        log.debug("=> I am a manually designed REs classifier, nothing to learn. But I can run your REs on the training and output your score on this set.")
        # we may compute the P/R/F on the training to compute the efficiency of each pattern
        self.classify(pool.trainEx)
        return None
    
    
    def preprocess(self, tweet:series):
        """
        :param tweet: just preprocess the string of the tweet before applying the REs, lower the case
        If we process tweets, we add one more step we anonymize the drug names with the joker __drug_name__
        """
#         if pd.isna(tweet['text']):
#             return ""
        print(f"=> {tweet['text']}")
        twt = re.sub(r'\W(rrb|lrb)\W', " ", tweet['text'])
        twt = re.sub(r'\s{2,}', " ", twt)
        twt = twt.lower()
        # If we are processing tweets we continue by replacing the drug names annotated with a joker __drug_name__
        if self.CORPUS=="Twitter":
            assert 'Drug Name Extracted' in tweet.index, "I was expecting the name of drugs extracted manually in Drug Name Extracted, they are not. Implement an alternative with find them from a dictionary..."
            if not pd.isna(tweet['Drug Name Extracted']):
                drugs = re.split(';|,', tweet['Drug Name Extracted'])
                for drug in drugs:
                    drug = drug.strip().lower()
                    if drug not in twt:
                        log.debug(f"=> can't find the drug name [{drug.strip()}] in the current tweet [{twt}] - original tweet: [{tweet['text']}], I'll just ignore it but REs will not match this tweet. Check the data probably not correctly annotated.")
                    else:
                        twt_replaced = twt.replace(drug, "__drug_name__")
                        if len(drug)<=3:
                            log.debug(f"Before for drug {drug}: {twt}")
                            log.debug(f"Check the replacement of the drug in the tweet because it is a small name [{drug}]: {twt}")
                            log.debug(f"After: {twt_replaced}")
                        twt = twt_replaced
            else:
                log.debug(f"=> can't find the drug name [NaN] in the current tweet [{twt}] - original tweet: [{tweet['text']}], I'll just ignore it but REs will not match this tweet. Check the data probably not correctly annotated.")                
                
        return twt
    
    
    def classify(self, examples: DataFrame) -> DataFrame:
        '''
        :param: examples to classify
        :return: a copy of the examples updated with 1 columns prediction and n columns certainty_label_n:
          - prediction: the best prediction made by the classifier, ie, the label with the highest certainty 
          - certainty_label_n: a column where the classifier assign the probability for the label n for each example
        '''
        regexes_compiled = [re.compile(regex, re.IGNORECASE) for regex in self.REGEXES]
        FPs = {}
        FPs_post = {}
        FNs = []
        TPs = {}
        TPs_post = {}
        evalExamples = examples.copy()
        def applyREs(ex:Series, regexes, FPs, FNs, TPs):
            #preprocess the tweet before applying the REs
            text = self.preprocess(ex)
            matched = 0
            for regex in regexes:
                if re.search(regex, text):
                    if ex['label']==0:
                        if regex not in FPs_post.keys():
                            FPs_post[regex] = []
                        FPs_post[regex].append(text)
                        if regex not in FPs.keys():
                            FPs[regex] = 0
                        FPs[regex] = FPs[regex]+1
#                         log.debug(f"FP => Label: {ex['label']} Example: {text} matched on \n\tPattern: {regex}")
                    if ex['label']==1:
                        if regex not in TPs_post.keys():
                            TPs_post[regex] = []
                        TPs_post[regex].append(text)
                        if regex not in TPs.keys():
                            TPs[regex] = 0
                        TPs[regex] = TPs[regex]+1
#                         log.debug(f"TP => Label: {ex['label']} Example: {text} matched on Pattern: {regex}")
                    matched = 1
            if ex['label']==1 and matched==0:
                FNs.append(text)
#                     log.debug(f"FN => Label: {ex['label']} Example: {text} not matched by any pattern")
            return matched

        evalExamples['prediction'] = evalExamples.apply(lambda post: applyREs(post, regexes_compiled, FPs, FNs, TPs), axis=1)
        #when I output 1 I'm always sure
        evalExamples['certainty_label_1'] = evalExamples['prediction']
        #when I output 0 I'm always sure as well
        def inverse(label):
            if label==1:
                return 0
            elif label==0:
                return 1
            else:
                raise Exception(f"I got an unexpected label {label} in the RegFinder when I was expecting 0 or 1.")
        evalExamples['certainty_label_0'] = evalExamples['prediction'].apply(lambda label: inverse(label))
        
        log.error(f"TODO: finish to compute the certainty of the REs on the training set")
        if self.verbose:
            self.displayDetails(FPs, TPs, FNs, FPs_post, TPs_post)
        
        evalExamples.to_csv('/tmp/output.tsv', sep='\t')
        return evalExamples
    
    
    def displayDetails(self, FPs:dict, TPs:dict, FNs:list, FPs_post:dict, TPs_post:dict):    
        # Everything is computed so we can display the stats...
        #the FNs, not capture by any pattern 
        log.debug(f"Display {len(FNs)} FNs:")
        for fn in FNs:
            log.debug(f"FN - Tweet missed: {fn}")
        self.writeFNs(FNs)
        #display the classification performance by pattern
        count = 0
        log.debug("Display FPs/TPs by patterns:")
        sorted_d = sorted(FPs.items(), key=lambda x: x[1], reverse=True)
        for tup in sorted_d:
            count = count+1
            log.debug(f"Pattern in File n'{count}")
            log.debug(f"FP - Score: {tup[1]} ===> Pattern: {tup[0]}")
            if tup[0] in TPs.keys():
                log.debug(f"TP - Score: {TPs[tup[0]]} ===> Pattern: {tup[0]}")
                # remove the pattern from the list to know which one are in TPs without FPs
                TPs.pop(tup[0])
            else:
                #the pattern was not found in TPs so no actual posts were matched by the pattern, can probably delete it
                log.debug(f"TP - Score: 0 ===> Pattern: {tup[0]}")
            self.writePatternsPosts(count, FPs_post, TPs_post, tup[0])
        sorted_d = sorted(TPs.items(), key=lambda x: x[1], reverse=True)
        log.debug("--- Display TPs without FPs ---")
        for tup in sorted_d:
            count = count+1
            log.debug(f"TP - Score: {tup[1]} ===> Pattern: {tup[0]}")
            assert tup[0] not in FPs.keys(), f"I was expecting that the pattern {tup[0]} was not in the set of FPs, check the code."
            log.debug(f"FP - Score: 0 ===> Pattern {tup[0]}")
            self.writePatternsPosts(count, FPs_post, TPs_post, tup[0])



    def writeFNs(self, FNs:list):
        with open(f"/tmp/FNs.txt", 'w') as file:
            file.write(f"#FNs: {len(FNs)}\n")
            for fn in FNs:
                file.write(f"{fn.lower()}\n\n")
                
                
    def writePatternsPosts(self, counter: int, FP_post:dict, TP_post:dict, pattern: str):
        """
        Write a file in tmp named by the counter and display first the TPs captured by the REs then the FPs
        :param counter: a simple counter to put a name on the file, not related with the indice of th REs in the list of REs
        :param FP_post: a dict with pattern -> [tweet1, tweet2, ...] where the tweets are matched by the pattern, here False Positive
        :param TP_post: a dict with pattern -> [tweet1, tweet2, ...] where the tweets are matched by the pattern, here True Positive
        :param pattern: the pattern we are looking for in the dictionary
        """
        with open(f"/tmp/pattern_{counter}.txt", 'w') as file:
            file.write(f"Pattern: {pattern}\n")
            if pattern in TP_post and pattern in FP_post:
                file.write(f"#TPs: {len(TP_post[pattern])}; #FPs: {len(FP_post[pattern])}\n")
                file.write(f"------------ TPs ------------\n")
                for twt in TP_post[pattern]:
                    file.write(f"{twt.lower()}\n\n")
                file.write(f"------------ FPs ------------\n")
                for twt in FP_post[pattern]:
                    file.write(f"{twt.lower()}\n\n")
            elif pattern in TP_post and pattern not in FP_post:
                file.write(f"#TPs: {len(TP_post[pattern])}; #FPs: 0\n")
                file.write(f"------------ TPs ------------\n")
                for twt in TP_post[pattern]:
                    file.write(f"{twt.lower()}\n\n")
                file.write(f"------------ No FPs ------------\n")
            elif pattern not in TP_post and pattern in FP_post:
                file.write(f"#TPs: 0; #FPs: {len(FP_post[pattern])}\n")
                file.write(f"------------ No TPs ------------\n")
                file.write(f"------------ FPs ------------\n")
                for twt in FP_post[pattern]:
                    file.write(f"{twt.lower()}\n\n")
            else:
                raise Exception(f"I have a pattern that is neither in the list of FP or FN, should not happen: {pattern}")
        

    def save(self, iteration: int):
        log.info("I am a RegEx classifier, nothing to save.")

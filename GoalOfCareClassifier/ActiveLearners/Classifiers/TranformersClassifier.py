'''
Created on Dec 3, 2021

@author: dweissen
'''
import gc
from threading import Thread
import os.path
import configparser
import logging as lg
log = lg.getLogger('TransformersClassifier')

from ActiveLearners.Classifiers.ClassifierInt import ClassifierInt

import numpy as np
import pandas as pd
from pandas.core.frame import DataFrame
from transformers import AutoTokenizer
from transformers import AutoModelForSequenceClassification
import torch
import torch.nn as nn
from datasets import Dataset
from transformers import AutoConfig, AutoModel
from transformers import Trainer, TrainingArguments
from scipy.special import softmax
from sklearn.metrics import accuracy_score, f1_score

#Allows weight on class
class CustomTrainer(Trainer):
    def compute_loss(self, model, inputs, return_outputs=False):
        if torch.cuda.is_available():
            device = torch.device("cuda")
        else:
            device = torch.device("cpu")
        labels = inputs.get("labels")
        # forward pass
        outputs = model(**inputs)
        logits = outputs.get('logits')
        # compute custom loss
        loss_fct = nn.CrossEntropyLoss(weight=torch.tensor([1.0, 1.0]).to(device)) # for multiclass
        #loss_fct = nn.BCEWithLogitsLoss(pos_weight=pos_weight)#torch.tensor([1.0, 5.0])) # for binary classification
        loss = loss_fct(logits.view(-1, self.model.config.num_labels), labels.view(-1))
        return (loss, outputs) if return_outputs else loss


class PyTorchBert(nn.Module):
    """
    Simplified version of the same class by HuggingFace.
    See transformers/modeling_distilbert.py in the transformers repository.
    
    TODO: Should be modified to change the architecture of the classifier, for the moment it is not called, just the default classifier is used
    """

    def __init__(self, pretrained_model_name: str, num_classes: int = 2, dropout: float = 0.4):
        """
        Args:
            pretrained_model_name (str): HuggingFace model name.
                See transformers/modeling_auto.py
            num_classes (int): the number of class labels
                in the classification task
        """
        super().__init__()

        config = AutoConfig.from_pretrained(pretrained_model_name, num_labels=num_classes)
        self.model = AutoModel.from_pretrained(pretrained_model_name, config=config)
        self.classifier = nn.Linear(config.hidden_size, num_classes)
        self.dropout = nn.Dropout(dropout)

    def forward(self, features, attention_mask=None, head_mask=None):
        """Compute class probabilities for the input sequence.
        Args:
            features (torch.Tensor): ids of each token,
                size ([bs, seq_length]
            attention_mask (torch.Tensor): binary tensor, used to select
                tokens which are used to compute attention scores
                in the self-attention heads, size [bs, seq_length]
            head_mask (torch.Tensor): 1.0 in head_mask indicates that
                we keep the head, size: [num_heads]
                or [num_hidden_layers x num_heads]
        Returns:
            PyTorch Tensor with predicted class scores
        """
        #assert attention_mask is not None, "attention mask is none"

        # taking BERTModel output
        # see https://huggingface.co/transformers/model_doc/bert.html#transformers.BertModel
        bert_output = self.model(input_ids=features, attention_mask=attention_mask, head_mask=head_mask)
        # we only need the hidden state here and don't need
        # transformer output, so index 0
        seq_output = bert_output[0]  # (bs, seq_len, dim)
        # mean pooling, i.e. getting average representation of all tokens
        pooled_output = seq_output.mean(axis=1)  # (bs, dim)
        pooled_output = self.dropout(pooled_output)  # (bs, dim)
        scores = self.classifier(pooled_output)  # (bs, num_classes)

        return scores

class TransformersClassifier(ClassifierInt):
    '''
    classdocs
    '''
    def __init__(self, name:str, config: configparser, maxLength = 512, pretrained_model = "bert-base-uncased"):
        '''
        :param name: the name of the classifier to be recognized in an expert commities
        :param config: config access
        :param maxLength: the maximum length of a sentence, for BERT is 512 (above raise an error)
        '''
        super(TransformersClassifier, self).__init__(name)
        log.info(f"A tranformers classifier: {self.getName()} is used, model loaded: {pretrained_model}")
        
        self.config = config
        self.batchSize = 6#keep the batch size low otherwise, at 64 we run out of memory after the 2 AL
        self.epoch = 5
        #self.weight_class = Modify CustomTrainer to add the tensor representing the weights...
        self.maxLength = maxLength # the max size of this model
        self.model_name = f"{self.getName()}-finetuned-{pretrained_model}"
        self.InitialCheckpointPath = self.config['Data']['tmpFolder']+f"InitialModel_{self.getName()}-finetuned-{pretrained_model.replace('/','_')}"
        self.CurrentCheckpointPath = self.config['Data']['tmpFolder']+f"CurrentModel_{self.getName()}-finetuned-{pretrained_model.replace('/','_')}"
        
        #On the server we don't have access to internet, the model need to be downloaded locally, check if they are there
        if os.path.exists(self.config['Data']['tmpFolder']+f"/cacheHF/Tokenizer/tokenizer_{self.getName()}-finetuned-{pretrained_model.replace('/','_')}"):
            self.tokenizer = AutoTokenizer.from_pretrained(local_files_only=True, pretrained_model_name_or_path=self.config['Data']['tmpFolder']+f"/cacheHF/Tokenizer/tokenizer_{self.getName()}-finetuned-{pretrained_model.replace('/','_')}")
        #otherwise we try to download them from hf, will only work on the local machine
        else:
            self.tokenizer = AutoTokenizer.from_pretrained(pretrained_model)
            self.tokenizer.save_pretrained(self.config['Data']['tmpFolder']+f"/cacheHF/Tokenizer/tokenizer_{self.getName()}-finetuned-{pretrained_model.replace('/','_')}")
            
        #On the server we don't have access to internet, the model need to be downloaded locally, check if they are there
        if os.path.exists(self.config['Data']['tmpFolder']+f"/cacheHF/Model/InitialModel_{self.getName()}-finetuned-{pretrained_model.replace('/','_')}"):
            model = (AutoModelForSequenceClassification.from_pretrained(pretrained_model_name_or_path=self.config['Data']['tmpFolder']+f"/cacheHF/Model/InitialModel_{self.getName()}-finetuned-{pretrained_model.replace('/','_')}", num_labels=2))
        #otherwise we try to download them from hf, will only work on the local machine
        else:
            model = (AutoModelForSequenceClassification.from_pretrained(pretrained_model, num_labels=2))
            model.save_pretrained(self.config['Data']['tmpFolder']+f"/cacheHF/Model/InitialModel_{self.getName()}-finetuned-{pretrained_model.replace('/','_')}")
        model.save_pretrained(self.getInitialCheckpointPath(), push_to_hub=False)
        model.save_pretrained(self.getCurrentCheckpointPath(), push_to_hub=False)
        
        if torch.cuda.is_available():
            log.info("GPU detected, the code will be running on this device")
            self.device = torch.device("cuda")
        else:
            log.warning("GPU NOT detected... the code will be running on CPU, make sure this is what you want to do.")
            self.device = torch.device("cpu")

        log.info(f"The Transformer classifier is created with the pretrained-embbedings: {pretrained_model}")
    
    
    def __DF2DS(self, df:DataFrame) -> Dataset:
        """
        Convert a DF into a DS supported by hf transformers
        """
#         df.to_csv(self.config["Data"]["tmpFolder"]+"tmpDF.csv")
#         ds = load_dataset('csv', data_files={"tag":[self.config["Data"]["tmpFolder"]+"tmpDF.csv"]})
        #make sure the text column is in string otherwise all "NA" and "N/A" are interpreted 
        df["text"] = df["text"].astype('str')
        df["note_id"] = df["note_id"].astype('str')
        #pyarrow.lib.ArrowTypeError: ("Expected bytes, got a 'int' object", 'Conversion failed for column note_id with type object')
        ds1 = Dataset.from_pandas(df)
        return ds1
    
    def __tokenize(self, batch):
        try:
            return self.tokenizer(batch["text"], padding=True, truncation=True, max_length=self.maxLength)
        except:
            log.error("An error occurred when tokenizing the utterances, searching which one causes the crash:")
            cmp = 0
            for text in batch["text"]:
                try:
                    tokens = self.tokenizer(text)
                    cmp = cmp + 1
                except Exception as e:
                    log.error(f"Error with ID:{batch['docID'][cmp]} -> [{text}], Exception: {e}")

    def train(self, trainExamples: DataFrame, evalExamples: DataFrame, checkPointPath:str):
        '''
        :param: trainExamples train the classifier on the training examples given
        :param: evalExamples the validation set used during the training
        :param pathCheckPoint: a valid path to the checkpoint to train
        :return: a model updated, the model is saved by default
        '''
        
        log.debug("I delete any existing model and trainer")
        self.model = None
        self.trainer = None
        del self.model
        del self.trainer
        gc.collect()
        torch.cuda.empty_cache()
        log.debug(f"Memory allocated at the beginning of training (should be 0):{torch.cuda.memory_allocated()}")
        log.debug(f"Memory reserved at the beginning of training (should be 0):{torch.cuda.memory_reserved()}")
        
        trainEncoded = self.__DF2DS(trainExamples).map(self.__tokenize, batched=True, batch_size=None)
        valEncoded = self.__DF2DS(evalExamples).map(self.__tokenize, batched=True, batch_size=None)
        
        logging_steps = len(trainExamples) // self.batchSize
        
        log.info(f"Continue training from model located @{checkPointPath}")
        training_args = TrainingArguments(resume_from_checkpoint=checkPointPath,
                                          output_dir=checkPointPath,
                                          num_train_epochs=self.epoch,
                                          learning_rate=2e-5,
                                          per_device_train_batch_size=self.batchSize,
                                          per_device_eval_batch_size=self.batchSize,
                                          weight_decay=0.01,
                                          #eval_accumulation_steps=10,
                                          evaluation_strategy="epoch",
                                          save_strategy = "epoch",
                                          load_best_model_at_end=True,#reload the best model at the end of the training
                                          metric_for_best_model="f1",
                                          greater_is_better=True,
                                          disable_tqdm=False,
                                          logging_steps=logging_steps,
                                          push_to_hub=False)
                                          #log_level="info")
    
        def compute_metrics(pred):
            labels = pred.label_ids
            preds = pred.predictions.argmax(-1)
            f1 = f1_score(labels, preds, average="binary", pos_label=1)
            acc = accuracy_score(labels, preds)
            return {"accuracy": acc, "f1": f1}

        self.model = (AutoModelForSequenceClassification.from_pretrained(checkPointPath, num_labels=2).to(self.device))
        log.debug(f"I just loaded the model @{checkPointPath}")
#         log.fatal("!!!!!!!!!!!!!!!!!!!!!I FREEZE THE WEIGHT IN EMBEDDINGS FOR SPEED!!!!!!!!!!!!!!!!!!!!!!!!!!")
#         for param in self.model.bert.parameters():
#             param.requires_grad = False
        
        #load the current model (initial model if we are learning on the seed or if incremental training is false, otherwise we are loading the best model from the last iteration
        self.trainer = CustomTrainer(model=self.model,
                                args=training_args,
                                compute_metrics=compute_metrics,
                                train_dataset=trainEncoded,
                                eval_dataset=valEncoded,
                                tokenizer=self.tokenizer)
#        self.trainer = Trainer(model=self.model,
#                          args=training_args,
#                          compute_metrics=compute_metrics,
#                          train_dataset=trainEncoded,
#                          eval_dataset=valEncoded,
#                          tokenizer=self.tokenizer)
        
        log.debug(f"start training classifier {self.getName()}")
        #log.fatal("!!!!!!!!!!!!!!!!!!!!!I COMMENTED THE TRAINING FOR SPEED!!!!!!!!!!!!!!!!!!!!!!!!!!")
        self.trainer.train();
        log.debug(f"classifier {self.getName()} is trained.")
        self.model.save_pretrained(self.getCurrentCheckpointPath(), push_to_hub=False)
        log.debug(f"Model saved @{self.getCurrentCheckpointPath()}.")
        log.debug(f"Memory allocated at the end of training (should be >0):{torch.cuda.memory_allocated()}")
        log.debug(f"Memory reserved at the end of training (should be >0):{torch.cuda.memory_reserved()}")

    
    def classify(self, examples:DataFrame) -> DataFrame:
        '''
        :param: examples to classify
        :return: run a binary classification the examples and return the DataFrame updated with 3 columns: prediction and 2 columns certainty_label_0/1:
          - prediction: the best prediction made by the classifier, ie, the label with the highest certainty 
          - certainty_label_0/1: a column where the classifier assign the probability for the label 0/1 for each example
        '''
        model = (AutoModelForSequenceClassification.from_pretrained(self.getCurrentCheckpointPath(), num_labels=2).to(self.device))
        log.debug(f"Model loaded for prediction from @{self.getCurrentCheckpointPath()}.")
        #should not output anything since we use it for prediction, we give the arg to remove the warning in the log
        training_args = TrainingArguments(output_dir=self.config['Data']['tmpFolder'])
        trainer = Trainer(model=model, args=training_args)
        
        exEncoded = self.__DF2DS(examples).map(self.__tokenize, batched=True, batch_size=None)
        preds_output = trainer.predict(exEncoded)
        #log.info(f"performance on the set of examples to classify: {preds_output.metrics}")
        df = pd.DataFrame(softmax(preds_output.predictions, axis=1), columns = ['certainty_label_0', 'certainty_label_1'], index=examples.index)
        examplesWPred = pd.concat([examples, df], axis=1)
        examplesWPred["prediction"] = np.argmax(preds_output.predictions, axis=1)
        assert len(examples)==len(examplesWPred), "Wrong concatenation between the predictions and the examples, check the code"
        
        return examplesWPred

    
    def save(self, iteration: int):
        '''
        Save the model at the iteration in the tmp folder
        '''
        log.debug(f"classifier {self.getName()} is trained.")
        pathFile = self.config["Data"]["tmpFolder"]+self.getName()+"_I"+str(iteration)+".pkl"
        self.model.save_pretrained(pathFile, push_to_hub=False)
        log.debug(f"Model saved @pathFile.")

    def load(self, pathModel):
        '''
        Load a valid model
        :param pathModel: a path to the model to load 
        '''
        self.model = (AutoModelForSequenceClassification.from_pretrained(pathModel, num_labels=2).to(self.device))
        log.debug(f"Model loaded for prediction from @{pathModel}.")
    
    def getInitialCheckpointPath(self) -> str:
        return self.InitialCheckpointPath
    
    def getCurrentCheckpointPath(self) -> str:
        return self.CurrentCheckpointPath

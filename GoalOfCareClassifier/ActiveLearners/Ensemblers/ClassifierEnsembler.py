'''
Created on Mar 18, 2022

@author: dweissen
'''

import logging as lg
from pandas.core.series import Series
log = lg.getLogger('DecisionTreeEnsembler')
from pandas.core.frame import DataFrame
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import OneHotEncoder
from sklearn import tree
from sklearn.compose import ColumnTransformer


from ActiveLearners.Ensemblers.EnsemblerInt import EnsemblerInt

class ClassifierEnsembler(EnsemblerInt):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        self.features_transformer = TfidfVectorizer(stop_words='english', ngram_range=(1, 2), lowercase=True, max_features=150000)
        self.classifier = tree.DecisionTreeClassifier()
        return

    def __binProbs(self, row:Series)->str:
        """
        Bin the probability of the prediction from a classifier into buckets:
        [0-25)  -> very-unlikely
        [25-50) -> unlikely
        [50-75) -> likely
        [75-100] -> very-likely
        """
        prob=row["certainty_label_1"]
        if 0<=prob and prob<0.25:
            return "very-unlikely"
        elif 0.25<=prob and prob<0.5:
            return "unlikely"
        elif 0.5<=prob and prob<0.75:
            return "likely"
        elif 0.75<=prob and prob<=1.0:
            return "very-likely"
        else:
            raise Exception(f"I was expecting a probability representing the certainty of the classifier but I was given: {prob}, I can't bin the value.")
        

    def train(self, trainingExamples: DataFrame, valExamples: DataFrame):
        '''
        The decision tree is train on the training set given and evaluated on the validation set 
        :param trainingExamples: used for training the tree, expect the column text and label
        :param valExamples: used for evaluation
        :return: the model train for further classification
        '''
        log.debug(f"start training the ClassifierEnsembler")
        classifiersPredsFeatures = DataFrame(index=trainingExamples.index)
        tuples = []
        #first compute the probabilities of each classifier
        for classifier in self.committee.getClassifiers():
            predictions = classifier.classify(trainingExamples)
            classifiersPredsFeatures[classifier.getName()+"_certainty_binned_label_1"] = predictions.apply(self.__binProbs, axis=1)
            import numpy as np
            cat = list(np.array(["very-unlikely", "unlikely", "likely", "very-likely"]).reshape(1,4))
            tuples.append((classifier.getName()+"_preds", OneHotEncoder(categories=cat, dtype='int'), [classifier.getName()+"_certainty_binned_label_1"]))
        #then add the token of the text
        classifiersPredsFeatures["text"] = trainingExamples["text"]
        tuples.append(("text_tfidf", TfidfVectorizer(stop_words='english', ngram_range=(1, 2), lowercase=True, max_features=150000), 'text'))
        
        self.features_transformer = ColumnTransformer(tuples)
        train_features = self.features_transformer.fit_transform(classifiersPredsFeatures)
        #train_tfidf = self.text_transformer.fit_transform(trainingExamples['text'].values.astype('str'))
        #self.classifier.fit(train_tfidf, trainingExamples['label'])
        self.classifier.fit(train_features, trainingExamples['label'])
        log.debug(f"ClassifierEnsembler is trained.")
        treeLearned = tree.export_text(self.classifier, feature_names=self.features_transformer.get_feature_names())
        log.debug(f"Tree learned on the training set: \n{treeLearned}")
        return
    
    def classify(self, examples: DataFrame) -> DataFrame:
        '''
        :param: predictions made by the different experts
        :return: the final decision taken 
        the dataframe returned with 2 columns 
          - prediction: the best prediction made by the classifier, ie, the label with the highest certainty 
          - certainty_label_0/1: a column where the classifier assign the probability for the label 0/1 for each example
        '''
        log.info("Start predicting the labels of unknown dataset...")
        cpExamples = examples.copy()
        classifiersPredsFeatures = DataFrame(index=cpExamples.index)
        for classifier in self.committee.getClassifiers():
            predictions = classifier.classify(cpExamples)
            classifiersPredsFeatures[classifier.getName()+"_certainty_binned_label_1"] = predictions.apply(self.__binProbs, axis=1)
        classifiersPredsFeatures["text"] = cpExamples["text"]
        examples_features = self.features_transformer.transform(classifiersPredsFeatures)
        preds = self.classifier.predict(examples_features)
        probs = self.classifier.predict_proba(examples_features)
        cpExamples['prediction'] = preds
        cpExamples['certainty_label_0'], cpExamples['certainty_label_1'] = probs[:, 0], probs[:, 1]
        return cpExamples
'''
Created on Mar 18, 2022

@author: dweissen
'''

import abc
from pandas.core.frame import DataFrame

class EnsemblerInt(object):
    '''
    Interface for an ensembler used by an expert committee
    '''
    
    @abc.abstractmethod
    def setExpertCommittee(self, committee):
        """
        :param committee: a reference to the expert committee the ensembler will ensemble 
        """
        self.committee = committee

    @abc.abstractmethod
    def train(self, trainingExamples: DataFrame, valExamples: DataFrame):
        '''
        :param trainingExamples: a valid training set with the document and their labels
        :param valExamples: a validation set with the document and their labels
        '''
        pass

    @abc.abstractmethod
    def classify(self, examples: DataFrame) -> DataFrame:
        '''
        :param: predictions made by the different experts
        :return: the final decision taken as the mean of each probability assigned by the experts (no weights)
        the dataframe returned with 2 columns 
          - prediction: the best prediction made by the classifier, ie, the label with the highest certainty 
          - certainty_label_0/1: a column where the classifier assign the probability for the label 0/1 for each example
        '''
        pass
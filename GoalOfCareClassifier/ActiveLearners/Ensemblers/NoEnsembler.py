'''
Created on Mar 18, 2022

@author: dweissen
'''

from pandas.core.frame import DataFrame

class NoEnsembler(object):
    '''
    A default ensembler used when only one classifier is used, in that case nothing is done
    '''
    def train(self, trainingExamples: DataFrame, valExamples: DataFrame):
        '''
        It's a No ensembler, nothing will be trained.
        '''
        return
    
    def classify(self, examples: DataFrame):
        '''
        It's a No ensembler, it returns the decision made by the classifier
        '''
        raise Exception("You are calling the classification function of the No Ensembler, which should not be.")
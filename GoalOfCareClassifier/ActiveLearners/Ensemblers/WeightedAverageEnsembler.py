'''
Created on Mar 18, 2022

@author: dweissen
'''

import logging as lg
from pandas.core.series import Series
log = lg.getLogger('WeightedAverageEnsembler')
from pandas.core.frame import DataFrame
import numpy as np
from sklearn.metrics import precision_score, recall_score, f1_score, confusion_matrix, classification_report, accuracy_score

from ActiveLearners.Ensemblers.EnsemblerInt import EnsemblerInt

class WeightedAverageEnsembler(EnsemblerInt):
    '''
    A weighted average voting by taking the mean of the certainty of each classifier weighted by their performances on the validation set
    '''

    def __init__(self, weights="Equal"):
        '''
        A weighted average ensembler
        :param weights: [Equal|Learned],
        if Equal all weight are set to 1 and the average of the certainty is returned
        if Learner the validation set is used to learned the weights and 
        '''
        assert weights=="Equal" or weights=="Learned", f"I received an unknown parameter for the weighted average ensembler:{weights}"
        self.weightsOption = weights
        self.weights = None
        return
    
    def train(self, trainingExamples: DataFrame, valExamples: DataFrame):
        '''
        The weight are computed using the f1 score for the positive class (1) of the classifiers on the validation set
        :param trainingExamples: not used for this ensembler
        :param valExamples: a df with the examples of the validation set annotated
        :return: self.weights are instantiated with the f1 on the positive obtained by each classifier on the validation set
        '''
        self.weights = {}
        if self.weightsOption=="Equal":
            for classifier in self.committee.getClassifiers():
                self.weights[classifier.getName()+"_certainty_label_1"] = 1
        else:#self.weightsOption=="Learned"
            for classifier in self.committee.getClassifiers():
                predictions = classifier.classify(valExamples)
                assert len(predictions.index.intersection(valExamples.index))==len(predictions.index), f"There is no index mapping between the predicted and the truth set of examples, check the code."
                assert 'prediction' in predictions.columns and 'label' in valExamples.columns, "The examples predicted or gold truth do not have the expected label columns, check code."
                df = DataFrame({'docIDTruth':valExamples.index, 'truths':valExamples['label'], 'docIDPredicted':predictions.index, 'preds':predictions['prediction']})
                assert(np.array_equal(df['docIDTruth'], df['docIDPredicted'])), "I have differences between the lists of docIDs from the truth and the predicted which should be identical lists, check the code..."
                f1 = f1_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')        
                self.weights[classifier.getName()+"_certainty_label_1"] = f1
        return
        
    
    def classify(self, examples: DataFrame) -> DataFrame:
        '''
        :param: predictions made by the different experts
        :return: the final decision taken as the weighted mean of each probability assigned by the experts
        the dataframe returned with 2 columns 
          - prediction: the best prediction made by the classifier, ie, the label with the highest certainty 
          - certainty_label_0/1: a column where the classifier assign the probability for the label 0/1 for each example
        '''
        predictions = DataFrame(index=examples.index)
        assert len(self.weights)==len(self.committee.getClassifiers()), "Why on earth do I have the vector of weights having a different length than the set of classifiers to weights? Check the code."
        cmp = 0
        for expert in self.committee.getClassifiers():
            pred = expert.classify(examples.copy(deep=True))
            assert 'prediction' in pred.columns, "One classifier did not return a label column has it should, change the code."
            #TODO: I'm coding for binary classification, (in rush as usual...), extend to multinomial 
            assert "certainty_label_1" in pred.columns,  "One classifier did not return a certainty_label_1 column has it should, change the code." 
            predictions[expert.getName()+"_certainty_label_1"] = pred["certainty_label_1"]
            examples[expert.getName()+"_certainty_label_1"] = pred["certainty_label_1"]
            examples[expert.getName()+"_certainty_label_0"] = 1-examples[expert.getName()+"_certainty_label_1"]
            cmp = cmp+1
        
        def weightedAverage(example:Series, weigths:dict) -> float:
            weightedProbs = []
            for classifierName in weigths:
                weightedProbs.append(example[classifierName]*weigths[classifierName])
            weightedAverage = sum(weightedProbs)/sum(weigths.values())
            assert (0<=weightedAverage and weightedAverage<=1), f"There is an issue with the weighted average, it should be a probability but it is not: weights for 0->{weigths}, example: {example}"  
            return weightedAverage
        #now I can compute the weighted average for the examples to be labeled 0
        examples['certainty_label_1'] = examples.apply(weightedAverage, axis=1, args=[self.weights])
        examples['certainty_label_0'] = 1-examples['certainty_label_1']
        def __predictClass__(mean):
            if mean>=0.5: return 1
            return 0
        examples['prediction'] = examples['certainty_label_1'].apply(lambda mean1: __predictClass__(mean1))
        return examples
        
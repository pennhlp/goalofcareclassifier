'''
Created on Oct 8, 2019

@author: davy
'''
import logging as lg
log = lg.getLogger('ExpertCommittee')

import configparser
import pandas as pd
from pandas.core.frame import DataFrame
import numpy as np
from enum import Enum
import scipy.stats as spy
from typing import List

from Data.Pool import Pool
from Evaluation.EvaluaterInt import EvaluaterInt
from ActiveLearners.ActiveLearner import ActiveLearner
from ActiveLearners.Classifiers.ClassifierInt import ClassifierInt
from ActiveLearners.Ensemblers.EnsemblerInt import EnsemblerInt


class DisagreementAlgorithm(Enum):
    '''
    A simple Enum class to enumerate the disagreement algorithms implemented and ready to be used by the committee through their querying algorithm
    '''
    voteEntropy = 1
    consensusEntropy = 2
    maxDisagreement = 3

class ECQueryingAlgorithm(object):
    '''
    Since we will have to try multiple querying algorithms to get the best I just externalize the querying algorithm from the committee expert
    it instantiate only with one algorithm and always return the result of this algorithm when asked for the most informative examples
    
    For the algorithms I re-implement those given by Burr Settles "active learning"
    See for examples: https://modal-python.readthedocs.io/en/latest/content/query_strategies/Disagreement-sampling.html#disagreement-sampling-for-classifiers
    '''
    
    def __init__(self, algorithm: DisagreementAlgorithm, committee: ActiveLearner):
        '''
        :param: instantiate the querying algorithm to be used, only disagreement algorithms and listed in the enum DisagreementAlgorithm are implemented
        '''
        self.algorithm = algorithm
        self.committee = committee
    
    def getAlgorithmName(self) -> DisagreementAlgorithm:
        return self.algorithm
    
    def getExamplesToQuery(self, examples: DataFrame):
        log.info(f"I compute miExamples with {self.algorithm}")
        if self.algorithm == DisagreementAlgorithm.voteEntropy:
            return self.__getVoteEntropyRankedExamples(examples)
        elif self.algorithm == DisagreementAlgorithm.consensusEntropy:
            return self.__getConsensusEntropyRankedExamples(examples)
        elif self.algorithm == DisagreementAlgorithm.maxDisagreement:
            return self.__getMaxDisagreementRankedExamples(examples)
        else:
            raise Exception(f"Unexpected querying algorithm name has been asked: {self.getAlgorithmName()}, check the code.")

    def __getVoteEntropyRankedExamples(self, predictions: DataFrame):
        '''
        :param: expect a dataframe with columns, label_i and certainty_i, for each classifier i, where their respective predictions are given for each examples
        :return: the examples ranked according to the entropy of the vote distribution
        Assuming predictions for 5 examples by 3 classifiers [[0, 1, 0], [1, 1, 2], [0, 1, 2], [2, 2, 2], [1, 2, 2]]
        We have the probability for the vote of the first example 0: 0.6666, 1: 0.3333, 2: 0.0; for the vote of the second example 0: 0.0, 1: 0.6666, 2: 0.3333 etc.
        The entropy is then computed 
        for ex 1: - 0.6666*ln(0.6666) - 0.3333*ln(0.3333) - 0*ln(0.0) = 0.6365
        for ex 2: - 0*ln(0.0) - 0.6666*ln(0.6666) - 0.3333*ln(0.3333) = 0.6365
        If we make all computations for our example we have [0.6365, 0.6365, 1.0986, 0.0, 0.6365], so ex.3 should be returned, then 1,2,5 and last ex.4
        '''
        #I count the number of 0s 
        vote0 = predictions.filter(regex='^label_[0-9]+$').isin([0]).sum(1)
        # then divide by the number of classifiers
        probVote0 = vote0.divide(len(predictions.filter(regex='^label_[0-9]+$').columns))
        # and compute the entropy for each row
        predictions['entropy'] = spy.entropy([probVote0, (1-probVote0)])
        # I rank the examples by their entropy, higher first
        predictions.sort_values(by='entropy', ascending=False, inplace=True)
        #remove the now useless column entropy
        predictions.drop(columns='entropy', inplace=True)
        # and return our newly correctly ranked dataframe
        return predictions
    
    def __getConsensusEntropyRankedExamples(self, predictions: DataFrame):
        '''
        :param: expect a dataframe with columns: label_i and certainty_i for each classifier i, where their respective predictions are given for each examples and the probabilities assigned in the certainty columns
        :return: the examples ranked according to the entropy of the vote distribution computed this time based on the probability assign for each possible label by each classifier
        Assuming predictions for each 3 possible labels of 5 examples by 3 classifiers 
        [[[0.9, 0.1, 0.0]    # \
        [0.3, 0.7, 0.0]    # |
        [1.0, 0.0, 0.0]    # |  <-- class probabilities for the first classifier
        [0.2, 0.2, 0.6]    # |
        [0.2, 0.7, 0.1]],  # /
        [[0.0, 1.0, 0.0]    # \
        [0.4, 0.6, 0.0]    # |
        [0.2, 0.7, 0.1]    # |  <-- class probabilities for the second classifier
        [0.3, 0.1, 0.6]    # |
        [0.0, 0.0, 1.0]],  # /
        [[0.7, 0.2, 0.1]    # \
        [0.4, 0.0, 0.6]    # |
        [0.3, 0.2, 0.5]    # |  <-- class probabilities for the third classifier
        [0.1, 0.0, 0.9]    # |
        [0.0, 0.1, 0.9]]]
        If we take the predictions of ex.2, we have C1: [0.3, 0.7, 0.0], C2: [0.4, 0.6, 0.0], C3: [0.4, 0.0, 0.6], if we take the mean for each possible label, 
        i.e. for label 1 (0.3+0.4+0.4)/3=0.366; for label 2 (0.7+0.6+0.0)/3=0.433, for label 3 (0.0+0.0+0.6)/3=0.2
        we can now compute the entropy for each prediction: -0.366*LN(0.366) -0.433*LN(0.433) -0.2*LN(0.2) = 1.0521908
        for all examples: 
        prob predictions: [[0.5   , 0.4333, 0.0333], [0.3666, 0.4333, 0.2   ], [0.5   , 0.3   , 0.2   ], [0.2   , 0.1   , 0.7   ], [0.0666, 0.2666, 0.6666]]
        entropy: [0.8167, 1.0521, 1.0296, 0.8018, 0.8033]
        the ranks returned are then ex 2,3,1,5,4
        '''
        # First I get the mean of the certainty of all classifiers for an example to be label 0
        meanL0 = predictions.filter(regex='^certainty_label_0_C[0-9]+$').mean(axis=1)
        # I compute the entropy for these mean
        predictions['entropy'] = spy.entropy([meanL0, (1-meanL0)])
        # I rank the examples by their entropy, higher first
        predictions.sort_values(by='entropy', ascending=False, inplace=True)
        #remove the now useless column entropy
        predictions.drop(columns='entropy', inplace=True)
        # and return our newly correctly ranked dataframe     
        return predictions
    
    def __getMaxDisagreementRankedExamples(self, predictions: DataFrame):
        '''
        :param: expect a dataframe with columns: label_i and certainty_i for each classifier i, where their respective predictions are given for each examples and the probabilities assigned in the certainty columns
        :return: the examples ranked according to the Kullback-Leibler divergence of each learner to the consensus prediction (we choose the example with the highest divergence computed)
        Resource for KL divergence: https://towardsdatascience.com/kl-divergence-python-example-b87069e4b810
        Assuming the same example and prediction used for the consensus entropy (self.__getConsensusEntropyRankedExamples), we can compute the probability of each prediction and get again: 
        [[0.5   , 0.4333, 0.0333]
         [0.3666, 0.4333, 0.2   ]
         [0.5   , 0.3   , 0.2   ]
         [0.2   , 0.1   , 0.7   ]
         [0.0666, 0.2666, 0.6666]]
         
        For ex.2 we have [0.3666, 0.4333, 0.2   ] computed from the votes: C1: [0.3(a), 0.7, 0.0], C2: [0.4, 0.6(b), 0.0], C3: [0.4, 0.0, 0.6(c)]
        The distance is compute by comparing each individual vote with the means of all votes, i.e.:
        print(spy.entropy([0.3, 0.7, 0.0], qk=[0.3666, 0.4333, 0.2])) = 0.2755083411831241  (C1)
        print(spy.entropy([0.4, 0.6, 0.0], qk=[0.3666, 0.4333, 0.2])) = 0.23007687556278716 (C2)
        print(spy.entropy([0.4, 0.0, 0.6], qk=[0.3666, 0.4333, 0.2])) = 0.6939446528814834  (C3)

        This should give [0.27549995,  0.23005799,  0.69397192] for ex.2, for all examples:
             C1            C2           C3
        [[0.32631363,  0.80234647,  0.15685227],
         [0.27549995,  0.23005799,  0.69397192], <-- ex.2
         [0.69314718,  0.34053564,  0.22380466],
         [0.04613903,  0.02914912,  0.15686827],
         [0.70556709,  0.40546511,  0.17201121]]
        Then we just take the max of each list to reorder the examples:
        [0.80234647, 0.69397192, 0.69314718, 0.15686827, 0.70556709], giving examples: 1,5,2,3,4
        '''
        # First I get the mean of the certainty of all classifiers for an example to be label 0
        meanL0 = predictions.filter(regex='^certainty_label_0_C[0-9]+$').mean(axis=1)
        # then I use the certainty for the labels assigned by each classifier to compute the KL distance to the consensus vote
        # and add it in the respective column
        for ind, _ in enumerate(self.committee.getClassifiers()):
            predictions['KLDistance_C'+str(ind)] = spy.entropy([predictions['certainty_label_0_C'+str(ind)], predictions['certainty_label_1_C'+str(ind)]], qk=[meanL0, (1-meanL0)])
        # then select the max KLDistance for each example
        predictions['maxKLDistance'] = predictions.filter(regex='^KLDistance_C[0-9]+$').max(axis=1)
        # and reorder the examples based on the max
        predictions.sort_values(by='maxKLDistance', ascending=False, inplace=True)
        predictions.drop(columns=predictions.filter(regex='^KLDistance_C[0-9]+$'), inplace=True)
        predictions.drop(columns='maxKLDistance', inplace=True)
                
        return predictions

class ExpertCommittee(ActiveLearner):
    '''
    Create a committee of experts to run query on unlabel examples
    This committee only handle binary classification
    
    '''

    def __init__(self, config:configparser, ensembler:EnsemblerInt, classifiers: List[ClassifierInt], evaluater:EvaluaterInt = None, trainingArgs:dict = None):
        '''
        Create the committee with all classifiers given, classifiers do not have to be all supervised but they should implement the service of their interface
        :param: expect a list of fully instantiated list of classifiers implementing the classifierInt services
        '''
        super(ActiveLearner, self).__init__(config, ensembler, classifiers, evaluater, trainingArgs)
        
        self.ensembler = ensembler
        self.ensembler.setExpertCommittee(self)
        self.trainingArgs = trainingArgs
        self.__setClassifiers(classifiers)        
        self._queryingAlgo: Optional[QueryingAlgorithm] = None
    
    def __setClassifiers(self, classifiers: List[ClassifierInt]):
        expertsNames = []
        #check the experts have unique names
        for expert in classifiers:
            expertsNames.append(expert.getName())
        for expertName in expertsNames:
            assert (expertsNames.count(expertName) == 1), f'Two experts have the same name, they should be uniquely identify, check your code' 
        for expert in classifiers:
            assert isinstance(expert, ClassifierInt), f"I was expecting an instance of ClassifierInt in the expert committee, got {type(expert)}, an incompatible object, check the code."
            assert len(classifiers)>=2, f"A committee of experts need more than one expert... Instantiate the object with more than one expert in the list or use an uncertainty sampler if only one classifier is available."


    def setQueryingAlgorithm(self, queryingAlgorithm: ECQueryingAlgorithm):
        '''
        :param: queryingAlgorithm the querying algorithm to be used to select the most informative examples, for an expert committee a disagreement based algorithm
        '''
        assert isinstance(queryingAlgorithm , ECQueryingAlgorithm), f"I was expecting an instance of QueryingAlgorithm, I got {queryingAlgorithm.__class__.__name__}, check the code."
        self._queryingAlgo = queryingAlgorithm
        
        
    def getQueryingAlgorithm(self) -> ECQueryingAlgorithm:
        '''
        :return: the querying algorithm set for the committee, 
        :raise exception: if the querying algorithm has not been set yet
        '''
        assert self._queryingAlgo is not None, "The code is calling for a querying algorithm which has not been instantiated in the expert committee yet."
        return self._queryingAlgo
                
        
    def getMostInformativeExamples(self, pool: Pool, ExQueriedBatchSize: int) -> DataFrame:
        '''
        :param pool: a pool with all unlabeled data
        :param ExQueriedBatchSize: the batch size of examples to query in the unlabeled set
        :return: a set of examples that should be the most informative to annotate,
                 the examples have a label and certainty columns set to NaN
        '''
        for ind, exp in enumerate(self.getClassifiers()):
            #take a copy since we don't want to keep the predictions of the experts in the pool...
            cpUnlabelExs = pool.unlabEx.copy(deep=True)
            cpUnlabelExs = exp.classify(cpUnlabelExs)
            assert set(['certainty_label_0', 'certainty_label_1', 'prediction']).issubset(cpUnlabelExs.columns), f"The classifier {exp.getName()} did not return a dtatframe with the expected columns: 'prediction', 'certainty_label_0', 'certainty_label_1', check the code"  
            cpUnlabelExs.rename(columns={"prediction":"prediction_"+str(ind), "certainty_label_0":"certainty_label_0_C"+str(ind), "certainty_label_1":"certainty_label_1_C"+str(ind)}, inplace=True)
            if ind==0:
                predictions = cpUnlabelExs                
            else:
                predictions = predictions.join(cpUnlabelExs[["prediction_"+str(ind), "certainty_label_0_C"+str(ind), "certainty_label_1_C"+str(ind)]], on='docID')
        
        #we have the predictions for each expert, now we select the most informative based on the querying algorithm
        log.info(f"I run the querying algorithm {self.getQueryingAlgorithm().getAlgorithmName()}")
        miExamples = self.getQueryingAlgorithm().getExamplesToQuery(predictions)
        if self.trainingArgs["biasTowardMinorityClass"] is not None:
            miExamples = self.__biasTowardMinorityClass(miExamples, ExQueriedBatchSize)
        else:
            miExamples = DataFrame(miExamples[0:ExQueriedBatchSize])

        #now I can remove the useless columns
        miExamples.drop(columns=miExamples.filter(regex='^prediction_[0-9]+$'), inplace=True)
        miExamples.drop(columns=miExamples.filter(regex='^certainty_label_[0-9]+_C[0-9]+$'), inplace=True)
        # and add the column empty for annotation
        miExamples['label'] = np.NaN
        
        return miExamples        
    
    def __biasTowardMinorityClass(self, miExamples:DataFrame, ExQueriedBatchSize: int)->DataFrame:
        """
        bias the choice of the most informative examples by selecting first the examples which were predicted as 1 with a majority voting 
        (if there is a tie between the experts, chose 0)
        :param miExamples: the predictions on the unlabeled set ordered by the quantity of information of each example measure by the metric chosen
        :return: the number expected of expected examples first filled with examples less likely predicted 1, then the 0s 
        """
        log.info("I am biasing the query of the most informative examples toward the minority class")
        #the examples are ordered by the disagreement between the classifiers, we take first the examples where the majority voted for the minority class
        miExamples["majorityPrediction"] = miExamples.filter(regex='^prediction_[0-9]+$').mode(axis=1)[0]
        predictions1 = miExamples[miExamples["majorityPrediction"]==1]
        log.info(f"The classifiers have predicted {len(predictions1)} examples as belonging to the minority class, we need {ExQueriedBatchSize}, will complete with the most uncerstain examples of the majority class if needed.")
        #then we complete with the examples where the majority voted for the majority class 
        if len(predictions1)>ExQueriedBatchSize:
            miExamples = DataFrame(predictions1[0:ExQueriedBatchSize])
        else:
            df1 = DataFrame(predictions1)
            df0 = DataFrame(miExamples[miExamples["majorityPrediction"]==0][0:ExQueriedBatchSize-len(df1)])
            miExamples = pd.concat([df1,df0])
            
        return miExamples
    
    def classify(self, examples: DataFrame) -> DataFrame:
        '''
        Classify all examples and return the predictions
        If the learner only have one classifier just run the classifier
        If the learner as multiple classifiers, take the mean of each probability assigned by the experts (no weights)
        Override if another behavior is wanted
        :return: a copy of the examples updated with 1+2 columns: prediction and 2 columns certainty_label_0/1:
            - prediction: the best prediction made by the classifier, ie, the label with the highest certainty 
            - certainty_label_0/1: a column where the classifier assign the probability for the label 0/1 for each example
        '''
        ex = examples.copy(deep=True)
        assert len(self.getClassifiers())>1, "I was expecting more than one classifier as I am an expert committee with an ensembling methods, check the code."
        return self.ensembler.classify(ex)

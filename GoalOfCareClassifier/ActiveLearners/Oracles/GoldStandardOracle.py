'''
Created on Oct 8, 2019

@author: davy
'''
from ActiveLearners.Oracles.OracleInt import OracleInt
from pandas.core.frame import DataFrame
import logging as log
log = log.getLogger('GoldStandardOracle')

class GoldStandardOracle(OracleInt):
    '''
    This oracle knows the labels from a preloaded gold standard
    '''

    def __init__(self, goldStandardExamples: DataFrame, outputPath:str=None):
        '''
        :param goldStandardExamples: all labeled examples annotated since the pool is only composed of gold standard corpus
        :param outputPath: a path to a folder where the examples queried (with their labeled) will be copied (mainly to debug and understand the behavior of learner)
        '''
        self.goldStandard = goldStandardExamples
        self.outputPath = outputPath
    
    
    def getQueryLabels(self, examples: DataFrame, iteration:int, writeQueriedExamples=False) -> DataFrame:
        """
        :param examples: the set of unlabeled examples the learner is asking for the labels
        :param iteration: the active learning iteration
        :param writeQueriedExamples: if True and a path was provided when creating the object, 
                                     the oracle will write the examples queried
        :return: updated examples with their labels
        """
        assert len(self.goldStandard.index.intersection(examples.index)) == len(examples), "some examples queried have not been found in the gold standard set of examples in the gold standard oracle, which should not be. Check the code."
        examples.loc[examples.index, 'label'] = self.goldStandard.loc[examples.index, 'label']
        if writeQueriedExamples and self.outputPath is not None:
            examples.to_csv(f"{self.outputPath}miExamplesQueriedI{iteration}.tsv", sep="\t")
        return examples
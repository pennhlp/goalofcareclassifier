'''
Created on Oct 8, 2019

@author: davy
'''


import logging as log
# from unittest.mock import inplace
log = log.getLogger('HumanOracle')
import pandas as pd
from pandas.core.frame import DataFrame

from Properties.PropertiesInt import PropertiesInt

class HumanOracle(object):
    '''
    This oracle will have to interact with a human to collect the labels queried by the active learner
    
    TODO: I change a lot the interface of the oracle, check the GoldStandard to modify this one
    '''
    

    def __init__(self):
        '''
        Constructor
        '''
                
    def getQueryLabels(self, properties: PropertiesInt, examples: DataFrame, iteration: int) -> DataFrame:
        """
        :param: properties, should contain the path to examples annotated by the human oracle; The file search is MIExamples_{iteration}.csv 
        :param: examples, the set of unlabeled examples the learner is asking for the labels
        :param: iteration, the int recording the iteration, used to search the corresponding file 
        :return: update de examples with their labels
        """
        examplesAnnotated = pd.read_csv(f'{properties.annotatedMostInformativeExamplesPath}/MIExamples_{iteration}.csv', sep='\t')
        assert set(['docID', 'text', 'label']).issubset(examplesAnnotated.columns), f"At least one of the columns: 'docID', 'text', 'label' is not in the set of columns of the examples returned annotated: {examplesAnnotated.columns}"
        examplesAnnotated.set_index('docID', inplace=True, verify_integrity=True)

        assert len(examplesAnnotated.index.intersection(examples.index)) == len(examples), "The set examples given and the set of examples annotated are not the same as they should be, check the data."
        # check if the annotator left one example without annotation
        assert (not examplesAnnotated['label'].isnull().values.any()), f"Apparently the annotator left {examplesAnnotated['label'].isnull().sum()} examples without annotations, check the data."
        
        #add the new labeled column with the oracle annotation as label_y
        examples = pd.merge(examples, examplesAnnotated[['label']], on='docID')
        #drop the previous label column which is now label_x
        examples.drop(columns='label_x', inplace=True)
        # and rename the label_y label as it contains the solutions
        examples.rename(columns={'label_y':'label'}, inplace=True)
        return(examples)
        
    
    def sendQueryLabels(self, properties: PropertiesInt, examples: DataFrame, iteration: int):
        '''
        This should be the entry point to communicate with the human annotator. Here we keep it very simple, output a tsv file ready for annotation.
        The file will be read back after annotation by getQueryLabels  
        '''
        assert 'label' in examples.columns, "I can't find the label column in the examples to query from the annotators, check the data"
        examples.to_csv(f'{properties.annotatedMostInformativeExamplesPath}/MIExamples_{iteration}.csv', sep='\t', index=True)
        
        
        
        
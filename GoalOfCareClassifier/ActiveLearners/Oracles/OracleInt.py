'''
Created on Oct 7, 2019

@author: davy
'''
import abc
from pandas.core.frame import DataFrame

class OracleInt(object):
    '''
    The Oracle provides the labels of any set of examples requested.
    '''
    @abc.abstractmethod
    def getQueryLabels(self, examples: DataFrame) -> DataFrame:
        """
        :param: examples, the set of unlabeled examples the learner is asking for the labels
        :return: updated examples with their labels
        """
        pass
'''
Created on Nov 11, 2019

@author: dweissen
'''

import logging as lg
log = lg.getLogger('RandomSampler')
import configparser
from typing import List
from pandas.core.frame import DataFrame
import numpy as np

from Data.Pool import Pool
from Evaluation.EvaluaterInt import EvaluaterInt
from ActiveLearners.ActiveLearner import ActiveLearner
from ActiveLearners.Classifiers.ClassifierInt import ClassifierInt
from ActiveLearners.Ensemblers.EnsemblerInt import EnsemblerInt


class RandomSampler(ActiveLearner):
    '''
    A baseline active learner which select the most informative examples randomly...
    '''

    def __init__(self, config: configparser, ensembler:EnsemblerInt, classifiers: List[ClassifierInt], evaluater:EvaluaterInt = None, trainingArgs:dict = None):
        super(ActiveLearner, self).__init__(config, ensembler, classifiers, evaluater, trainingArgs)
        
    
    def getMostInformativeExamples(self, pool: Pool, ExQueriedBatchSize: int) -> DataFrame:
        '''
        :param: pool with all unlabeled data
        :param: ExQueriedBatchSize the number of examples returned to be annotated
        :return: a set of examples randomly selected
        '''
        miExamples = pool.unlabEx.sample(frac=1.0, replace=False)
        miExamples = DataFrame(miExamples[0:ExQueriedBatchSize])
        miExamples['label'] = np.NaN
        
        return miExamples
    
    def classify(self, examples: DataFrame) -> DataFrame:
        '''
        Classify all examples and return the predictions
        If the learner only have one classifier just run the classifier
        If the learner as multiple classifiers, take the mean of each probability assigned by the experts (no weights)
        Override if another behavior is wanted
        :return: a copy of the examples updated with 1+2 columns: prediction and 2 columns certainty_label_0/1:
            - prediction: the best prediction made by the classifier, ie, the label with the highest certainty 
            - certainty_label_0/1: a column where the classifier assign the probability for the label 0/1 for each example
        '''
        ex = examples.copy(deep=True)
        assert len(self.getClassifiers())==1, "I was expecting only one classifier as I am a RandomSampler with no ensembling method, check the code."
        return self.getClassifiers()[0].classify(ex)
            
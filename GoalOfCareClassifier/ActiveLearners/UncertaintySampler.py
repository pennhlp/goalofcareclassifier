'''
Created on Nov 7, 2019

@author: davy
'''

import logging as lg
log = lg.getLogger('UncertaintySampler')

import configparser
from typing import List
import numpy as np
import pandas as pd
from pandas.core.frame import DataFrame
from enum import Enum
import scipy.stats as spy

from Data.Pool import Pool
from ActiveLearners.ActiveLearner import ActiveLearner
from Evaluation.EvaluaterInt import EvaluaterInt
from ActiveLearners.Classifiers.ClassifierInt import ClassifierInt
from ActiveLearners.Ensemblers.EnsemblerInt import EnsemblerInt


class UncertaintyMeasure(Enum):
    '''
    A simple Enum class to enumerate the measure of uncertainty to use to return the examples
    '''
    LeastConfident = 1
    Margin = 2
    Entropy = 3
    
class USQueryingAlgorithm(object):
    def __init__(self, measure: UncertaintyMeasure):
        self.uncertaintyMeasure = measure 

    
    def getUncertaintyMeasure(self) -> UncertaintyMeasure:
        return self.uncertaintyMeasure

    
    def getExamplesToQuery(self, examples: DataFrame):
        log.info(f"I compute miExamples with {self.getUncertaintyMeasure()}")
        if self.getUncertaintyMeasure()==UncertaintyMeasure.LeastConfident:
            return self.__getUncertaintyRankedExamples(examples)
        elif self.getUncertaintyMeasure()==UncertaintyMeasure.Margin:
            return self.__getMarginRankedExamples(examples)
        elif self.getUncertaintyMeasure()==UncertaintyMeasure.Entropy:
            return self.__getEntropyRankedExamples(examples)

        
    def __getUncertaintyRankedExamples(self, predictions: DataFrame):
        '''
        :return: the n examples ranked by the max of uncertainty
        Assume 3 examples classified in 3 different classes, each probability assigned to one class: 
        [[0.1 , 0.85, 0.05],
         [0.6 , 0.3 , 0.1 ],
         [0.39, 0.61, 0.0 ]]
         U(x) = 1-P(x^|x), x^ being the most likely class, we compute:
         1-0.85 = 0.15
         1-0.6 = 0.4
         1-0.61 = 0.39
         And the ex.2 should be returned as it is the less certain 
        '''
        raise Exception("Uncertainty does not seems to be computable for binary classification, just the margin.")
           
        
    def __getMarginRankedExamples(self, predictions: DataFrame):
        '''
        :return: the n examples ranked by the max margin between classes
        Assume 3 examples classified in 3 different classes, each probability assigned to one class: 
        [[0.1 , 0.85, 0.05],
         [0.6 , 0.3 , 0.1 ],
         [0.39, 0.61, 0.0 ]]
         U(x) = P(x1|x)-P(x2|x) where x1 is the most likely examples and x2 the second most likely example
         0.85-0.1 = 0.75
         0.6-03 = 0.3
         0.61-0.39 = 0.22
         and the example 3 should be return since it exhibits the smallest margin that is the ma uncertainty
        '''
        if len(set(predictions.filter(regex=("certainty_label_[0-9]+")).columns))==2:
            #binary classification
            predictions['margin'] = (predictions['certainty_label_0'] - predictions['certainty_label_1']).abs()
        elif len(set(predictions.filter(regex=("certainty_label_[0-9]+")).columns))>2:
            #multiclass
            raise Exception("Not implemented for multi-class")
        else:
            #should not enter here but in case
            raise Exception("I did not find the expected number of classes, found 0 or 1 class, check the code and the dataframe input.")        
        # we want to minimize the difference since a prediction of 0.50 is the most uncertain one
        predictions.sort_values(by='margin', ascending=True, inplace=True)
        predictions.drop(columns='margin', inplace=True)
         
        return predictions
    
    
    def __getEntropyRankedExamples(self, predictions: DataFrame):
        '''
        :return: the n examples ranked by the entropy between classes
        Assume 3 examples classified in 3 different classes, each probability assigned to one class: 
        [[0.1 , 0.85, 0.05],
         [0.6 , 0.3 , 0.1 ],
         [0.39, 0.61, 0.0 ]]
         U(x) = -SigmaK pk * log(pk) where pk is the probability assign to the example to belong to the class k
         entropy(0.1 , 0.85, 0.05) = 0.5181862130502128
         entropy(0.6 , 0.3 , 0.1) = 0.89794572
         entropy(0.39, 0.61, 0.0) = 0.66874809
         and the example 2 should be returned since it has the highest entropy, i.e. the distribution is closer to the uniform distribution
        '''
        if len(set(predictions.filter(regex=("certainty_label_[0-9]+")).columns))==2:
            #binary classification
            predictions['entropy'] = spy.entropy([predictions['certainty_label_0'], 1-predictions['certainty_label_0']])
        elif len(set(predictions.filter(regex=("certainty_label_[0-9]+")).columns))>2:
            #multiclass classification
            predictions['entropy'] = predictions.filter(regex=("certainty_label_[0-9]+")).apply(spy.entropy, axis=1)
        else:
            #should not enter here but in case
            raise Exception("I did not find the expected number of classes, found 0 or 1 class, check the code and the dataframe input.")
        predictions.sort_values(by='entropy', ascending=False, inplace=True)
        predictions.drop(columns='entropy', inplace=True)
        
        return predictions
                

class UncertaintySampler(ActiveLearner):
    '''
    This class implement the basic approach for active learning, the uncertainty sampling.
    I am following the implementation of modal: https://modal-python.readthedocs.io/en/latest/content/query_strategies/uncertainty_sampling.html 
    '''
    def __init__(self, config: configparser, ensembler:EnsemblerInt, classifiers: List[ClassifierInt], evaluater:EvaluaterInt = None, trainingArgs:dict = None):
        super(ActiveLearner, self).__init__(config, ensembler, classifiers, evaluater, trainingArgs)
        self._queryingAlgo: Optional[QueryingAlgorithm] = None
        
        assert len(classifiers)==1, f"The uncertainty sampler expect only one classifier, {len(classifiers)} were given, check the code."


    def setQueryingAlgorithm(self, queryingAlgorithm: USQueryingAlgorithm):
        '''
        :param: queryingAlgorithm the querying algorithm to be used to select the most informative examples
        '''
        assert isinstance(queryingAlgorithm , USQueryingAlgorithm), f"I was expecting an instance of USQueryingAlgorithm, I got {queryingAlgorithm.__class__.__name__}, check the code." 
        self._queryingAlgo = queryingAlgorithm


    def getQueryingAlgorithm(self) -> USQueryingAlgorithm:
        '''
        :return: the querying algorithm set for the uncertainty sampler, 
        :raise exception: if the querying algorithm has not been set yet
        '''
        assert self._queryingAlgo is not None, "The code is calling for a querying algorithm which has not been instantiated in the uncertainty sampler."
        return self._queryingAlgo

    
    def getMostInformativeExamples(self, pool: Pool, ExQueriedBatchSize: int) -> DataFrame:
        '''
        :param: pool with all unlabeled data
        :param: ExQueriedBatchSize the number of most informative examples returned to be annotated
        :return: a set of examples that should be the most informative to annotate
        '''
        cpUnlabelExs = pool.unlabEx.copy()
        predictions = self.getClassifiers()[0].classify(cpUnlabelExs)
        # TODO: Sid: Changed below line to look for 'prediction' instead of 'label' to match the column name created by the classifiers. Make sure this is ok.
        #assert set(['certainty_label_0', 'certainty_label_1', 'label']).issubset(predictions.columns), f"The classifier {self.getClassifiers()[0].getName()} did not return a dtatframe with the expected columns: 'label', 'certainty_label_0', 'certainty_label_1', check the code"
        assert set(['certainty_label_0', 'certainty_label_1', 'prediction']).issubset(predictions.columns), f"The classifier {self.getClassifiers()[0].getName()} did not return a dtatframe with the expected columns: 'prediction', 'certainty_label_0', 'certainty_label_1', check the code"
        log.info(f"I run the querying algorithm {self.getQueryingAlgorithm().getUncertaintyMeasure()}")
        miExamples = self.getQueryingAlgorithm().getExamplesToQuery(predictions)
        if self.trainingArgs["biasTowardMinorityClass"] is not None:
            miExamples = self.__biasTowardMinorityClass(miExamples, ExQueriedBatchSize)
        else:
            miExamples = DataFrame(miExamples[0:ExQueriedBatchSize])
 
        #delete the useless columns
        #miExamples.drop(columns=list(miExamples.filter(regex=("certainty_label_[0-9]+")).columns), inplace=True)
        # and add the column empty for annotation
        miExamples['label'] = np.NaN
        return miExamples
    
    
    def __biasTowardMinorityClass(self, miExamples:DataFrame, ExQueriedBatchSize: int)->DataFrame:
        log.info("I am biasing the query of the most informative examples toward the minority class")
        predictions1 = miExamples[miExamples["prediction"]==1]
        log.info(f"The classifier(s) have predicted {len(predictions1)} examples as belonging to the minority class, we need {ExQueriedBatchSize}, will complete with the most uncerstain examples of the majority class if needed.")
        if len(predictions1)>ExQueriedBatchSize:
            miExamples = DataFrame(predictions1[0:ExQueriedBatchSize])
        else:
            df1 = DataFrame(predictions1)
            df0 = DataFrame(miExamples[miExamples["prediction"]==0][0:ExQueriedBatchSize-len(df1)])
            miExamples = pd.concat([df1,df0])
            
        return miExamples
    
    def classify(self, examples: DataFrame) -> DataFrame:
        '''
        Classify all examples and return the predictions
        If the learner only have one classifier just run the classifier
        If the learner as multiple classifiers, take the mean of each probability assigned by the experts (no weights)
        Override if another behavior is wanted
        :return: a copy of the examples updated with 1+2 columns: prediction and 2 columns certainty_label_0/1:
            - prediction: the best prediction made by the classifier, ie, the label with the highest certainty 
            - certainty_label_0/1: a column where the classifier assign the probability for the label 0/1 for each example
        '''
        ex = examples.copy(deep=True)
        assert len(self.getClassifiers())==1, "I was expecting only one classifier as I am a Uncertainty Sampler with no ensembling method, check the code."
        return self.getClassifiers()[0].classify(ex)


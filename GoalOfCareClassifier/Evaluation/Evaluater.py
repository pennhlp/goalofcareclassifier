'''
Created on Jan 19, 2022

@author: dweissen
'''
import logging as lg
log = lg.getLogger('Evaluater')
lg.getLogger('matplotlib').setLevel(lg.WARNING)
import numpy as np
from pandas.core.frame import DataFrame
from sklearn.metrics import precision_score, recall_score, f1_score, confusion_matrix, classification_report, accuracy_score

import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

from Evaluation.EvaluaterInt import EvaluaterInt

class Scores(object):
    '''
    The scores obtained during a single evaluation
    '''
    def __init__(self, confusionMatrix: list, classificationReport: str, F1positive: float, PrecisionPositive: float, RecallPositive: float, Accuracy: float):
        self.scores = {"confusionMatrix": confusionMatrix, "classificationReport": classificationReport, "F1 positive": F1positive, "Precision Positive": PrecisionPositive, "Recall Positive": RecallPositive, "Accuracy": Accuracy}
    def getDetails(self):
        return self.scores
    
    def __str__(self):
        out = ""
        for k,v in self.scores.items():
            out = out+f"{k}:\n{v}\n"
        return out


class Performance(object):
    '''
    A class representing the performance of a classifier during multiple evaluation
    This is a dictionary iteration -> scores
    '''
    def __init__(self, activeLearnerName: str):
        '''
        Constructor
        '''
        self.activeLearnerName = activeLearnerName
        # a dictionary iteration -> scores
        self.perfHistory = {}

    
    def addPerformance(self, iteration:int, scores: Scores):
        assert not iteration in self.perfHistory.keys(), f"A performance for the iteration {iteration} has already been inserted, check the code."
        self.perfHistory[iteration] = scores
    
        
    def getPerformance(self):
        return self.perfHistory

    
    def __str__(self):
        out = f"ActiveLearner: {self.activeLearnerName}\n"
        for ind, score in self.perfHistory.items():
            out =  out + f"Iteration: {ind} => {score}"
        return out
    
    def drawPerformance(self, path:str):
        '''
        Draw the performance of a classifier 
        '''
        df = DataFrame()
        iterationsX = sorted(self.getPerformance().keys())
        assert len(iterationsX)>0, "I am asked to draw an history which is empty, check the code."
        
        df['x'] = iterationsX 
        f1s = []
        for iterationx in iterationsX:
            f1s.append(self.getPerformance()[iterationx].getDetails()['F1 positive'])
        df[self.activeLearnerName] = f1s
                    
        pp = PdfPages(path)
        
        fig = plt.figure()
        plt.plot('x', self.activeLearnerName, data=df, marker='', color='blue', linewidth=2)
        for row in df.itertuples():
            assert row[2]<=1.0, "Probably an error when retrieving the f1-score from the df for drawing the curve, check the code..."
            if len(str(row[2]))>=5:
                plt.text(row[1],row[2], str(row[2])[2:5], fontsize=4)
            else:
                plt.text(row[1],row[2], str(row[2])[2:], fontsize=4)
        plt.legend()
        pp.savefig(fig)        
        pp.close()
        plt.close(fig)
        
#     def drawHistory(self, history: History, path: str):
#         '''
#         Draw the performance of a classifier on the evaluation set from the history computed by Keras
#         '''        
#         # Plot training & validation loss values
#         plt.plot(history.history['loss'])
#         plt.plot(history.history['val_loss'])
#         plt.title('Model loss')
#         plt.ylabel('Metrics')
#         plt.xlabel('Epoch')
#         plt.legend(['Train', 'Validation'], loc='upper right')
#         plt.draw()
#         plt.savefig(path)
#         plt.close()

class Evaluater(EvaluaterInt):
    '''
    The default evaluater which evaluates on validation and test sets at each iteration, both can be displayed in graph
    '''
    
    def __init__(self, ActiveLearnerName: str):
        super(Evaluater, self).__init__(ActiveLearnerName)
        self.valPerf = Performance(self.activeLearnerName)
        self.testPerf = Performance(self.activeLearnerName)
    
    def evaluate(self, examplesPredicted: DataFrame, examplesTruth: DataFrame) -> Scores:
        '''
        evaluate the performance of the learner on the set of examples given
        :param: examplesPredicted, the predictions made by the classifier, the df should have a column 'prediction' where the predictions are store
        :param: examplesTruth, the true values of the examples also store in a column label, both set of examples should have the same docIDs
        :return: P,R,F1 scores
        '''
        assert len(examplesPredicted.index.intersection(examplesTruth.index))==len(examplesPredicted.index), f"There is no index mapping between the predicted and the truth set of examples, check the code."
        assert 'prediction' in examplesPredicted.columns and 'label' in examplesTruth.columns, "The examples predicted or gold truth do not have the expected label columns, check code."
         
        df = DataFrame({'docIDTruth':examplesTruth.index, 'truths':examplesTruth['label'], 'docIDPredicted':examplesPredicted.index, 'preds':examplesPredicted['prediction']})
        assert(np.array_equal(df['docIDTruth'], df['docIDPredicted'])), "I have differences between the lists of docIDs from the truth and the predicted which should be identical lists, check the code..."        
 
        cf = confusion_matrix(list(df['truths']), list(df['preds']))        
        cr = classification_report(list(df['truths']), list(df['preds']),digits=4)
        acc=accuracy_score(list(df['truths']), list(df['preds']))
        if df['truths'].nunique()>2 or df['preds'].nunique()>2: #NERVER TESTED...
            log.warn("Running an evaluation on multinomial classification never testes, TODO!!!!!!")
            #We are in a multinomial classification
            prec = precision_score(list(df['truths']), list(df['preds']), average='macro')
            rec = recall_score(list(df['truths']), list(df['preds']), average='macro')
            f1 = f1_score(list(df['truths']), list(df['preds']), average='macro')
        else:
            #binary
            prec = precision_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
            rec = recall_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
            f1 = f1_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
         
        return Scores(confusionMatrix=cf, classificationReport=cr, F1positive=f1, PrecisionPositive=prec, RecallPositive=rec, Accuracy=acc)
        
'''
Created on Jan 19, 2022

@author: dweissen
'''

from pandas.core.frame import DataFrame


class EvaluaterInt(object):
    def __init__(self, activeLearnerName):
        self.activeLearnerName = activeLearnerName

    def evaluate(self, examplesPredicted: DataFrame, examplesTruth: DataFrame):
        '''
        evaluate the performance of the active learner on the set of examples given
        :param: examplesPredicted, the predictions made by the classifier, the df should have a column 'prediction' where the predictions are store
        :param: examplesTruth, the true values of the examples also store in a column label, both set of examples should have the same docIDs
        :return: a Scores object containing the metrics
        '''
        
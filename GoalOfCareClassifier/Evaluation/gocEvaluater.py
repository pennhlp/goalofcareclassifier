'''
Created on Jan 27, 2022

@author: dweissen
'''
from pandas.core.frame import DataFrame
from sklearn.metrics import precision_score, recall_score, f1_score, confusion_matrix, classification_report, accuracy_score

from Evaluation.Evaluater import Evaluater
from Evaluation.Evaluater import Scores

class gocEvaluater(Evaluater):
    '''
    An evaluater for the Goal of Care Project, it runs the evaluation at the sentence and the document level.
    At the sentence level just compute the F1-score of the positive class, utterances speaking about GOC
    At the document level computes the F1-score of the classifier when detecting documents having at least one utterance speaking about GOC
    NOTE: this is only pertinent for the test set which is composed of full clinical notes, evaluation set is only composed of losse utterances
    '''

    def __init__(self, ActiveLearnerName: str):
        super(gocEvaluater, self).__init__(ActiveLearnerName)
    
    def evaluate(self, examplesPredicted: DataFrame, examplesTruth: DataFrame) -> Scores:
        scores = super().evaluate(examplesPredicted, examplesTruth)
        self.__getPerformanceOnDocLevel(examplesPredicted, examplesTruth, scores)
        return scores
    
    
    def __getPerformanceOnDocLevel(self, examplesPredicted: DataFrame, examplesTruth: DataFrame, scores: Scores):
        """
        :param examplesPredicted: the predictions for all uterrances of the documents
        :param examplesTruth: the true label for uterrances of the documents
        :param scores: the Scores object which contains any metrics computed by Evaluater
        :return: update Scores.scores with the F1, R, P for the document classification
        """
        noteIDstruth = set(examplesTruth["note_id"].unique())
        noteIDspredicted = set(examplesPredicted["note_id"].unique())
        assert noteIDstruth==noteIDspredicted, f"I was expecting the same set of note IDs in the predictions and the truth DataFrame, but there are differences: {noteIDstruth.symmetric_difference(noteIDspredicted)} -> Truth:[{noteIDstruth}] and Pedicted:[{noteIDspredicted}]. Check the code and the input."
        results = []
        for noteID in noteIDstruth:
            result = {}
            results.append(result)
            result["note_id"] = noteID
            if len(examplesPredicted[(examplesPredicted["note_id"]==noteID) & (examplesPredicted["prediction"]==1)])>0:
                result["prediction"]=1
            else:
                result["prediction"]=0
            if len(examplesTruth[(examplesTruth["note_id"]==noteID) & (examplesTruth["label"]==1)])>0:
                result["label"]=1
            else:
                result["label"]=0
            
        df = DataFrame(results)
        cf = confusion_matrix(list(df['label']), list(df['prediction']))        
        cr = classification_report(list(df['label']), list(df['prediction']),digits=4)
        acc=accuracy_score(list(df['label']), list(df['prediction']))
        #binary
        prec = precision_score(list(df['label']), list(df['prediction']), pos_label=1, average='binary')
        rec = recall_score(list(df['label']), list(df['prediction']), pos_label=1, average='binary')
        f1 = f1_score(list(df['label']), list(df['prediction']), pos_label=1, average='binary')   
              
        #add the computation at the doc level
        scores.scores["docConfusionMatrix"]=cf
        scores.scores["docClassificationReport"]=cr
        scores.scores["docAcc"]=acc
        scores.scores["docF1"]=f1
        scores.scores["docPrecision"]=prec
        scores.scores["docRecall"]=rec
        return scores
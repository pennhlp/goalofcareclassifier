'''
Created on Nov 17, 2021

@author: dweissen
'''
from pandas.core.frame import DataFrame
from pandas.core.series import Series
import pandas as pd
import numpy as np
import logging as log
import random
from os import path
import configparser

from Handlers.HandlerInt import HandlerInt



class EHRNotesHandler(HandlerInt):
    '''
    Access to the pre-formated clinical notes annotated
    '''

    def __init__(self, config: configparser, pathAPC:str, pathRandom:str):
        '''
        Read the tsv of the APC notes and Random notes that have been annotated
        Create test with 176 random notes (full notes), randomly selected (same seed to compare systems), 
        200 notes for train and validation, 
        with 0.80 utterance for training, 0.20 for validation, utterances are randomly selected
        APC notes are available as additional data for experiments
        '''
        self.config = config
        self.test = None
        self.train = None
        self.val = None
        self.unlab = None
        self.apc = None
        if path.exists(self.config['Data']['tmpFolder']+"randomTest.tsv") and path.exists(self.config['Data']['tmpFolder']+"apc.tsv"):
            log.info("I found the EHR notes already preprocessed in the tmp folder, I just read them...")
            self.apc = pd.read_csv(self.config['Data']['tmpFolder']+"apc.tsv", sep="\t", index_col="docID", dtype={"note_id":str, "sentence_number":np.int32, "text":str, "label":np.int32})
            self.test = pd.read_csv(self.config['Data']['tmpFolder']+"randomTest.tsv", sep="\t", index_col="docID", dtype={"note_id":str, "sentence_number":np.int32, "text":str, "label":np.int32})
            self.train = pd.read_csv(self.config['Data']['tmpFolder']+"randomTrain.tsv", sep="\t", index_col="docID", dtype={"note_id":str, "sentence_number":np.int32, "text":str, "label":np.int32})
            self.val = pd.read_csv(self.config['Data']['tmpFolder']+"randomVal.tsv", sep="\t", index_col="docID", dtype={"note_id":str, "sentence_number":np.int32, "text":str, "label":np.int32})
        else:
            self.apc = self.__getAPCExamples(pathAPC)
            # I re-index the random notes otherwise we have same IDs between sentences in the APC and Random notes
            self.RandomNotes = self.__getRandomNotes(pathRandom, startIndex = (self.apc.index.max()+1))
            self.__createCorpus()
            self.apc.to_csv(self.config['Data']['tmpFolder']+"apc.tsv", sep="\t")
            self.test.to_csv(self.config['Data']['tmpFolder']+"randomTest.tsv", sep="\t")
            self.train.to_csv(self.config['Data']['tmpFolder']+"randomTrain.tsv", sep="\t")
            self.val.to_csv(self.config['Data']['tmpFolder']+"randomVal.tsv", sep="\t")
    
    
    def __createCorpus(self)-> list:
        # first create the test set
        notesIDs = set(self.RandomNotes['note_id'].tolist())
        random.seed(a=22)
        testIDs = random.sample(notesIDs, 176)
        self.test = self.RandomNotes.copy(deep=True)
        self.test = self.test[self.test['note_id'].isin(testIDs)]
        self.train = self.RandomNotes.copy(deep=True)
        self.train = self.train.drop(self.test.index)
        self.val = self.train.sample(frac=0.2, replace=False, random_state=42)
        self.train = self.train.drop(self.val.index)
        assert (len(self.test)+len(self.train)+len(self.val))==len(self.RandomNotes), "Something went wrong when creating the training sets, I don't have the same number of lines, check the code."

        
    
    def __getAPCExamples(self, pathTSV)->DataFrame:
        """
        Set of examples used for transfer learning in order to boost the performance of the system
        :param pathTSV: the path to a tsv file with the data
        :return: a df with the columns note: note ID, sent_num: utterance ID, sentence (note utterance), label (annotation, 1: GOC, 0: otherwise)
        """
        typesDict = {"note":"str", "sent_num":np.int64, "sentence":"str"}
        df = pd.read_csv(pathTSV, encoding = "utf-8", dtype=typesDict)
        assert len(df) == 16267, "Did not find the number of line expected in the annotated APC notes, check the input..."
        df.drop(['Unnamed: 0'], axis=1, inplace=True)
        #df = self.__detectDuplicatesAPC(df)
        df = self.__cleanAPCNotes(df)
        df = self.__getAPCGOCLabel(df)
        df.rename(columns={"sentence":"text", "note":"note_id", "sent_num":"sentence_number"}, inplace=True)
        df.index.set_names("docID", inplace=True)
        return df
    
    
    def __detectDuplicatesAPC(self, notes: DataFrame) -> DataFrame:
        """
        When doctors update their notes, they just copy past the previous one and add the update section. Results: part of notes are duplicated with same note IDs different sentence IDs
        This function detect them (do not remove them)
        NOT WORKING PERFECTLY...
        """
        def checkDuplicate(df:DataFrame) -> bool:
            lenbefore = len(df)
            dfc = df.copy(deep=True)
            dfc.drop_duplicates(subset=['sentence'], inplace=True)
            lenafter = len(dfc)
            if lenafter < (lenbefore-10):# I leave the case where a unique note has few duplicated  sentences
                return True
            else:
                return False
        currentNote = None
        df = DataFrame()
        for index, row in notes.iterrows():
            if currentNote is None:
                currentNote = row["note"]
            if currentNote==row["note"]:
                df = df.append(row, ignore_index = True)
            else:
                #We have a note check if it is duplicated
                if checkDuplicate(df):
                    log.warn(f"We have a duplicated note: {currentNote}")
                currentNote = row["note"]
                df = DataFrame()
                df = df.append(row, ignore_index = True)
        #last note...
        if checkDuplicate(df):
            log.warn(f"We have a duplicated note: {currentNote}")
            
            
    def __cleanAPCNotes(self, notes: DataFrame) -> DataFrame:
        """
        Apply several cleaning process to the text
        """
        #remove the empty sentence
        notes['sentence'].replace('', np.nan, inplace=True)
        notes.dropna(subset=["sentence"], inplace=True)
#         for index, row in notes.iterrows():
#             if index==89:
#                 print("stop here")
#             print(f"{index} - {row['sentence']}")
        #notes.replace(to_replace="��", value = " - ", inplace = True)
        #notes.to_csv("/Volumes/HIPAA_Data/tmp/avirer.tsv", sep="\t")
        return notes
    
    
    def __getAPCGOCLabel(self, notes: DataFrame) -> DataFrame:
        def getLabel(row:Series):
            if row["other_GOC_kc"]==1 or row["prog_GOC_kc"]==1 or row["goals_GOC_kc"]==1:
                #We have a disagreement and Kate adjudicated for a 1
                return 1
            elif row["other_GOC_kc"]==0 or row["prog_GOC_kc"]==0 or row["goals_GOC_kc"]==0:
                #We have a disagreement and Kate adjudicated for a 0
                return 0
            elif ((row["tag_GoalsofcareACP_other_nick"]==1 and row["tag_GoalsofcareACP_other_corinne"]==1) or 
                 (row["tag_GoalsofcareACP_prognosis_nick"]==1 and row["tag_GoalsofcareACP_prognosis_corinne"]==1) or 
                 (row["tag_GoalsofcareACP_goals_preferences_plan_nick"]==1 and row["tag_GoalsofcareACP_goals_preferences_plan_corinne"]==1)):
                return 1
            elif ((row["tag_GoalsofcareACP_other_lindsay"]==1 and row["tag_GoalsofcareACP_other_joseph"]==1) or
                 (row["tag_GoalsofcareACP_prognosis_lindsay"]==1 and row["tag_GoalsofcareACP_prognosis_joseph"]==1) or
                 (row["tag_GoalsofcareACP_goals_preferences_plan_lindsay"]==1 and row["tag_GoalsofcareACP_goals_preferences_plan_joseph"]==1)):
                return 1
            elif (row["tag_GoalsofcareACP_other_nick"]==0 and row["tag_GoalsofcareACP_other_corinne"]==0 and 
                 row["tag_GoalsofcareACP_prognosis_nick"]==0 and row["tag_GoalsofcareACP_prognosis_corinne"]==0 and 
                 row["tag_GoalsofcareACP_goals_preferences_plan_nick"]==0 and row["tag_GoalsofcareACP_goals_preferences_plan_corinne"]==0):
                return 0
            elif (row["tag_GoalsofcareACP_other_lindsay"]==0 and row["tag_GoalsofcareACP_other_joseph"]==0 and
                 row["tag_GoalsofcareACP_prognosis_lindsay"]==0 and row["tag_GoalsofcareACP_prognosis_joseph"]==0 and
                 row["tag_GoalsofcareACP_goals_preferences_plan_lindsay"]==0 and row["tag_GoalsofcareACP_goals_preferences_plan_joseph"]==0):
                return 0
            else:
                #raise Exception(f"I have an unexpected annotation set in row: {row}")
                log.error(f"I have an unexpected annotation set in row: {row}")
                return 0
        notes["label"] = notes.apply(lambda row: getLabel(row), axis = 1)
        notes.drop(columns=["tag_GoalsofcareACP_other_nick", "tag_GoalsofcareACP_prognosis_nick", "tag_GoalsofcareACP_goals_preferences_plan_nick", "tag_GoalsofcareACP_other_corinne", "tag_GoalsofcareACP_prognosis_corinne", "tag_GoalsofcareACP_goals_preferences_plan_corinne", "tag_GoalsofcareACP_other_lindsay", "tag_GoalsofcareACP_prognosis_lindsay", "tag_GoalsofcareACP_goals_preferences_plan_lindsay", "tag_GoalsofcareACP_other_joseph", "tag_GoalsofcareACP_prognosis_joseph", "tag_GoalsofcareACP_goals_preferences_plan_joseph", "adjud_other_GOC", "other_GOC_kc", "adjuc_prog_GOC", "prog_GOC_kc", "adjud_goals_GOC", "goals_GOC_kc"], inplace=True)
        return notes
    
    
    def __getRandomNotes(self, pathTSV, startIndex=0)->DataFrame:
        """
        Read the annotated random notes
        :param pathTSV: path to the tsv
        :param startIndex: a integer to start the index from  
        Index with increments following the last row of the APC notes
        """
        typesDict = {"note_id":"str", "sentence_number":np.int32, "sentence":"str"}
        df = pd.read_csv(pathTSV, encoding = "utf-8", dtype=typesDict)
        df.drop(['Unnamed: 0'], axis=1, inplace=True)
        df = self.__cleanRandomNotes(df)
        df = self.__getRandomGOCLabel(df)
        df.rename(columns={"sentence":"text"}, inplace=True)
        df.reset_index(drop=True, inplace=True)
        df.index = df.index + startIndex
        df.index.set_names("docID", inplace=True)
        #df.to_csv(self.config['Data']['tmpFolder']+"random.tsv", sep="\t")
        return df
        
    
    def __cleanRandomNotes(self, notes:DataFrame) -> DataFrame:
        """
        Clean the notes
        """
        def removeQuote(row: Series) -> str:
            if row['sentence'] is not None and not pd.isna(row['sentence']) and row['sentence']!='' and row['sentence'][0]=='`':
                return row['sentence'][1:]
            else:
                return row['sentence']
        notes['sentence'] = notes.apply(lambda row: removeQuote(row), axis = 1)
        #remove the empty lines
        notes['sentence'].replace('', np.nan, inplace=True)
        notes.dropna(subset=["sentence"], inplace=True)
        return notes
        
        
    def __getRandomGOCLabel(self, notes: DataFrame) -> DataFrame:
        def getLabel(row:Series):
            if (row['curation_GoalsofcareACP_goals_preferences_plan']==1 or
                row['curation_GoalsofcareACP_prognosis']==1 or
                row['curation_GoalsofcareACP_other']==1):
                return 1
            elif (row['curation_GoalsofcareACP_goals_preferences_plan']==0 and
                row['curation_GoalsofcareACP_prognosis']==0 and
                row['curation_GoalsofcareACP_other']==0):
                return 0
            elif ((row['GoalsofcareACP_goals_preferences_plan_corinne']==1 and row['GoalsofcareACP_goals_preferences_plan_nick']==1) or
                  (row['GoalsofcareACP_prognosis_corinne']==1 and row['GoalsofcareACP_prognosis_nick']==1) or
                  (row['GoalsofcareACP_other_corinne']==1 and row['GoalsofcareACP_other_nick']==1)):
                return 1
            elif (row['GoalsofcareACP_goals_preferences_plan_corinne']==0 and row['GoalsofcareACP_goals_preferences_plan_nick']==0 and
                  row['GoalsofcareACP_prognosis_corinne']==0 and row['GoalsofcareACP_prognosis_nick']==0 and
                  row['GoalsofcareACP_other_corinne']==0 and row['GoalsofcareACP_other_nick']==0):
                return 0
            else:
                #raise Exception(f"I have an unexpected annotation set in row: {row}")
                log.error(f"I have an unexpected annotation set in row: {row}")
                return 0
                
        notes["label"] = notes.apply(lambda row: getLabel(row), axis = 1)
        #No need of the extra columns
        notes.drop(columns=["curation_GoalsofcareACP_goals_preferences_plan", "curation_GoalsofcareACP_prognosis", "curation_GoalsofcareACP_other", "disagreement", "random_check", "GoalsofcareACP_goals_preferences_plan_corinne", "GoalsofcareACP_goals_preferences_plan_nick", "GoalsofcareACP_prognosis_corinne", "GoalsofcareACP_prognosis_nick", "GoalsofcareACP_other_corinne", "GoalsofcareACP_other_nick", "GoalsofcareACP_goals_preferences_plan_qualifying_span_corinne", "GoalsofcareACP_goals_preferences_plan_qualifying_span_nick", "GoalsofcareACP_prognosis_qualifying_span_corinne", "GoalsofcareACP_prognosis_qualifying_span_nick","GoalsofcareACP_other_qualifying_span_corinne", "GoalsofcareACP_other_qualifying_span_nick"], inplace=True)
        return notes
    
    
    def getAPCExamples(self)->DataFrame:
        return self.apc
    def getTrainExamples(self)->DataFrame:
        return self.train
    def getValidationExamples(self)->DataFrame:
        return self.val
    def getTestExamples(self) -> DataFrame:
        return self.test
    def getUnlabeledExamples(self, pathTSV)->DataFrame:
        return self.unlab
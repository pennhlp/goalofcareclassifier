'''
Created on Nov 16, 2021

The main function to call, all experiments are defined in independant functions for reproducability

@author: dweissen
'''
import os
import sys
import configparser
import logging as log
import pandas as pd


pd.set_option("display.max_columns", 100)
from pandas.core.frame import DataFrame

from ActiveLearners.Classifiers import ClassifierInt
from ActiveLearners.Oracles.GoldStandardOracle import GoldStandardOracle
from Handlers.EHRNotesHandler import EHRNotesHandler
from Handlers.HandlerInt import HandlerInt
from Data.Pool import Pool
from Evaluation.EvaluaterInt import EvaluaterInt
from Evaluation.gocEvaluater import gocEvaluater
from Evaluation.Evaluater import Evaluater



class GOC_Pipe(object):
    
    def __init__(self, config: configparser):
        return
        

    def runExperiment1(self, config: configparser, hdl: HandlerInt):
        """
        Experiment settings: RegEx classifier using RegEx's created from training notes
        RegEx have been developed on APC notes, the validation set is the Random notes set, evaluation on the Random notes test set
        """
        from ActiveLearners.Classifiers.GOCRegExFinder import GOCRegExFinder

        gocEval: EvaluaterInt = gocEvaluater("RegEx Finder")
        emptyDF = DataFrame(columns=["text", "docID"])
        emptyDF.set_index("docID", drop=True, inplace=True, verify_integrity=False)
        pool = Pool(unlabeledExamples=emptyDF, trainingExamples=hdl.getAPCExamples(),
                    validationExamples=hdl.getValidationExamples(), testExamples=hdl.getTestExamples())
        log.info(f"Pool of examples created: {pool}")
        classifier: ClassifierInt = GOCRegExFinder('RegEx', config, verbose=False)

        # For RegExFinder train will get results on train set
        #classifier.train(pool.trainEx)

        log.info("Evaluation on the evaluation set...")
        predictions = classifier.classify(pool.valEx)
        log.info(gocEval.evaluate(predictions, pool.valEx))
        
        log.info("Evaluation on the test set...")
        predictions = classifier.classify(pool.testEx)
        log.info(gocEval.evaluate(predictions, pool.testEx))

    def runExperiment2(self, config: configparser, hdl:HandlerInt):
        """
        Experiment settings: Logistic Regression with TF-IDF
        Trained on APC notes, the validation set is the Random notes set, evaluation on the Random notes test set
        """
        from ActiveLearners.Classifiers.GOCLogRegClassifier import GOCLogRegClassifier

        gocEval: EvaluaterInt = gocEvaluater("Logistic Regression")
        emptyDF = DataFrame(columns=["text", "docID"])
        emptyDF.set_index("docID", drop=True, inplace=True, verify_integrity=False)
        pool = Pool(unlabeledExamples=emptyDF, trainingExamples=hdl.getAPCExamples(),
                    validationExamples=hdl.getValidationExamples(), testExamples=hdl.getTestExamples())
        log.info(f"Pool of examples created: {pool}")
        classifier: ClassifierInt = GOCLogRegClassifier('LogReg', config)

        classifier.train(pool.trainEx, pool.valEx, checkPointPath=None)

        log.info("Evaluation on the evaluation set...")
        predictions = classifier.classify(pool.valEx)
        log.info(gocEval.evaluate(predictions, pool.valEx))
        
        log.info("Evaluation on the test set...")
        predictions = classifier.classify(pool.testEx)
        log.info(gocEval.evaluate(predictions, pool.testEx))
        
        
    def runExperiment3(self, config: configparser, hdl:HandlerInt):
        """
        Experiment settings: word2vec embeddings + BiLSTM, weights [1,1], epoch 10 keep, the best, batch size 6 
        Trained on APC notes, the validation set is the Random notes set, evaluation on the Random notes test set
        """
        from ActiveLearners.Classifiers.CNNTF2Classifier import CNNTF2Classifier
        from TextProcessor.Wor2VecProcessor import Word2VecPreprocessor
        import pickle as pkl
        
        PATHEmbeddings = config['Data']['tmpFolder']+'preprocessWebMD.pkl'
        
        gocEval: EvaluaterInt = gocEvaluater("UncertaintySampler")
        emptyDF = DataFrame(columns=["text", "docID"])
        emptyDF.set_index("docID", drop=True, inplace=True, verify_integrity=False)
        pool = Pool(unlabeledExamples = emptyDF, trainingExamples = hdl.getAPCExamples(), validationExamples = hdl.getValidationExamples(), testExamples = hdl.getTestExamples())
        log.info(f"Pool of examples created: {pool}")
        
        #embeddings take to long to read, so I pickled it...
        if os.path.exists(PATHEmbeddings):
            log.debug(f"Im reading an existing process_WebMD pickled @{PATHEmbeddings}")
            processor = pkl.load(open(PATHEmbeddings, 'rb'))
        else:
            processor=Word2VecPreprocessor(pool.getAllTexts(), config)
            processor.generate_indices_embedding()
            pkl.dump(processor, open(PATHEmbeddings, 'wb'))
        
        classifier: ClassifierInt = CNNTF2Classifier('CNNTF2', processor, config, InitialModelsPath=None)
        classifier.train(pool.trainEx, pool.valEx, classifier.getInitialCheckpointPath())
        
        log.info("Evaluation on the evaluation set...")
        predictions = classifier.classify(pool.valEx)
        log.info(gocEval.evaluate(predictions, pool.valEx))
        
        log.info("Evaluation on the test set...")
        predictions = classifier.classify(pool.testEx)
        log.info(gocEval.evaluate(predictions, pool.testEx))

    def runExperiment3b(self, config: configparser, hdl:HandlerInt):
        """
        Experiment settings: word2vec embeddings + BiLSTM, weights [1,1], epoch 5 keep, the best, batch size 6 
        Trained with AL APC notes for seed, the training set of the random notes for unlabeled set, the validation set is the Random notes set, evaluation on the Random notes test set
        """
        from ActiveLearners.Classifiers.CNNTF2Classifier import CNNTF2Classifier
        from ActiveLearners.UncertaintySampler import UncertaintySampler, UncertaintyMeasure, USQueryingAlgorithm
        from TextProcessor.Wor2VecProcessor import Word2VecPreprocessor
        import pickle as pkl
        
        PATHEmbeddings = config['Data']['tmpFolder']+'preprocessWebMD.pkl'
        
        pool = Pool(unlabeledExamples = hdl.getTrainExamples(), trainingExamples = hdl.getAPCExamples(), validationExamples = hdl.getValidationExamples(), testExamples = hdl.getTestExamples())
        log.info(f"Pool of examples created: {pool}")
        gsOracle: GoldStandardOracle = GoldStandardOracle(pool.unlabEx.copy(), outputPath=config["Data"]["tmpFolder"])
        
        trainingArgs = {
            'unlabledSetAnnotated':True,
            'incrementalLearning':False,
            'evaluateEachALIteration':True,
            'writePredictionsAtEachALIteration':True,
            'batchSizeExamplesQueried':400,#400,
            'biasTowardMinorityClass':True,
            'budget':len(pool.unlabEx)}
        
        gocEval: EvaluaterInt = gocEvaluater("UncertaintySampler")
        
        #embeddings take to long to read, so I pickled it...
        if os.path.exists(PATHEmbeddings):
            log.debug(f"Im reading an existing process_WebMD pickled @{PATHEmbeddings}")
            processor = pkl.load(open(PATHEmbeddings, 'rb'))
        else:
            processor=Word2VecPreprocessor(pool.getAllTexts(), config)
            processor.generate_indices_embedding()
            pkl.dump(processor, open(PATHEmbeddings, 'wb'))
         
        actLearner: UncertaintySampler = UncertaintySampler(config, [CNNTF2Classifier('CNNTF2', processor, config, InitialModelsPath=None)], gocEval, trainingArgs)
        actLearner.setQueryingAlgorithm(USQueryingAlgorithm(UncertaintyMeasure.Entropy))
        
        actLearner.train(pool, gsOracle)
        predictions = actLearner.classify(pool.testEx)
        log.info(gocEval.evaluate(predictions, pool.testEx))
        

    def runExperiment4(self, config: configparser, hdl:HandlerInt):
        """
        Experiment settings: [BERT-base-cased|BioClinicalBERT], default transformers head, weights [1,1], epoch 5 keep, the best, batch size 6 (MAKE sure the transformers are correctly set up)
        Trained on APC notes, the validation set is the Random notes set, evaluation on the Random notes test set
        """
        from ActiveLearners.Classifiers.TranformersClassifier import TransformersClassifier
        
        gocEval: EvaluaterInt = gocEvaluater("UncertaintySampler")
        emptyDF = DataFrame(columns=["text", "docID"])
        emptyDF.set_index("docID", drop=True, inplace=True, verify_integrity=False)
        pool = Pool(unlabeledExamples = emptyDF, trainingExamples = hdl.getAPCExamples(), validationExamples = hdl.getValidationExamples(), testExamples = hdl.getTestExamples())
        log.info(f"Pool of examples created: {pool}")
        #classifier: ClassifierInt = TransformersClassifier('tinyBERT', config, maxLength=512, pretrained_model="prajjwal1/bert-tiny")
        #classifier: ClassifierInt = TransformersClassifier('BERT-base-cased', config, maxLength=512, pretrained_model="bert-base-cased")
        classifier: ClassifierInt = TransformersClassifier('roberta-base', config, maxLength=512, pretrained_model="roberta-base")
        #classifier: ClassifierInt = TransformersClassifier('bioClinicalBERT', config, maxLength=512, pretrained_model="emilyalsentzer/Bio_ClinicalBERT")

        classifier.train(pool.trainEx, pool.valEx, classifier.getInitialCheckpointPath())
        
        predictions = classifier.classify(pool.testEx)
        log.info(gocEval.evaluate(predictions, pool.testEx))

    
    def runExperiment6(self, config: configparser, hdl:HandlerInt):
        """
        The sixth experiment: use a default Bio_ClinicalBERT transformer classifier with active learning
        Seed APC notes, unlabeled: training 
        """
        from ActiveLearners.Classifiers.TranformersClassifier import TransformersClassifier
        from ActiveLearners.Classifiers.GOCLogRegClassifier import GOCLogRegClassifier
        from ActiveLearners.RandomSampler import RandomSampler
        from ActiveLearners.UncertaintySampler import UncertaintySampler, UncertaintyMeasure, USQueryingAlgorithm
        from ActiveLearners.ExpertCommittee import ExpertCommittee, ECQueryingAlgorithm, DisagreementAlgorithm
        from ActiveLearners.Ensemblers.WeightedAverageEnsembler import WeightedAverageEnsembler
        from ActiveLearners.Ensemblers.ClassifierEnsembler import ClassifierEnsembler
        from ActiveLearners.Classifiers.GOCRegExFinder import GOCRegExFinder
        
        pool = Pool(unlabeledExamples = hdl.getTrainExamples(), trainingExamples = hdl.getAPCExamples(), validationExamples = hdl.getValidationExamples(), testExamples = hdl.getTestExamples())
        log.info(f"Pool of examples created: {pool}")
        gsOracle: GoldStandardOracle = GoldStandardOracle(pool.unlabEx.copy(), outputPath=config["Data"]["tmpFolder"])
        
        trainingArgs = {
            'unlabledSetAnnotated':True,
            'incrementalLearning':False,
            'evaluateEachALIteration':True,
            'writePredictionsAtEachALIteration':True,
            'batchSizeExamplesQueried':400,#400,
            'biasTowardMinorityClass':True,
            'budget':len(pool.unlabEx)}

        gocEval: EvaluaterInt = gocEvaluater("ExpertCommittee")
        
        #actLearner: RandomSampler = RandomSampler(config, [TransformersClassifier('tinyBERT', config, maxLength=512, pretrained_model="prajjwal1/bert-tiny")], gocEval, trainingArgs)
        #gocEval: EvaluaterInt = gocEvaluater("ramdomSampler")

        # Active learning with Logistic Regression
        #actLearner: RandomSampler = RandomSampler(config, [GOCLogRegClassifier('LogReg', config)], gocEval, trainingArgs)
        #actLearner: UncertaintySampler = UncertaintySampler(config, [GOCLogRegClassifier('LogReg', config)], gocEval, trainingArgs)
        #actLearner.setQueryingAlgorithm(USQueryingAlgorithm(UncertaintyMeasure.Entropy))

        #actLearner: RandomSampler = RandomSampler(config, [TransformersClassifier('tinyBERT', config, maxLength=512, pretrained_model="prajjwal1/bert-tiny")], gocEval, trainingArgs)
        #actLearner: RandomSampler = RandomSampler(config, [TransformersClassifier('BERT-base-cased', config, maxLength=512, pretrained_model="bert-base-cased")], gocEval, trainingArgs)
        #actLearner: UncertaintySampler = UncertaintySampler(config, [RandomClassifier('randomClassifier')], gocEval, trainingArgs)
        #actLearner: UncertaintySampler = UncertaintySampler(config, [TransformersClassifier('bioClinicalBERT', config, maxLength=512, pretrained_model="prajjwal1/bert-tiny")], gocEval, trainingArgs)
        #actLearner: UncertaintySampler = UncertaintySampler(config, [TransformersClassifier('BERT-base-cased', config, maxLength=512, pretrained_model="bert-base-cased")], gocEval, trainingArgs)        
        #actLearner: UncertaintySampler = UncertaintySampler(config, [TransformersClassifier('roberta-large', config, maxLength=512, pretrained_model="roberta-large")], gocEval, trainingArgs)
        #actLearner: UncertaintySampler = UncertaintySampler(config, [TransformersClassifier('bioClinicalBERT', config, maxLength=512, pretrained_model="emilyalsentzer/Bio_ClinicalBERT")], gocEval, trainingArgs)
        #actLearner.setQueryingAlgorithm(USQueryingAlgorithm(UncertaintyMeasure.Entropy))
        #actLearner: ExpertCommittee = ExpertCommittee(config, [RandomClassifier('randomClassifier1'), RandomClassifier('randomClassifier2'), RandomClassifier('randomClassifier3')], gocEval, trainingArgs)
        #actLearner: ExpertCommittee = ExpertCommittee(config, WeightedAverageEnsembler(weights="Learned"), [TransformersClassifier('tinyBERT1', config, maxLength=512, pretrained_model="prajjwal1/bert-tiny"), GOCLogRegClassifier('LogReg', config), GOCRegExFinder('RegEx', config, verbose=False)], gocEval, trainingArgs)
        #actLearner: ExpertCommittee = ExpertCommittee(config, ClassifierEnsembler(), [TransformersClassifier('tinyBERT1', config, maxLength=512, pretrained_model="prajjwal1/bert-tiny"), GOCLogRegClassifier('LogReg', config), GOCRegExFinder('RegEx', config, verbose=False)], gocEval, trainingArgs)
        #actLearner: ExpertCommittee = ExpertCommittee(config, [TransformersClassifier('BERT-base-cased1', config, maxLength=512, pretrained_model="bert-base-cased"), TransformersClassifier('BERT-base-cased2', config, maxLength=512, pretrained_model="bert-base-cased"), TransformersClassifier('BERT-base-cased3', config, maxLength=512, pretrained_model="bert-base-cased")], gocEval, trainingArgs)
        #actLearner: ExpertCommittee = ExpertCommittee(config, [TransformersClassifier('BERT-base-cased', config, maxLength=512, pretrained_model="bert-base-cased"), TransformersClassifier('bioClinicalBERT', config, maxLength=512, pretrained_model="emilyalsentzer/Bio_ClinicalBERT"), TransformersClassifier('roberta-base', config, maxLength=512, pretrained_model="roberta-base")], gocEval, trainingArgs)
        actLearner: ExpertCommittee = ExpertCommittee(config, WeightedAverageEnsembler(weights="Learned"), [TransformersClassifier('BERT-base-cased', config, maxLength=512, pretrained_model="bert-base-cased"), TransformersClassifier('bioClinicalBERT', config, maxLength=512, pretrained_model="emilyalsentzer/Bio_ClinicalBERT"), TransformersClassifier('roberta-base', config, maxLength=512, pretrained_model="roberta-base"), GOCLogRegClassifier('LogReg', config), GOCRegExFinder('RegEx', config, verbose=False)], gocEval, trainingArgs)
        #actLearner: ExpertCommittee = ExpertCommittee(config, ClassifierEnsembler(), [TransformersClassifier('BERT-base-cased', config, maxLength=512, pretrained_model="bert-base-cased"), TransformersClassifier('bioClinicalBERT', config, maxLength=512, pretrained_model="emilyalsentzer/Bio_ClinicalBERT"), TransformersClassifier('roberta-base', config, maxLength=512, pretrained_model="roberta-base"), GOCLogRegClassifier('LogReg', config), GOCRegExFinder('RegEx', config, verbose=False)], gocEval, trainingArgs)    
        actLearner.setQueryingAlgorithm(ECQueryingAlgorithm(DisagreementAlgorithm.voteEntropy, actLearner))

        actLearner.train(pool, gsOracle)
        predictions = actLearner.classify(pool.testEx)
        log.info(gocEval.evaluate(predictions, pool.testEx))
        
    def runexpWebMDBalanced(self, config: configparser):
        """
        A test of the AL on a balance dataset, can be deleted
        """
        from Handlers.WebMDReviewsHandler import WebMDReviewsHandler
        from ActiveLearners.Classifiers.TranformersClassifier import TransformersClassifier
        from ActiveLearners.RandomSampler import RandomSampler
        from ActiveLearners.UncertaintySampler import UncertaintySampler, UncertaintyMeasure, USQueryingAlgorithm
        
        wrh = WebMDReviewsHandler()
        train = wrh.getTrainExamples()
        val = wrh.getValExamples()
        test = wrh.getTestExamples()
        
        #use 10% of the training examples as seed
        seed = train.sample(frac=0.1, random_state=6)
        #remaining as unlabeled
        unlabReviews = train.drop(seed.index, inplace=False)
        assert ((len(seed)+len(unlabReviews))==len(train)), f"Error in the code, seed {len(seed)}+ unlabeled {len(unlabReviews)} should be equal to the training set {len(train)}."
         
        webMDPool4GS = Pool(unlabeledExamples = unlabReviews, trainingExamples = seed, validationExamples = val, testExamples = test)
        log.info(f"Pool of examples created: {webMDPool4GS}")
        gsOracle: GoldStandardOracle = GoldStandardOracle(webMDPool4GS.unlabEx.copy())
        
        trainingArgs = {
            'incrementalLearning':True, 
            'evaluateEachALIteration':True,
            'batchSizeExamplesQueried':200,
            'budget':len(webMDPool4GS.unlabEx)}
        #actLearner: RandomSampler = RandomSampler(config, [TransformersClassifier('bioClinicalBERT', config, maxLength=512, pretrained_model="prajjwal1/bert-tiny")], trainingArgs)
        actLearner: UncertaintySampler = UncertaintySampler(config, [TransformersClassifier('bioClinicalBERT', config, maxLength=512, pretrained_model="prajjwal1/bert-tiny")], trainingArgs)
        actLearner.setQueryingAlgorithm(USQueryingAlgorithm(UncertaintyMeasure.Entropy))
        actLearner.train(webMDPool4GS, gsOracle)
        predictions = actLearner.classify(webMDPool4GS.testEx)
        evaluater: EvaluaterInt = Evaluater(actLearner.__class__.__name__)
        log.info(evaluater.evaluate(predictions, webMDPool4GS.testEx))
    
    
    def predict(self, config: configparser, hdl:HandlerInt):
        from ActiveLearners.Classifiers.TranformersClassifier import TransformersClassifier
        from ActiveLearners.Classifiers.GOCLogRegClassifier import GOCLogRegClassifier
        from ActiveLearners.RandomSampler import RandomSampler
        from ActiveLearners.UncertaintySampler import UncertaintySampler, UncertaintyMeasure, USQueryingAlgorithm
        from ActiveLearners.ExpertCommittee import ExpertCommittee, ECQueryingAlgorithm, DisagreementAlgorithm
        from ActiveLearners.Ensemblers.WeightedAverageEnsembler import WeightedAverageEnsembler
        from ActiveLearners.Classifiers.GOCRegExFinder import GOCRegExFinder
        
        trainingArgs = {
            'unlabledSetAnnotated':True,
            'incrementalLearning':False,
            'evaluateEachALIteration':True,
            'writePredictionsAtEachALIteration':True,
            'batchSizeExamplesQueried':400,
            'biasTowardMinorityClass':True,
            'budget':0}

        gocEval: EvaluaterInt = gocEvaluater("ExpertCommittee")
        
        actLearner: ExpertCommittee = ExpertCommittee(config, WeightedAverageEnsembler(), [TransformersClassifier('BERT-base-cased', config, maxLength=512, pretrained_model="bert-base-cased"), TransformersClassifier('bioClinicalBERT', config, maxLength=512, pretrained_model="emilyalsentzer/Bio_ClinicalBERT"), TransformersClassifier('roberta-base', config, maxLength=512, pretrained_model="roberta-base"), GOCLogRegClassifier('LogReg', config), GOCRegExFinder('RegEx', config, verbose=False)], gocEval, trainingArgs)        
        actLearner.setQueryingAlgorithm(ECQueryingAlgorithm(DisagreementAlgorithm.voteEntropy, actLearner))

        predictions = actLearner.classify(hdl.getTestExamples())
        predictions.to_csv(config['Data']['tmpFolder']+"ALPredictions.tsv", sep="\t")
        
    
if __name__ == '__main__':
    config = configparser.ConfigParser()
    #change with the path to your own configuration file
    #LOCAL
    #config.read('/Users/dweissen/tal/Soft_Developement/Codes/PyGit/GoalOfCareClassifier/GoalOfCareClassifier/Properties/config.properties.cfg')
    #i2c2Sub
    config.read('/home/dweissen/tal/projects/GoalOfCareClassifier/code/GoalOfCareClassifier/Properties/config.properties.i2c2.cfg')

    logFile = config['Logs']['logFile']
    log.basicConfig(level=log.DEBUG, 
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    handlers=[log.StreamHandler(sys.stdout), log.FileHandler(logFile, 'w', 'utf-8')])
    
    log.info("Create GOC pipeline")
    goc: GOC_Pipe = GOC_Pipe(config)
    log.info("Read annotated data...")
    hdl: EHRNotesHandler = EHRNotesHandler(config, config['Data']['apcnotes'], config['Data']['othernotes'])
    
    #goc.runExperiment1(config, hdl)
    #goc.runExperiment2(config, hdl)
    #goc.runExperiment3(config, hdl)
    #goc.runExperiment3b(config, hdl)
    #goc.runExperiment4(config, hdl)
    #goc.runExperiment6(config, hdl)
    #goc.runExperiment4(config, hdl)
    goc.runExperiment6(config, hdl)
    #goc.runexpWebMDBalanced(config)
    
    #goc.predict(config, hdl)

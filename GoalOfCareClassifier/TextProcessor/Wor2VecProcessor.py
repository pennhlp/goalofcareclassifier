import configparser
import numpy as np
import pandas as pd
from pandas.core.frame import DataFrame
from nltk.tokenize import TweetTokenizer
from keras.preprocessing.sequence import pad_sequences


class Word2VecPreprocessor(object):
    '''
    The object for text preprocessing.
    We use pretrained glove embedding to initialize word embedding. For those words out of glove but appear multiple times in our vocabulary (which is set to 3 times in this script), we also randomly initialize their embeddings.
    
    Input: 
        rawtext: 
        The original textual content .
    
    Output:
        embedding_index: 
        The matrix mapping words to their discrete indices. Indices of words are inputs to neural network.
        lister: 
        The word embedding matrix which maps indices to 400-dimensional vectors. We use this matrix to initialize the word embedding layer.
    '''

    def __init__(self, examples: DataFrame, config: configparser):
        self.sentenceLength=100
        self.rawtext=examples['text']
        self.embedding_index=dict()
        self.lister=np.array([0])
        
        self.config = config

    def tokenize(self, texts:list):
        tokenizer= TweetTokenizer(strip_handles=True, reduce_len = True, preserve_case=False)
        tokenized_texts=[]
        for text in texts:  
            try:
                tokenized_text=tokenizer.tokenize(text)
            except:
                pass
            tokenized_texts.append(tokenized_text)
        return tokenized_texts

    def generate_word_index(self, text):
        # generate indices for tokenized text        
        tokenized_text=self.tokenize(text)
        text_word_indices=[]
        for text in tokenized_text:
            text_word_index=[]
            for word in text:
                if word in self.embedding_index:
                    text_word_index.append(self.embedding_index[word])
                else:
                    text_word_index.append(self.embedding_index['PADDING'])
            text_word_indices.append(text_word_index)
        text_word_indices=pad_sequences(text_word_indices, maxlen=100, dtype='int32', padding='post', truncating='post', value=0)
        return text_word_indices


    def generate_indices_embedding(self):
        #count apppearing times of words
        tokenized_text=self.tokenize(self.rawtext)
        word_indices_count={'PADDING':[0,99999]}
        word_indices={'PADDING':0}
        self.embedding_index={'PADDING':0}
        for sentence in tokenized_text:
            for word in sentence:
                if word not in word_indices_count:
                    word_indices_count[word]=[len(word_indices_count),1]
                else:
                    word_indices_count[word][1]+=1
        for word in word_indices_count.keys():
            word_indices[word]=len(word_indices)

        w2vVocab=np.load(self.config["Resources"]["w2vVocabPath"], allow_pickle=True).item()
        w2vVector=np.load(self.config["Resources"]["w2vVectorPath"])
        
        count=0
        for word in word_indices.keys():
            if word in w2vVocab.keys():
                count+=1 
            elif word_indices_count[word][1]>1:
                count+=1
        self.lister=np.zeros((count,400),dtype='float32')
        
        for word in word_indices.keys():
            if word in w2vVocab.keys() and word!='PADDING':
                self.embedding_index[word]=len(self.embedding_index)
                self.lister[self.embedding_index[word]]=w2vVector[w2vVocab[word]]
        reference=self.lister[:len(self.embedding_index)-1]
        mu=np.mean(reference, axis=0)
        Sigma=np.cov(reference.T)
        for word in word_indices_count.keys():
            if word not in self.embedding_index.keys() and word_indices_count[word][1]>1:
                self.embedding_index[word]=len(self.embedding_index)
                self.lister[self.embedding_index[word]]=np.random.multivariate_normal(mu, Sigma, 1)
    
        return self.embedding_index,self.lister
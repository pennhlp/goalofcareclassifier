'''
Created on Mar 4, 2022

@author: dweissen
'''

import configparser
import logging as log
import pandas as pd
from pandas.core.frame import DataFrame
from pandas.core.series import Series
import sys
import numpy as np
import sklearn.metrics 

from Handlers.EHRNotesHandler import EHRNotesHandler

class IAACalculator(object):
    '''
    A small object to compute the IAA for this project
    '''

    def __init__(self, config: configparser):
        '''
        Constructor
        '''
        self.config = config
        

    def getKScoreAPCNotes(self, PATH_APCNotes):
        typesDict = {"note":"str", "sent_num":np.int64, "sentence":"str"}
        df = pd.read_csv(PATH_APCNotes, encoding = "utf-8", dtype=typesDict)
        assert len(df) == 16267, "Did not find the number of line expected in the annotated APC notes, check the input..."
        df.drop(['Unnamed: 0'], axis=1, inplace=True)
        df = self.__cleanAPCNotes(df)
        df = self.__getAPCNotesAnnotatorsLabels(df)
        
        print(f"K-score between Nick and Corinne on APC Notes: {sklearn.metrics.cohen_kappa_score(list(df['labelNick']), list(df['labelCorinne']))}")
        
    def __cleanAPCNotes(self, notes: DataFrame) -> DataFrame:
        """
        Apply several cleaning process to the text
        """
        #remove the empty sentence
        notes['sentence'].replace('', np.nan, inplace=True)
        notes.dropna(subset=["sentence"], inplace=True)
        return notes
    
    def __getAPCNotesAnnotatorsLabels(self, notes:DataFrame) -> DataFrame:
        def __getNickAnnotations(row:Series):
            if (row["tag_GoalsofcareACP_other_nick"]==1 or 
                row["tag_GoalsofcareACP_prognosis_nick"]==1 or 
                row["tag_GoalsofcareACP_goals_preferences_plan_nick"]==1):
                return 1
            else:
                return 0
        notes["labelNick"] = notes.apply(lambda row: __getNickAnnotations(row), axis = 1)
        
        def __getCorinneAnnotations(row:Series):
            if (row["tag_GoalsofcareACP_other_corinne"]==1 or 
                row["tag_GoalsofcareACP_prognosis_corinne"]==1 or 
                row["tag_GoalsofcareACP_goals_preferences_plan_corinne"]==1):
                return 1
            else:
                return 0
        notes["labelCorinne"] = notes.apply(lambda row: __getCorinneAnnotations(row), axis = 1)
        notes.drop(columns=["tag_GoalsofcareACP_other_nick", "tag_GoalsofcareACP_prognosis_nick", "tag_GoalsofcareACP_goals_preferences_plan_nick", "tag_GoalsofcareACP_other_corinne", "tag_GoalsofcareACP_prognosis_corinne", "tag_GoalsofcareACP_goals_preferences_plan_corinne", "tag_GoalsofcareACP_other_lindsay", "tag_GoalsofcareACP_prognosis_lindsay", "tag_GoalsofcareACP_goals_preferences_plan_lindsay", "tag_GoalsofcareACP_other_joseph", "tag_GoalsofcareACP_prognosis_joseph", "tag_GoalsofcareACP_goals_preferences_plan_joseph", "adjud_other_GOC", "other_GOC_kc", "adjuc_prog_GOC", "prog_GOC_kc", "adjud_goals_GOC", "goals_GOC_kc"], inplace=True)
        return notes
            
    def getKScoreRandomNotes(self, PATH_RandomNotes):
        df = self.getRandomNotes(PATH_RandomNotes)
        print(f"K-score between Nick and Corinne on Random: {sklearn.metrics.cohen_kappa_score(list(df['labelNick']), list(df['labelCorinne']))}")
        
        
    def getRandomNotes(self, PATH_RandomNotes):
        """
        return the Random notes cleaned with the labels of Nick and Corinne merged into 2 columns
        labelNick and labelCorinne
        """
        typesDict = {"note_id":"str", "sentence_number":np.int32, "sentence":"str"}
        df = pd.read_csv(PATH_RandomNotes, encoding = "utf-8", dtype=typesDict)
        df.drop(['Unnamed: 0'], axis=1, inplace=True)
        df = self.__cleanRandomNotes(df)
        df = self.__getRandomNotesAnnotatorsLabels(df)
        return df
    
    
    def __getRandomNotesAnnotatorsLabels(self, notes:DataFrame) -> DataFrame:
        """
        """
        def __getNickAnnotations(row:Series):
            if (row['GoalsofcareACP_goals_preferences_plan_nick']==1 or
               row['GoalsofcareACP_prognosis_nick']==1 or
               row['GoalsofcareACP_other_nick']==1):
                return 1
            else:
                return 0
        notes["labelNick"] = notes.apply(lambda row: __getNickAnnotations(row), axis = 1)
        
        def __getCorinneAnnotations(row:Series):
            if (row['GoalsofcareACP_goals_preferences_plan_corinne']==1 or
                row['GoalsofcareACP_prognosis_corinne']==1 or
                row['GoalsofcareACP_other_corinne']==1):
                return 1
            else:
                return 0
        notes["labelCorinne"] = notes.apply(lambda row: __getCorinneAnnotations(row), axis = 1)
        #No need of the extra columns
        notes.drop(columns=["curation_GoalsofcareACP_goals_preferences_plan", "curation_GoalsofcareACP_prognosis", "curation_GoalsofcareACP_other", "disagreement", "random_check", "GoalsofcareACP_goals_preferences_plan_corinne", "GoalsofcareACP_goals_preferences_plan_nick", "GoalsofcareACP_prognosis_corinne", "GoalsofcareACP_prognosis_nick", "GoalsofcareACP_other_corinne", "GoalsofcareACP_other_nick", "GoalsofcareACP_goals_preferences_plan_qualifying_span_corinne", "GoalsofcareACP_goals_preferences_plan_qualifying_span_nick", "GoalsofcareACP_prognosis_qualifying_span_corinne", "GoalsofcareACP_prognosis_qualifying_span_nick","GoalsofcareACP_other_qualifying_span_corinne", "GoalsofcareACP_other_qualifying_span_nick"], inplace=True)
        notes.to_csv("/tmp/out.tsv", sep="\t")
        return notes
    
    
    def __cleanRandomNotes(self, notes:DataFrame) -> DataFrame:
        """
        Clean the notes
        """
        def removeQuote(row: Series) -> str:
            if (row['sentence'] is not None and not pd.isna(row['sentence']) and row['sentence']!='' and row['sentence'][0]=='`'):
                return row['sentence'][1:]
            else:
                return row['sentence']
        notes['sentence'] = notes.apply(lambda row: removeQuote(row), axis = 1)
        #remove the empty lines
        notes['sentence'].replace('', np.nan, inplace=True)
        notes.dropna(subset=["sentence"], inplace=True)
        return notes


#     def getKScoreRandomNotesTestSet(self, PATH_RandomNotes, testSet):
#         """
#         :param PATH_RandomNotes: the path to the Random notes annotated by Nick and Corinne
#         :param testSet: the subset of the random notes used as test set and labeled by our best system
#         :return: the K-scores showing the agreement between the system and each annotator
#         """
#         allNotes = self.__getRandomNotes(PATH_RandomNotes)
#         allNotes.set_index(["note_id", "sentence_number"], inplace = True, verify_integrity = True)
#         testSet.set_index(["note_id", "sentence_number"], inplace = True, verify_integrity = True)
#         df = testSet.join(allNotes)
#         assert(len(df)==len(testSet)), "Issue with the joint between the test set and the all notes, I did not retrieve all sentences of the test set after the jointure, check code and data."
#         
#         print(f"K-score between Nick and the Classifier on the test set of Random Notes: {sklearn.metrics.cohen_kappa_score(list(df['labelNick']), list(df['prediction']))}")
#         print(f"K-score between Corinne and the Classifier on the test set of Random Notes: {sklearn.metrics.cohen_kappa_score(list(df['labelCorinne']), list(df['prediction']))}")
        
if __name__ == '__main__':
    config = configparser.ConfigParser()
    #change with the path to your own configuration file
    #LOCAL
    config.read('/Users/dweissen/tal/Soft_Developement/Codes/PyGit/GoalOfCareClassifier/GoalOfCareClassifier/Properties/config.properties.cfg')
    #i2c2Sub
    #config.read('/home/dweissen/tal/projects/GoalOfCareClassifier/code/GoalOfCareClassifier/Properties/config.properties.i2c2.cfg')

    logFile = config['Logs']['logFile']
    log.basicConfig(level=log.DEBUG, 
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    handlers=[log.StreamHandler(sys.stdout), log.FileHandler(logFile, 'w', 'utf-8')])
    
    hdl: EHRNotesHandler = EHRNotesHandler(config, config['Data']['apcnotes'], config['Data']['othernotes'])
    
    iaaCalc = IAACalculator(config)
    iaaCalc.getKScoreAPCNotes(config['Data']['apcnotes'])
    iaaCalc.getKScoreRandomNotes(config['Data']['othernotes'])
    
    testSetAnnotated = iaaCalc.getRandomNotes(config['Data']['othernotes'])
    #testSetAnnotated.set_index(["note_id", "sentence_number"], inplace=True, verify_integrity=True)
    print(testSetAnnotated.columns)
    typesDict = {"note_id":"str", "sentence_number":np.int32, "sentence":"str"}
    testSetPrediction = pd.read_csv("/private/tmp/outActiveLearner_I9.tsv", sep="\t", dtype=typesDict)
    #testSetPrediction.set_index(["note_id", "sentence_number"], inplace=True, verify_integrity=True)
    
    testSetPrediction = testSetPrediction.merge(testSetAnnotated, how="inner", on=["note_id", "sentence_number"])
    print(testSetPrediction.columns)
    testSetPrediction.to_csv("/tmp/out.tsv", sep="\t")
    
    print(f"K-score between Nick and Corinne on the test set of Random Notes: {sklearn.metrics.cohen_kappa_score(list(testSetPrediction['labelNick']), list(testSetPrediction['labelCorinne']))}")
    print(f"K-score between Nick and the Classifier on the test set of Random Notes: {sklearn.metrics.cohen_kappa_score(list(testSetPrediction['labelNick']), list(testSetPrediction['prediction']))}")
    print(f"K-score between Corinne and the Classifier on the test set of Random Notes: {sklearn.metrics.cohen_kappa_score(list(testSetPrediction['labelCorinne']), list(testSetPrediction['prediction']))}")
    